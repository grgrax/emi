<?php

ini_set('display_errors',1);
error_reporting(-1);

require_once('Calculator.php');

$Calculator=new Calculator();

$amount=5000000;
$term=15;
$interest=10.5;
$startDate=new DateTime('12/01/2014');

echo "<h4>loan schedule</h4>";

echo "amount:$amount, term:$term, interest:$interest";
echo " startDate:".$startDate->format('Y-m-d');
echo "<hr>";

$installments=round($Calculator->calculate($amount, $term, $interest, $startDate),2);
// echo gettype($installments);
echo "installments: $installments";

$every_last_installment=$installments+(0.04*$amount);
echo "<br>$every_last_installment : every_last_installment ";

$remainingPercent=100-($term*4);
$final_last_installment=$every_last_installment+($remainingPercent*$amount/100);
echo "<br>$final_last_installment : final_last_installment ";

echo "<h4>loan calculator</h4>";
$emi=new Emicalculator();
$age=35;
$rate=2;
$emi->calculate($age,$term,$amount);


class Emicalculator{
	function calculate($age,$term,$amount){

		$emi['age']=$age;
		$emi['term']=$term;
		$emi['amount']=$amount;

		//our some values
		$emi['ourRate']=2;
		$emi['ourSomeValue']=0;

		$rateObj=new Rate($age,$term);
		$rate=$rateObj->getRate();
		$emi['rate']=$rate;
		$emi['largeSumDiscount']=0;					

		if($amount<=500000){
			$emi['largeSumDiscount']=0;					
		}
		elseif(500000<$amount and $amount<=2500000 ){
			$emi['largeSumDiscount']=0.5;					
		}
		elseif(2500000<$amount and $amount<=5000000 ){
			$emi['largeSumDiscount']=1;					
		}
		elseif(5000000<$amount and $amount<=10000000 ){
			$emi['largeSumDiscount']=1.5;					
		}
		elseif(10000000<$amount){
			$emi['largeSumDiscount']=2;					
		}

		$emi['afterDiscount']=$emi['rate']-$emi['largeSumDiscount'];
		$emi['annualPremium']=($emi['afterDiscount']*$emi['amount']/1000);
		$emi['discountOnAnnualPayment']=($emi['annualPremium']*$emi['ourRate']/100);
		$emi['premiumAfterDiscount']=$emi['annualPremium']-$emi['discountOnAnnualPayment'];
		$emi['monthlyPreiumAmount']=round(($emi['premiumAfterDiscount']-$emi['ourSomeValue'])/12,2);


		echo "<pre>";
		print_r($emi);
		echo "</pre>";
	}
}

class Rate{
	private $_age;
	private $_term;
	private $_rate;
	
	function __construct($age,$term){
		$this->_age=$age;
		$this->_term=$term;				
		$this->_rate=81.65;
	}

	function getRate(){
		return $this->_rate;
	}

	function lookUpRate(){
		$rates=array(
			'20'=>array(
				'18'=>'60.05',
				'19'=>'60.05',
				'20'=>'60.10',
				'21'=>'60.15',
				),
			);
	}
}

//=IF(E3<=500000,0,IF(AND(E3>=500001,E3<=2500000),-0.5,IF(AND(E3>=2500001,E3<=5000000),-1,IF(AND(E3>=5000001,E3<=10000000),-1.5,IF(E3>=10000001,-2)))))
?>