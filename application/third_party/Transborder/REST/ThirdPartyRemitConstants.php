<?php

namespace Transborder\REST;

final class ThirdPartyRemitConstants
{
	const PAYMENT_TYPE_ID = 'ID';
	const PAYMENT_TYPE_BANK = 'BANK';
	const PAYMENT_TYPE_WALLET = 'WALLET';
	const PAYMENT_TYPE_EWALLET = 'EWALLET';
	const PAYMENT_TYPE_MWALLET = 'MWALLET';
}

