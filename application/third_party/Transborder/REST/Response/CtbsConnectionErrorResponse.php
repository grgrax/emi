<?php
namespace Transborder\REST\Response;
use Transborder\REST\ErrorResponse;

class CtbsConnectionErrorResponse extends ErrorResponse
{
	public function __construct(){
		$this->setErrorCode(self::ERR_CTBS_NOT_ENABLED);
		$this->setHttpResponseCode(HTTP_INTERNAL_SERVER_ERROR);
	}
}