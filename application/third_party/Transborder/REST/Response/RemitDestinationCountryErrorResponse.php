<?php
namespace Transborder\REST\Response;
use Transborder\REST\ErrorResponse;

class RemitDestinationCountryErrorResponse extends ErrorResponse
{
	public function __construct(){
		$this->setErrorCode(self::ERR_DESTINATION_COUNTRY_NOT_FOUND);
		$this->setHttpResponseCode(HTTP_BAD_REQUEST);
	}
}