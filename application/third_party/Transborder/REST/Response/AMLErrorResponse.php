<?php
namespace Transborder\REST\Response;
use Transborder\REST\ErrorResponse;

class AMLErrorResponse extends ErrorResponse
{
	public function __construct(){
		$this->setErrorCode(self::ERR_AML_MATCH_FOUND);
		$this->setHttpResponseCode(HTTP_BAD_REQUEST);
	}
}