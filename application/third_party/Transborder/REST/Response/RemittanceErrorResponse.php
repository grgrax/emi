<?php
namespace Transborder\REST\Response;
use Transborder\REST\ErrorResponse;

class RemittanceErrorResponse extends ErrorResponse
{
	public function __construct(){
		$this->setErrorCode(self::ERR_CREATING_TRANSACTION_FAILED);
		$this->setHttpResponseCode(HTTP_INTERNAL_SERVER_ERROR);
	}
}