<?php
namespace Transborder\REST\Response;
use Transborder\REST\ErrorResponse;

class BankNotFoundErrorResponse extends ErrorResponse
{
	public function __construct(){
		$this->setErrorCode(self::ERR_BANK_NOT_FOUND);
		$this->setHttpResponseCode(HTTP_NOT_FOUND);
	}
}