<?php
namespace Transborder\REST\Response;
use Transborder\REST\ErrorResponse;

class ValueNotMatchedErrorResponse extends ErrorResponse
{
	public function __construct(){
		$this->setErrorCode(self::ERR_VALUE_NOT_MATCHED);
		$this->setHttpResponseCode(HTTP_BAD_REQUEST);
	}
}