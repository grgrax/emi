<?php
namespace Transborder\Forex;

use models\Common\ExchangeRateBatch;

use models\Agent;

class ForexManager{
	
	public function updateForexRates(ExchangeRateBatch $batch,$agent = NULL){
		$arepo = \CI::$APP->doctrine->em->getRepository('models\Agent');
		
		if(!is_null($agent)){
			$agent->setForexBatch($batch);
			\CI::$APP->doctrine->em->persist($agent);
			
			if($agent->getParentAgent() == NULL){
				
				$subagents = $agent->getSubagents();
// 				$subagents = $arepo->findBy(array(	'parentAgent'	=>	$agent));
				foreach($subagents as $s){
					$s->setForexBatch($batch);
					\CI::$APP->doctrine->em->persist($s);
				}
			}
		}else{
			foreach($arepo->findAll() as $ag){
				$ag->setForexBatch($batch);
				\CI::$APP->doctrine->em->persist($ag);
			}
		}
// 		exit;
		
		\CI::$APP->doctrine->em->flush();
		
		if(is_null($agent))
			\Options::update('forex_current_global_batch', $batch->id());
	}
}