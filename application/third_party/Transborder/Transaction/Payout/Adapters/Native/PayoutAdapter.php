<?php

namespace Transborder\Transaction\Payout\Adapters\Native;

use models\User\Group;

use Doctrine\DBAL\Types\Type;

use Doctrine\DBAL\Types\TimeType;

use models\LedgerPosting;

use Transborder\Common\Util\Calculator;

use models\Transaction;

class PayoutAdapter implements \Transborder\Transaction\Payout
{
	private $control_number;
	
	private $doctrine;
	
	public function __construct($control_number){
		$this->control_number = $control_number;
		$this->doctrine = &\CI::$APP->doctrine;
	}
	
	public function getDetails(){
		/* @var $trepo \Doctrine\ORM\EntityRepository */
		$trepo = $this->doctrine->em->getRepository('models\Transaction');
		
		$currentUser = \Current_User::user();
		$currentUserAgent = $currentUser->getAgent();
		
		$txn = $trepo->getPayoutTransaction($this->control_number);
		
		if (!is_null($txn)) {
			
			log_message('info', 'Transaction found for control number  :: '.$this->control_number);
			
			$payoutAgent   = $txn->getPayoutAgent();
			$payoutAgentId = 0;
			$remitAgentId  = $txn->getRemittingAgent()->id();

			if (!\Current_User::isSuperUser() and $txn->getDestinationCountry()->id() != $currentUserAgent->getCountry()->id()) {
				return NULL;
			}
			
			if($currentUser->getGroup()->id() != Group::SUPER_ADMIN 
				&& !is_null($payoutAgent) && ($payoutAgentId = $payoutAgent->id()) != $currentUserAgent->id()
				) {
				if(\CI::$APP->input->is_ajax_request())
					return array("This transaction is targeted for Payout agent: {$payoutAgent->getName()}.");
				return NULL;
			}
			if (($sts = $txn->getStatus()) != Transaction::STATUS_APPROVED) {
				if(\CI::$APP->input->is_ajax_request())
					return array("The current status of this transaction is ".strtoupper($sts).".");
				return NULL; 
			}
			
			if ($currentUserAgent->getParentAgent()) {
				if ( $currentUserAgent->id() == $remitAgentId // same subagent cant be remitter and payer 
						and $payoutAgentId != $remitAgentId  //  if not strictly assigned 
					) {  
					if(\CI::$APP->input->is_ajax_request())
						return array("This transaction cannot be paid out by Agent: " . $currentUserAgent->getName() . "."); 
					// return NULL;
				}
			}
			
			
			
			if($txn->getIsLocked()){
				if($txn->getLockedBy()->id() == \Current_User::user()->id()){
					$this->setLock($txn);
					return $txn;
				}
				else{
					$txn_lock_time = (\Options::get('txn_lock_time',300)) ? \Options::get('txn_lock_time',300) : 300;
					$lastLockedTime = $txn->getLockedTime()->getTimestamp();
					if(time()-$lastLockedTime > $txn_lock_time) {
						$this->setLock($txn);
						return $txn; 
					}
					else {
						if(\CI::$APP->input->is_ajax_request())
							return array("This transaction is temporarily locked by {$txn->getLockedBy()->getFullName()} ({$txn->getLockedBy()->getUsername()}).");
						return NULL; 
					}
				}
			}
			else {
				
				$this->setLock($txn);
				return $txn;
			}
			
		}	else return NULL;
	}
	
	private function setLock(\models\Transaction $txn){
		$txn->setLockedTime(new \Datetime());
		$txn->setIsLocked(TRUE);
		$txn->setLockedBy(\Current_User::user());
		$this->doctrine->em->persist($txn);
		$this->doctrine->em->flush();
	}
	
	public function pay($note = NULL,$beneficiaryDocuments=NULL){
		
		/* @var $trepo \Doctrine\ORM\EntityRepository */
		$trepo = $this->doctrine->em->getRepository('models\Transaction');
		$transaction = $trepo->findOneBy(array('control_number'=> md5($this->control_number)));
		$lockingUser = $transaction->getLockedBy();
		$currentUser = \Current_User::user();
		if($lockingUser == $currentUser){
			$transborder = new \Transborder();
			return $transborder->tm->pay($transaction, $note ,$beneficiaryDocuments);
		}
		else{
			return FALSE;
		}
		
	}

	public function getControlNumber()
	{
	    return $this->control_number;
	}
	
	public function cancel(){
		$trepo = $this->doctrine->em->getRepository('models\Transaction');
		$transaction = $trepo->findOneBy(array('control_number'=>md5($this->control_number)));
		$transaction->setIsLocked(FALSE);
		$transaction->setLockedBy(NULL);
		$transaction->setLockedTime(NULL);
		$this->doctrine->em->persist($transaction);
		$this->doctrine->em->flush();
		if($transaction->id()){
			return TRUE;
		}
		else{
			return FALSE;
		}
	}

}