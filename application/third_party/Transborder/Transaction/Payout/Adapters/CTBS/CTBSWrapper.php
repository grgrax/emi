<?php

namespace Transborder\Transaction\Payout\Adapters\CTBS;

class CTBSWrapper{
	
	const GET_INFO = 'MTCN_DETAIL_REQ';
	const PAYOUT_INIT = 'PAYOUT_INIT_REQ';
	const PAYOUT_CONFIRM = 'PAYOUT_CONFIRM_REQ';
	const PAYOUT_CANCEL = 'PAYOUT_CANCEL_REQ';
	const RELEASE = 'RELEASE_MTCN_REQ';
	private $control_number;
	private $ctbs;
	private $response;
	
	public function  __construct($control_number){
		
		$this->control_number = $control_number;
		
		try {
			
			$ctbs_url = \Options::get('ctbs_url', 'http://esewaincubator.f1soft.com.np/ctbs/ctbsws/ctbsservice.wsdl');
			
			log_message('info', 'CTBSWrapper::__construct() - Connecting to Central Transborder System :: '.$this->control_number);
			$this->ctbs = @new \SoapClient($ctbs_url);
			$this->ctbs->__setLocation ($ctbs_url);
			
	
		} catch(\Exception $e) {
			log_message('info', 'CTBSWrapper::__construct() - Could not connect to Central Transborder System :: '.$this->control_number);
			return FALSE;
		}
	}
	
	public function request($code='MTCN_DETAIL_REQ'){
	
		try {
			log_message('info', 'CTBSWrapper::request() - Requesting CTBS Request_code :: '.$code.' :: '.$this->control_number);
			$ctbsRequest = $this->ctbs->CTBSService(
										array(
												'username' 		=> \Options::get('ctbs_username','testagent'), 	
												'password'		=> \Options::get('ctbs_password','pass123'),
												'requestFields'	=> array(
																		array(	
																			'key' => 'MTCN',
																			'value' => $this->control_number,
																			)
																		),
												'requestCode'	=> $code,
												'agentUserId'   => \Current_User::user()->getUsername(),
												'uniqueId'		=> time(),
											)
									);	
					
				$this->response = $ctbsRequest;
				return $this->response;
					
						
		} catch(\Exception $e) {
			log_message('info', 'CTBSWrapper::request() - No response from CTBS for RequestCode :: '.$code.' :: '.$this->control_number);
			die($e->getMessage());
		}
	}
	
	public function getSoap(){

		return $this->ctbs;
				
	}
	
}
