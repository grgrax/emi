<?php

namespace Transborder\Transaction\Payout\Adapters\CTBS;

use models\Transaction;

use models\Customer;

use models\Transaction\IDPayment;

use models\Agent;

use Transborder\Common\Util\ConvertNumberToWord;


class PayoutAdapter implements \Transborder\Transaction\Payout
{
	private $control_number;
	private $doctrine;
	private $ctbs_txn;

	public function  __construct($control_number){
		
		$this->control_number = $control_number;
		$this->doctrine = &\CI::$APP->doctrine;
		$this->ctbs_txn = new CTBSWrapper($this->control_number);
	}
	
	public function getDetails(){		
			$ctbs_txn = $this->ctbs_txn;
			if ($ctbs_txn->getSoap() != ''){
				
				
				$response = $ctbs_txn->request(CTBSWrapper::GET_INFO);			
			
				
				if ($response->status == "FAILURE"){
					log_message('info', 'CTBSPayoutAdapter::getDetails() - Got response FAILURE from CTBS :: '.$this->control_number.' :: Details --> '.$response->responseMessage);
					return NULL;
				}
				else if ($response->status == "SUCCESS"){
				
							log_message('info', 'IRemitPayoutAdapter::getDetails() - Got response SUCCESS from IRemit :: '.$this->control_number);
							
							$mtcnDetail = $response->mtcnDetail;
							
							$cusrepo = $this->doctrine->em->getRepository('models\Customer');
							$transRepo = $this->doctrine->em->getRepository('models\Transaction');
							$known_transaction = $transRepo->findOneBy(array("control_number" => md5($mtcnDetail->mtcn)));
																
									$arepo = $this->doctrine->em->getRepository('models\Agent');
									$crepo = $this->doctrine->em->getRepository('models\Common\Country');
										
									$ctbs_detail= $response->mtcnDetail;
									$mtcnDetail= $ctbs_detail;
									
									$senderAgentCode = $mtcnDetail->senderAgentCode;
									
									log_message('info', 'looking for senderAgentCode:: '.$senderAgentCode);
										
									$known_agent = $arepo->findOneBy(array('branch_code' => $senderAgentCode));
									
									if (!$known_agent) {
										log_message('info', 'senderAgentCode not found for:: '.$senderAgentCode);
										$error_array = array();
									
										$error_array[] = 'Sender Agent code '.$senderAgentCode.' not found for '.$this->control_number;
									
										return $error_array;
									}
							
									$metafactotry = \CI::$APP->doctrine->em->getMetadataFactory();
									$txn = $metafactotry->getMetadataFor('models\Transaction\IDPayment');
		
									$metafactotry = \CI::$APP->doctrine->em->getMetadataFactory();
									$txn = $metafactotry->getMetadataFor('models\Transaction\IDPayment');
										
									$ctbsTxn = $txn->newInstance();
									$ctbsTxn->setControlNumber($ctbs_detail->mtcn);
									$ctbsTxn->setTrackingNumber($ctbs_detail->mtcn);
									$ctbsTxn->setRemittanceType(Transaction::TYPE_ID_PAYMENT);
									$date = new \DateTime($ctbs_detail->remittanceEntryDate);
									$ctbsTxn->setCreated($date);
										
									$remitter = $metafactotry->getMetadataFor('models\Customer');
									$remitter = $remitter->newInstance();
// 									$remitter->setFirstName($ctbs_detail->senderName);
									
									$name = str_replace('  ', ' ', trim($ctbs_detail->senderName));
									
									$full_name = \Splitfullname::split($name);								
									
									$remitter->setFirstName($full_name['fname']);
									$remitter->setMiddleName($full_name['initials']);
									$remitter->setLastName($full_name['lname']);
									
									$remitter->setGuid($cusrepo->generateGUID());
									$remitter->setStreet($ctbs_detail->senderAddress);
									$remitter->setMobile($ctbs_detail->senderContactDetail);
									$remitterCity = $known_agent->getCity();
									$remitterCountry = $remitterCity->getCountry();
									$remitter->setCity($remitterCity);
									
									$customerDetails =  array(
											'firstName'=>$full_name['fname'],
											'lastName'=>$full_name['lname'],
											'street'=>$ctbs_detail->senderAddress,
											'mobile'=>$ctbs_detail->senderContactDetail
											
											);
									$existremitter = $cusrepo->findOneBy($customerDetails);
									
																
									if($existremitter){
										$ctbsTxn->setRemitter($existremitter);
									}else{
										$ctbsTxn->setRemitter($remitter);
									}
															
									
									
									$beneficiary = $metafactotry->getMetadataFor('models\Customer');
									$beneficiary = $beneficiary->newInstance();
// 									$beneficiary->setFirstName($ctbs_detail->beneficiaryName);

									$beneficiary = new Customer();
									
									$b_name = trim($ctbs_detail->beneficiaryName);		

									$ben_name = \Splitfullname::split($b_name);
									
													
									$beneficiary->setFirstName($ben_name['fname']);
									$beneficiary->setMiddleName($ben_name['initials']);
									$beneficiary->setLastName($ben_name['lname']);
									
									$beneficiary->setGuid($cusrepo->generateGUID2());
									$beneficiary->setStreet(isset($ctbs_detail->beneficiaryAddress) ? $ctbs_detail->beneficiaryAddress : '');
									$beneficiary->setMobile((isset($ctbs_detail->beneficiaryContactDetail))? $ctbs_detail->beneficiaryContactDetail : '');
									
									
									$benficiaryDetails =  array(
											'firstName'=>$ben_name['fname'],
											'lastName'=>$ben_name['lname'],
											'street'=>$ctbs_detail->beneficiaryAddress,
											'mobile'=>$ctbs_detail->beneficiaryContactDetail
											);
									
									$existbeneficiary = $cusrepo->findOneBy($benficiaryDetails);
									
									if($existbeneficiary)
									{
											$ctbsTxn->setBeneficiary($existbeneficiary);
										
									}else{
											$ctbsTxn->setBeneficiary($beneficiary);
										
									}
										
									
									$ctbsTxn->setBeneficiary($beneficiary);
										
									$curRepo = $this->doctrine->em->getRepository('models\Common\Currency');
									$conRepo = $this->doctrine->em->getRepository('models\Common\Country');
										
									$receivingCurrency = $curRepo->findOneBy( array('iso_code'=>$ctbs_detail->receivingCurrency));
									$ctbsTxn->setReceivingCurrency($receivingCurrency);
										
									$remittingCurrency = $remitterCountry->getCurrency();
									$ctbsTxn->setRemittingCurrency($remittingCurrency);
										
									$ctbsTxn->setRemittingAmount($ctbs_detail->originatingAmount);
									$ctbsTxn->setTargetAmount($ctbs_detail->receivingAmount);
									$inWords = new ConvertNumberToWord();
									$ctbsTxn->setAmountInWord($inWords->numberToWord($ctbs_detail->receivingAmount));
									$ctbsTxn->setRemittingAgent($known_agent);
									$ctbsTxn->setStatus(\models\Transaction::STATUS_APPROVED);
									$ctbsTxn->setSource(Transaction::TXN_SRC_CTBS);
										
									return $ctbsTxn;
							}
				else{ return null;}
			}
			return false;
			
	}

	public function pay($note = NULL, $beneficiaryDocuments = NULL){
		$ctbs_txn = new CTBSWrapper($this->control_number);
		
		$cusrepo = $this->doctrine->em->getRepository('models\Customer');	

		if ($ctbs_txn->getSoap() != NULL) {

			$ctbs_detail = $ctbs_txn->request(CTBSWrapper::GET_INFO)->mtcnDetail;

			$remitterexits = $cusrepo->checkCustomer($ctbs_detail->senderContactDetail);			
			
 			$beneficiaryexits = $cusrepo->checkBenefiaciary($ctbs_detail->beneficiaryContactDetail);
			
			
			if ($ctbs_txn->request(CTBSWrapper::PAYOUT_INIT)->status == 'SUCCESS') {

				$senderAgentCode = $ctbs_detail->senderAgentCode;
				log_message('info', 'looking for senderAgentCode:: ' . $senderAgentCode);

				$arepo = $this->doctrine->em->getRepository('models\Agent');
				$transRepo = $this->doctrine->em->getRepository('models\Transaction');
				$known_transaction = $transRepo->findOneBy(array("control_number" => md5($ctbs_detail->mtcn)));
				$known_agent = $arepo->findOneBy(array('branch_code' => $senderAgentCode));

				log_message('info', 'CTBSPayoutAdapter::pay() - PAYOUT_INIT --> SUCCESS from CTBS :: ' . $this->control_number);

				if ($known_transaction) {
					$ctbsTxn = $known_transaction ;
					
				} else 
				{
					$ctbsTxn = new IDPayment();
					$date = new \DateTime($ctbs_detail->remittanceEntryDate);
					$ctbsTxn->setCreated($date);
					$ctbsTxn->setControlNumber($ctbs_detail->mtcn);					
					

			     	$remitter = new Customer();

					$name = str_replace('  ', ' ', trim($ctbs_detail->senderName));

					$word_count = str_word_count($name);

					if ($word_count <= 2) {
						$arr = explode(' ', trim($name));
						$f_name = $arr[0];
						$m_name = NULL;
						$l_name = $arr[$word_count - 1];
					} elseif ($word_count < 2) {
						$f_name = $name;
						$m_name = NULL;
						$l_name = NULL;
					} else {
						$arr = explode(' ', trim($name));
						$f_name = $arr[0];
						$l_name = $arr[$word_count - 1];
						$m_name = str_replace(array($f_name, $l_name), "", $name);
					}
                    
					$remitter->setGuid($cusrepo->generateGUID());
					$remitter->setFirstName($f_name);
					$remitter->setMiddleName($m_name);
					$remitter->setLastName($l_name);
					
					$remitter->setStreet($ctbs_detail->senderAddress);
					$remitter->setMobile($ctbs_detail->senderContactDetail);
					$remitter->setCity($known_agent->getCity());
					
					
					$customerDetails =  array(
							'firstName'=>$f_name,
							'lastName'=>$l_name,
							'street'=>$ctbs_detail->senderAddress,
							'mobile'=>$ctbs_detail->senderContactDetail
								
					);
					$existremitter = $cusrepo->findOneBy($customerDetails);
						
										
					if($existremitter){
							$ctbsTxn->setRemitter($existremitter);
					}else{
							$ctbsTxn->setRemitter($remitter);
					}
					
							
					$beneficiary = new Customer();

					$b_name = trim($ctbs_detail->beneficiaryName);
					$b_word_count = str_word_count($b_name);
					if ($b_word_count <= 2) {
						$arr = explode(' ', trim($b_name));
						$bf_name = $arr[0];
						$bm_name = null;					
						$bl_name = $arr[2];
						
					} elseif ($b_word_count < 2) {
						$bf_name = $b_name;
						$bm_name = null;
						$bl_name = null;
					} else {
						$arr = explode(' ', trim($b_name));
						$bf_name = $arr[0];
						$bm_name = $arr[1]; 
						$bl_name = $arr[2];
					}
					
					$beneficiary->setGuid($cusrepo->generateGUID2());
					$beneficiary->setFirstName($bf_name);
					$beneficiary->setMiddleName($bm_name);
					$beneficiary->setLastName($bl_name);
									
					$beneficiary->setStreet(isset($ctbs_detail->beneficiaryAddress) ? $ctbs_detail->beneficiaryAddress : '');
					$beneficiary->setMobile((isset($ctbs_detail->beneficiaryContactDetail)) ? $ctbs_detail->beneficiaryContactDetail : '');
					
					$benficiaryDetails =  array(
							'firstName'=>$bf_name,
							'lastName'=>$bl_name,
							'street'=>$ctbs_detail->beneficiaryAddress,
							'mobile'=>$ctbs_detail->beneficiaryContactDetail
					);
						
					$existbeneficiary = $cusrepo->findOneBy($benficiaryDetails);
						
					if($existbeneficiary)
					{
						$ctbsTxn->setBeneficiary($existbeneficiary);
					
					}else{
						$ctbsTxn->setBeneficiary($beneficiary);
					
					}
					
								
					
					$curRepo = $this->doctrine->em->getRepository('models\Common\Currency');
					$conRepo = $this->doctrine->em->getRepository('models\Common\Country');

					$receivingCurrency = $curRepo->findOneBy(array('iso_code' => $ctbs_detail->receivingCurrency));
					$remitting_currency = $known_agent->getCity()->getCountry()->getCurrency();

					if (!is_null($receivingCurrency)) {
						$ctbsTxn->setReceivingCurrency($receivingCurrency);
						$receiving_currency = $receivingCurrency->getIsoCode();
					} else {
						$receiving_currency = '';
					}

					if (!is_null($remitting_currency)) {
						$ctbsTxn->setRemittingCurrency($remitting_currency);
					}
					$ctbsTxn->setTargetAmount($ctbs_detail->receivingAmount);
					$inWords = new ConvertNumberToWord();
					$ctbsTxn->setAmountInWord($receiving_currency . ' ' . $inWords->numberToWord($ctbs_detail->receivingAmount));

					$arepo = $this->doctrine->em->getRepository('models\Agent');
					$agent = $arepo->findOneBy(array(
							'branch_code' => $ctbs_detail->senderAgentCode,
					));

					$ctbsTxn->setRemittingAgent($agent);
					$ctbsTxn->setStatus(\models\Transaction::STATUS_APPROVED);
					$ctbsTxn->setTrackingNumber($ctbs_detail->mtcn);
					$ctbsTxn->setRemittingAmount($ctbs_detail->originatingAmount);

					$ctbsTxn->setUser(\Current_User::user());
					$ctbsTxn->setRemittanceType(Transaction::TYPE_ID_PAYMENT);
					$ctbsTxn->setSource(Transaction::TXN_SRC_CTBS);
					$ctbsTxn->setPaidDate(new \DateTime());
				}

				$response = $ctbs_txn->request(CTBSWrapper::PAYOUT_CONFIRM);

				if ($response->status == 'SUCCESS') {

					log_message('info', 'CTBSPayoutAdapter::pay() - PAYOUT_CONFIRM --> SUCCESS from CTBS :: ' . $this->control_number);

					$transborder = new \Transborder();
					return $transborder->tm->pay($ctbsTxn, $note, $beneficiaryDocuments);
				}
			}
		}
		return FALSE;
	}
	
	public function cancel(){
		
		$ctbs_txn = new CTBSWrapper($this->control_number);
		
		if($ctbs_txn->getSoap()!=NULL){
			if ($ctbs_txn->request(CTBSWrapper::RELEASE)->status == 'SUCCESS') {
				log_message('info', 'CTBSPayoutAdapter::pay() - PAYOUT_CANCEL --> SUCCESS from CTBS :: '.$this->control_number);
				return TRUE;
			}
		}
		return FALSE;
	}

	public function bank_transactions(){
		
	}
	
	public function bank_pay(){
		
	}
	
	public function comm_report(){
		
	}
	
	public function getStatus(){}
	
}