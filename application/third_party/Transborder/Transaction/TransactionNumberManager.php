<?php
namespace Transborder\Transaction;

use Transborder\Transaction\TransactionNumber\StrategyBase;

class TransactionNumberManager implements StrategyBase {
	
	/*
	 * @param bool useFactory | default TRUE
	 * @return actual TrackingNumber based on strategy  
	 */
	public function generateTrackingNumber($useFactory = TRUE) {
		
	//@todo:  determine strategy with config vars
		
		if ($useFactory) $strategy = new \Transborder\Transaction\TransactionNumber\FactoryDefault();
		else $strategy = new \Transborder\Transaction\TransactionNumber\BankSpecific();
		
		return $strategy->generateTrackingNumber();
		
	}
	
	/*
	 * @param bool useFactory | default TRUE
	 * @return actual MTCN based on strategy
	*/
	public function generateControlNumber($useFactory = TRUE) {
		
	//@todo:  determine strategy with config vars
		
		if ($useFactory) $strategy = new \Transborder\Transaction\TransactionNumber\FactoryDefault();
		else $strategy = new \Transborder\Transaction\TransactionNumber\BankSpecific();
		
		return $strategy->generateControlNumber();
	}
	
}