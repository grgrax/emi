<?php
namespace Transborder\Transaction\TransactionNumber;

use Transborder\Transaction\TransactionNumber\StrategyBase;

class FactoryDefault implements StrategyBase {
	
	public function generateTrackingNumber() {
	
		$user = \Current_User::user();
		$country = $mtcnPrefix = \Current_User::getRemitDestCountry();
		$mtcnPrefix = ($country)? $country->getMtcn() : $user->getAgent()->getCountry()->getMtcn();
		$generatedNumber = $mtcnPrefix.random_string('numeric', 16);
	
		$exist = \CI::$APP->doctrine->em->getRepository('models\Transaction')
							->findBy(array('tracking_number' => $generatedNumber));
				
		if ($exist) return $this->generateTrackingNumber();
		else return $generatedNumber;
	
	}
	
	public function generateControlNumber() {
	
		$user = \Current_User::user();
		$country = $mtcnPrefix = \Current_User::getRemitDestCountry();
		$mtcnPrefix = ($country)? $country->getMtcn() : $user->getAgent()->getCountry()->getMtcn();
		$generatedNumber = $mtcnPrefix.random_string('numeric', \Options::get('txn_mtcn_len', '12'));
	
		$exist = \CI::$APP->doctrine->em->getRepository('models\Transaction')
							->findBy( array('control_number' => md5($generatedNumber)) );
	
		if($exist) return $this->generateControlNumber();
		else return $generatedNumber;
	
	}
	
}