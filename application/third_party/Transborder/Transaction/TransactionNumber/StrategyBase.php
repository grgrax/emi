<?php 
namespace Transborder\Transaction\TransactionNumber;

interface StrategyBase{
	
	public function generateTrackingNumber();
	
	public function generateControlNumber();

} 