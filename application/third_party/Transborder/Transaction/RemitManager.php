<?php

namespace Transborder\Transaction;

use models\CustomerRelation;

use models\PrivilegeCard;

use models\Transaction\TransactionNote;

use models\CustomerIDocuments;

use models\Customer;

use models\Transaction\WalletPayment;

use models\Transaction\IDPayment;

use models\Transaction\BankPayment;

use models\Transaction;

use Transborder\Common\Util\CommissionCalculator;
use Transborder\Common\Util\Calculator;

class RemitManager{
	
	private $ci;
	
	public function __construct(){
		$this->ci = &\CI::$APP;
	}
	
	/**
	 * 
	 * @param models\Transaction $transaction
	 * @return \Transborder\Transaction\Remit
	 */
	public function getRemitAdapter($transaction){
		
// 		if(\Options::get('use_ctbs','0') == '1'){
			
// 			return (
// 						($transaction instanceof IDPayment && !$transaction->getPayoutAgent()) or 
// 						($transaction instanceof  BankPayment && \Options::get('use_ctbs_for_bankpayment', 0) == '1') or 
// 						($transaction instanceof WalletPayment)
// 					)
// 			? new \Transborder\Transaction\Remit\CTBS\RemitAdapter()
// 			: new \Transborder\Transaction\Remit\Native\RemitAdapter();
			
// 		}

		//if(\Options::get('use_ctbs','0') == '1' &&  ($_POST['appkey'])==''){

		if(\Options::get('use_ctbs','0') == '1' ){
			return (
					($transaction instanceof IDPayment) or
					($transaction instanceof  BankPayment && \Options::get('use_ctbs_for_bankpayment', 0) == '1') or
					($transaction instanceof WalletPayment)
			)
			? new \Transborder\Transaction\Remit\CTBS\RemitAdapter()
			: new \Transborder\Transaction\Remit\Native\RemitAdapter();
				
		}
		
		return new \Transborder\Transaction\Remit\Native\RemitAdapter();
	}
	
	/**
	 *
	 * @param mixed $remit_data
	 * @return models\Transaction
	 */
	public function createTransaction($remit_data, $thirdPartyCall = FALSE){
		

		$current_user = \Current_User::user();
		$remitting_agent = $current_user->getAgent();
		$remitting_currency = $remitting_agent->getCountry()->getCurrency();
	
		$destination_country = $this->ci->doctrine->em->find('models\Common\Country', $remit_data['destination_country']);
		$receiving_currency = $destination_country->getCurrency();
		
		$crepo = $this->ci->doctrine->em->getRepository('models\Customer');
	
		$transaction_type = $remit_data['pay_type'];
		$amount_in_words = $remit_data['amount_in_words'];
		$remitting_exchange_id = $remit_data['remitting_exchange_id'];
		$receiving_exchange_id = $remit_data['receiving_exchange_id'];
	
		if(file_exists(APPPATH.'modules/privilegecard')){
			$privilegeCardType = (isset($remit_data['privilege_card_type']))?$remit_data['privilege_card_type']:'';
			$privilegeCardNumber =(isset($remit_data['privilege_card_number']))?$remit_data['privilege_card_number']:'';
			$privilegeCard = NULL;
		}
		else{
			$privilegeCardType = '';
			$privilegeCardNumber ='';
			$privilegeCard = NULL;
		}
	
		switch ($transaction_type){
			case Transaction::TYPE_BANK_PAYMENT:
				$transaction = new BankPayment();
				break;
					
			case Transaction::TYPE_WALLET_PAYMENT:
				$transaction = new WalletPayment();
				break;
					
			default:
				$transaction = new IDPayment();					
		}
	
		if($privilegeCardType!='' && $privilegeCardNumber!=''){
			$privilegeCardRepo = $this->ci->doctrine->em->getRepository('models\PrivilegeCard');
			$privilegeCard = $privilegeCardRepo->findOneBy(array('card_type'=>$privilegeCardType, 'card_number'=>$privilegeCardNumber, 'status'=>PrivilegeCard::CARD_ACTIVE));
		}
	
			
	if(isset($remit_data['remitter_guid']) && $remit_data['remitter_guid'] != ''){
			$remitter = $crepo->findOneBy(array('guid'=>$remit_data['remitter_guid']));
		}else
		{			
			$remitter = new Customer();
			$remitter->setFirstName($remit_data['remitter_name']);
			$remitter->setStreet($remit_data['remitter_street']);
			$remitter_mobile = ($remit_data['remitter_mobile']!="")? $remit_data['remitting_country_code'].$remit_data['remitter_mobile'] : "";
			$remitter->setMobile($remitter_mobile);
			$remitter->setGuid($crepo->generateGUID());
			$remitter->setPhone($remit_data['remitter_phone']);
				
			$remitterID = new CustomerIDocuments();
			$remitterID->setCustomer($remitter);
			$remitterID->setDocType($this->ci->doctrine->em->find('models\Common\IdentificationDocument',$remit_data['remitter_id_type']));
			$remitterID->setDocNumber($remit_data['remitter_id_no']);
			$remitterID->setIssuedCountry($this->ci->doctrine->em->find('models\Common\Country',$remit_data['issued_country_remitter']));
			$this->ci->doctrine->em->persist($remitterID);
			$transaction->setRemitterDocument($remitterID);
			
		}
		
		if(isset($remit_data['beneficiary_guid']) && $remit_data['beneficiary_guid'] != ' '){
			$beneficiary = $crepo->findOneBy(array('guid'=>$remit_data['beneficiary_guid']));
		}else{
			$beneficiary = new Customer();
			if($transaction_type == Transaction::TYPE_BANK_PAYMENT){
				$beneficiary_bank_branch = $this->ci->doctrine->em->find('models\Common\BankBranch', $remit_data['beneficiary_bank_branch']);
				$beneficiary_city = $beneficiary_bank_branch->getCity();
				$beneficiary->setCity($beneficiary_city);
			}
			
			$beneficiary->setFirstName($remit_data['beneficiary_name']);
			$beneficiary->setStreet($remit_data['beneficiary_street']);
			$beneficiary->setMobile($remit_data['beneficiary_mobile']);
			$beneficiary->setPhone($remit_data['beneficiary_phone']);
			$beneficiary->setGuid($crepo->generateGUID());
			$this->ci->doctrine->em->persist($beneficiary);			

		}
	
	
		$transaction->setBeneficiary($beneficiary);
		$transaction->setRemitter($remitter);																																																																																																				
		$transaction->setRemittingAgent($remitting_agent);
		$transaction->setRemittingAmount($remit_data['sending_amount']);
		$transaction->setUSDAmount($remit_data['amount_in_usd']);
		$transaction->setNetComission($remit_data['commission']);
		$transaction->setDepositedAmount($remit_data['total_amount']);
		$transaction->setTargetAmount($remit_data['receiving_amount']);
		$transaction->setExchangeRate($remit_data['sdExchangeRate']);
		$transaction->setEffectiveExchangeRateRemitter($this->ci->doctrine->em->find('models\Common\ExchangeRate',$remit_data['remitting_exchange_id']));
		$transaction->setEffectiveExchangeRatePayout($this->ci->doctrine->em->find('models\Common\ExchangeRate',$remit_data['receiving_exchange_id']));
		$transaction->setRemittingCurrency($remitting_currency);
		$transaction->setReceivingCurrency($receiving_currency);
		$transaction->setUser($current_user);
		$transaction->setDestinationCountry($destination_country);
		$transaction->setRemittanceType($transaction_type);
		
	
		if ($transaction instanceof IDPayment && $remit_data['payout_sub_agent'] != ''){
			$payout_agent = $this->ci->doctrine->em->find('models\Agent', $remit_data['payout_sub_agent']);
			$transaction->setPayoutAgent($payout_agent);
		}
	
		$tracking_number = $this->ci->transborder->nm->generateTrackingNumber(TRUE);
	
		$transaction->setTrackingNumber($tracking_number);
	
		if ($transaction instanceof BankPayment) {
			$transaction->setAccountName($remit_data['beneficiary_name']);
			$transaction->setAccountNumber($remit_data['beneficiary_account_no']);
			$transaction->setBankBranch($this->ci->doctrine->em->find('models\Common\BankBranch', $remit_data['beneficiary_bank_branch']));
		}
	
		if ($transaction instanceof WalletPayment){
			$this->ci->load->helper('email');
				
			if(valid_email($remit_data['beneficiary_wallet_id'])){
				$transaction->setWalletAddress($remit_data['beneficiary_wallet_id']);
				$transaction->setWalletType(WalletPayment::TYPE_EWALLET);
			} else {
				$transaction->setWalletType(WalletPayment::TYPE_MWALLET);
				$beneficiary_mobile = $remit_data['beneficiary_country_code'].$remit_data['beneficiary_wallet_id'];
				$beneficiary->setMobile($beneficiary_mobile);
				$transaction->setWalletAddress($beneficiary_mobile);
			}
				
		}
	
		/**
		 Delegate MTCN generation Service
		 */
		
		$adapter = $this->getRemitAdapter($transaction);
	
		try{
			$mtcn_normal = $adapter->generateControlNumber($transaction);
		}
		catch(\Exception $e)
		{
			if($thirdPartyCall){
				throw new \Exception($e->getMessage());	
			}
			else{
				$this->ci->message->set($e->getMessage(), 'error', TRUE, 'feedback');
				redirect('remittance/newentry');
			}
		}
		
//    Transaction source ctbs
// 		$transactionSource = ($thirdPartyCall)? Transaction::TXN_SRC_THIRDPARTY : Transaction::TXN_SRC_NATIVE;
		$transactionSource = Transaction::TXN_SRC_CTBS;
		
		$transaction->setControlNumber($mtcn_normal);
	
		$transaction->setStatus(Transaction::STATUS_PENDING);
		$transaction->setAmountInWord($remit_data['amount_in_words']);
		$transaction->setSource($transactionSource);
		$transaction->setPurpose($remit_data['purpose']);
		$transaction->setMoneySource($remit_data['source']);
		$aml_status = ($this->ci->session->userdata('aml_status')!= '')? $this->ci->session->userdata('aml_status') : 0;
	
		$transaction->setAmlStatus($aml_status);
		
		$customerRelation = new CustomerRelation();
		$customerRelation->setRemitter($remitter);
		$customerRelation->setBenficiary($beneficiary);
		
		$this->ci->doctrine->em->persist($customerRelation);


		// $remit_data['commission_slab_id'] = 1;

		/*
		commission slab
		*/
		// show_pre($remit_data);
		// exit

		if(!isset($remit_data['commission_slab_id'])){
			/*
			* @$paagent = 0 -> refers to open transaction
			*if its bank payment get corresponding branch
			*/


			if(isset($remit_data['beneficiary_bank_branch'])){
				$bank_branch = \CI::$APP->doctrine->em->find('models\Common\BankBranch', $remit_data['beneficiary_bank_branch']);
				$bank = $bank_branch->getBank()->id();
			}
			else $bank = 0;

			$commCalc = new CommissionCalculator();
			
			$totalAmount = $commCalc->calculateTotalAmount($remit_data['sending_amount'], $remitting_currency, $receiving_currency, $remitting_agent, $destination_country , $paAgent = 0,  $transaction_type, $bank);

			$remit_data['commission_slab_id'] = $totalAmount->commission_slab_id;
		}
		
		$commission_slab = $this->ci->doctrine->em->find('models\RemitCommission', $remit_data['commission_slab_id']);
		$transaction->setCommissionSlab($commission_slab);
	
		$this->ci->doctrine->em->persist($transaction);
		
		$note = $remit_data['remarks'].' (Transaction Created)';
		
		$this->ci->transborder->tm->createTransactionNote($transaction, $note, Transaction::STATUS_PENDING);
		
		$this->ci->doctrine->em->flush();
	
		if($transaction->id())
		{
			$this->ci->session->set_userdata('mtcn',$mtcn_normal);
				
			\Events::trigger('transaction_entry',array('txn' => $transaction, 'MTCN' => $mtcn_normal));
				
			return $transaction;
		}else{
			
			if($thirdPartyCall)
			{
				throw new \Exception("Sorry, Unable to create Transaction.");
			}
			
			return FALSE;
		}
	}
	
	
	public function createUploadTransaction($remit_data,$thirdPartyCall = FALSE){
	
	
		$current_user = \Current_User::user();
		$remitting_agent = $current_user->getAgent();
		$remitting_currency = $remitting_agent->getCountry()->getCurrency();
	
		$destination_country = $this->ci->doctrine->em->find('models\Common\Country', $remit_data['destination_country']);
		$receiving_currency = $destination_country->getCurrency();
	
		$make_new_doc_entry = ($remit_data['check_new_entry'] == 'Y')? TRUE : FALSE;
	
		$transaction_type = $remit_data['pay_type'];
		$amount_in_words = $remit_data['amount_in_words'];
		$remitting_exchange_id = $remit_data['remitting_exchange_id'];
		$receiving_exchange_id = $remit_data['receiving_exchange_id'];
	
	
		$privilegeCardType = '';
		$privilegeCardNumber ='';
		$privilegeCard = NULL;
	
	
		switch ($transaction_type){
			case Transaction::TYPE_BANK_PAYMENT:
				$transaction = new BankPayment();
				break;
	
			default:
				$transaction = new IDPayment();
					
		}
	
	
		$remitter = new Customer();
		$remit_data['remitter_street']=(isset($remit_data['remitter_street']))?$remit_data['remitter_street']:'';
		$remit_data['remitter_phone']=(isset($remit_data['remitter_phone']))?$remit_data['remitter_phone']:'';
	
		$remitter->setName($remit_data['remitter_name']);
		$remitter->setStreet($remit_data['remitter_street']);
		$remitter_mobile = (isset($remit_data['remitter_mobile']))? $remit_data['remitting_country_code'].$remit_data['remitter_mobile'] : "";
		$remitter->setMobile($remitter_mobile);
		$remitter->setPhone($remit_data['remitter_phone']);
	
		$remitterID = new CustomerIDocuments();
		$remitterID->setCustomer($remitter);
		$remitterID->setDocType($this->ci->doctrine->em->find('models\Common\IdentificationDocument',$remit_data['remitter_id_type']));
		$remitterID->setDocNumber($remit_data['remitter_id_no']);
		$remitterID->setIssuedCountry($this->ci->doctrine->em->find('models\Common\Country',$remit_data['issued_country_remitter']));
		$this->ci->doctrine->em->persist($remitterID);
	
		$transaction->setRemitterDocument($remitterID);
	
	
		$this->ci->doctrine->em->persist($remitter);
	
		$beneficiary_mobile = (isset($remit_data['beneficiary_mobile'])) ? $remit_data['beneficiary_country_code'].$remit_data['beneficiary_mobile'] : '';
	
		$beneficiary = new Customer();
	
		if($transaction_type == Transaction::TYPE_BANK_PAYMENT){
			$beneficiary_bank_branch = $this->ci->doctrine->em->find('models\Common\BankBranch', $remit_data['beneficiary_bank_branch']);
			$beneficiary_city = $beneficiary_bank_branch->getCity();
			$beneficiary->setCity($beneficiary_city);
		}
	
		$beneficiary->setName(ucwords($remit_data['beneficiary_name']));
		$remit_data['beneficiary_street']=(isset($remit_data['beneficiary_street']))?$remit_data['beneficiary_street']:'';
		$beneficiary->setStreet($remit_data['beneficiary_street']);
		$beneficiary->setMobile($beneficiary_mobile);
		$remit_data['beneficiary_phone']=(isset($remit_data['beneficiary_phone']))?$remit_data['beneficiary_phone']:'';
	
		$beneficiary->setPhone($remit_data['beneficiary_phone']);
		$this->ci->doctrine->em->persist($beneficiary);
	
		if($remit_data['beneficiary_id_type']){
			$beneficiaryID = new CustomerIDocuments();
			$beneficiaryID->setDocType($this->ci->doctrine->em->find('models\Common\IdentificationDocument',$remit_data['beneficiary_id_type']));
			$beneficiaryID->setDocNumber($remit_data['beneficiary_id_no']);
			$beneficiaryID->setIssuedCountry($this->ci->doctrine->em->find('models\Common\Country',$remit_data['issued_country_beneficiary']));
			$beneficiaryID->setCustomer($beneficiary);
			$this->ci->doctrine->em->persist($beneficiaryID);
	
			$transaction->setBeneficiaryDocument($beneficiaryID);
		}
	
	
		$transaction->setBeneficiary($beneficiary);
		$transaction->setRemitter($remitter);
		$transaction->setRemittingAgent($remitting_agent);
		$transaction->setRemittingAmount($remit_data['sending_amount']);
		$transaction->setUSDAmount($remit_data['amount_in_usd']);
		$transaction->setNetComission($remit_data['commission']);
		$transaction->setDepositedAmount($remit_data['total_amount']);
		$transaction->setTargetAmount($remit_data['receiving_amount']);
		$transaction->setExchangeRate($remit_data['sdExchangeRate']);
		$transaction->setEffectiveExchangeRateRemitter($this->ci->doctrine->em->find('models\Common\ExchangeRate',$remit_data['remitting_exchange_id']));
		$transaction->setEffectiveExchangeRatePayout($this->ci->doctrine->em->find('models\Common\ExchangeRate',$remit_data['receiving_exchange_id']));
		$transaction->setRemittingCurrency($remitting_currency);
		$transaction->setReceivingCurrency($receiving_currency);
		$transaction->setUser($current_user);
		$transaction->setDestinationCountry($destination_country);
		$transaction->setRemittanceType($transaction_type);
	
	
		$remit_data['payout_sub_agent']='';
		if ($transaction instanceof IDPayment && $remit_data['payout_sub_agent'] != ''){
			$payout_agent = $this->ci->doctrine->em->find('models\Agent', $remit_data['payout_sub_agent']);
			$transaction->setPayoutAgent($payout_agent);
		}
	
		$tracking_number = $this->ci->transborder->nm->generateTrackingNumber(TRUE);
	//	$tracking_number=$remit_data['tracking_number'];
	
		$transaction->setTrackingNumber($tracking_number);
	
		if ($transaction instanceof BankPayment) {
			$transaction->setAccountName($remit_data['beneficiary_name']);
			$transaction->setAccountNumber($remit_data['beneficiary_account_no']);
			$transaction->setBankBranch($this->ci->doctrine->em->find('models\Common\BankBranch', $remit_data['beneficiary_bank_branch']));
		}
	
	
		$mtcn_normal=$remit_data['control_number'];
		/**
		 Delegate MTCN generation Service
		 */
		// 		$adapter = $this->getRemitAdapter($transaction);
	
		// 		try{
		// 			$mtcn_normal = $adapter->generateControlNumber($transaction);
		// 		}
		// 		catch(\Exception $e)
		// 		{
		// 			if($thirdPartyCall){
		// 				throw new \Exception($e->getMessage());
		// 			}
		// 			else{
		// 				$this->ci->message->set($e->getMessage(), 'error', TRUE, 'feedback');
		// 				redirect('remittance/newentry');
		// 			}
		// 		}
	
		$remit_data['purpose']=(isset($remit_data['purpose']))?$remit_data['purpose']:'';
		$remit_data['source']=(isset($remit_data['source']))?$remit_data['source']:'';
	
		$transactionSource = ($thirdPartyCall)? Transaction::TXN_SRC_THIRDPARTY : Transaction::TXN_SRC_NATIVE;
	
		$transaction->setControlNumber($mtcn_normal);
	
		$transaction->setStatus(Transaction::STATUS_PENDING);
		$transaction->setAmountInWord($remit_data['amount_in_words']);
		$transaction->setSource($transactionSource);
		$transaction->setPurpose($remit_data['purpose']);
		$transaction->setMoneySource($remit_data['source']);
		$aml_status = ($this->ci->session->userdata('aml_status')!= '')? $this->ci->session->userdata('aml_status') : 0;
	
		$transaction->setAmlStatus($aml_status);
	
	
		$this->ci->doctrine->em->persist($transaction);
	
		$note = $remit_data['remarks'].' (Transaction Created)';
	
		$this->ci->transborder->tm->createTransactionNote($transaction, $note, Transaction::STATUS_PENDING);
			
		$this->ci->doctrine->em->flush();
	
		if($transaction->id())
		{
			$this->ci->session->set_userdata('mtcn',$mtcn_normal);
	
			log_message('info','Trigger transaction entry with mtcn :: '.$mtcn_normal);
	
			\Events::trigger('transaction_entry',array('txn' => $transaction, 'MTCN' => $mtcn_normal));
	
			log_message('info','Event transaction_entry Fired finished');
	
			return $transaction;
		}else{
	
			if($thirdPartyCall)
			{
				throw new \Exception("Sorry, Unable to create Transaction.");
			}
	
			return FALSE;
		}
		
	}
	/* if(\options::get('use_ctbs') == '1'){
	 $this->ci->doctrine->em->flush();
	} */
	
	public function validateAML($senderDocuments = array(), $beneficiaryDocuments = array())
	{
		$aml_config = \Options::get('config_aml_enabled', FALSE);
		
		if(!$aml_config) return NULL;
		
		$amlRepo = $this->ci->doctrine->em->getRepository('models\AML');
		
		$amlResponse = $amlRepo->getAML($senderDocuments, $beneficiaryDocuments); 
		
		if($amlResponse == Transaction::AML_SAFE_TRANSACTION) 
		{
			$this->ci->session->set_userdata('aml_status', Transaction::AML_SAFE_TRANSACTION);
			return NULL;
		}
		else
		{
			$this->ci->session->set_userdata('aml_status', $amlResponse);
			$suspicious_sender = $amlRepo->getSuspiciousDetail($senderDocuments);
			$suspicious_beneficiary = $amlRepo->getSuspiciousDetail($beneficiaryDocuments);
			return array('aml_status' => $amlResponse,'suspicious_sender' => $suspicious_sender, 'suspicious_beneficiary' => $suspicious_beneficiary);
		}
		
		$this->ci->transborder->tm->createTransactionNote($transaction, $note, Transaction::STATUS_PENDING);
			
		$this->ci->doctrine->em->flush();
		if($transaction->id()){
				
			$this->ci->session->set_userdata('mtcn',$mtcn_normal);
				
			log_message('info','Trigger transaction entry with mtcn :: '.$mtcn_normal);
				
			\Events::trigger('transaction_entry',array('txn' => $transaction, 'MTCN' => $mtcn_normal));
				
			log_message('info','Event transaction_entry Fired finished');
				
			return $transaction;
		}else{
			return FALSE;
		}
	}
	
	
}