<?php

use Transborder\Transconnect\AlertService\Exceptions\TcAlertException;

class WrongCredentialException extends TcAlertException
{
	
}