<?php

namespace Transborder\Transconnect\AlertService\Exceptions;

class TcAlertException extends \Exception
{
	private $tcExceptionCode;
	
	public function __construct($message = null, $tcExceptionCode,$code = 0,\Exception $previous = null ){
		$this->tcExceptionCode = $tcExceptionCode;
		
		parent::__construct($message, $code, $previous);
	}
	
	public function getTcExceptionCode(){
		return $this->tcExceptionCode;
	}
}