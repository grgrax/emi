<?php

namespace Transborder\Transconnect\AlertService;

class NotifyStatusResponse {
	/**
	 * @access public
	 * @var tnsTransconnectAlertServiceStatus
	 */
	public $status;
	/**
	 * @access public
	 * @var string
	 */
	public $responseCode;
	/**
	 * @access public
	 * @var TransconnectAlertMapEntry[]
	 */
	public $responseFields;
}