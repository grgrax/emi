<?php

namespace Transborder\Transconnect\AlertService;

class TcAlertWatchdogRequest {
	/**
	 * @access public
	 * @var string
	 */
	public $username;
	/**
	 * @access public
	 * @var string
	 */
	public $password;
	/**
	 * @access public
	 * @var TransconnectAlertMapEntry[]
	 */
	public $requestFields;
}