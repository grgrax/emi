<?php
namespace Transborder\Common\Util;
use Doctrine\Common\EventArgs;

use Doctrine\Common\EventSubscriber;
class TransborderEventListener implements EventSubscriber{

	public function __construct(){

	}

	public function getSubscribedEvents(){
		return array('onFlush');
	}

	public function onFlush(EventArgs $args){

		/* @var $em \Doctrine\ORM\EntityManager
		 *
		* */

		$em = $args->getEntityManager();
		$uow = $em->getUnitOfWork();

		foreach ($uow->getScheduledEntityInsertions() AS $entity) {
				
			if($entity instanceof \models\Transaction\TransactionLog){

				$entity->setIpaddress(\CI::$APP->input->ip_address());
				
				if(\Current_User::user()){
					$entity->setUsername(\Current_User::user()->getUsername());
					$entity->setUser(\Current_User::user());
				}
				$uow->recomputeSingleEntityChangeSet($em->getClassMetaData(get_class($entity)),$entity);
			}
				
			if($entity instanceof \models\Commission\CommissionLog){
				$entity->setUsername(\Current_User::user()->getUsername());
				$uow->recomputeSingleEntityChangeSet($em->getClassMetaData(get_class($entity)),$entity);
			}
			if($entity instanceof \models\Agent\AgentLog){
				$entity->setUsername(\Current_User::user()->getUsername());
				$uow->recomputeSingleEntityChangeSet($em->getClassMetaData(get_class($entity)),$entity);
			}
				
		}
	}
}
