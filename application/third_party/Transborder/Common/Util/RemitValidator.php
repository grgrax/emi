<?php
namespace Transborder\Common\Util;


class RemitValidator{
	
	private $ci;
	
	private $errorMessage = NULL;
	
	
	public function __construct(){
		$this->ci =& \CI::$APP;
	}
	
	/**
	 * @param models\Agent $agent
	 * @param float $amount
	 * @return boolean
	 */
	public function validate($amount, $agent){
		if($amount > $agent->getPerTxnLimit()){
			$this->errorMessage = "Per Transaction Limit exceeded.";
			return FALSE;
		}
		
		/* @var $arepo \models\Repository\Agent\AgentRepository */
		$arepo = $this->ci->doctrine->em->getRepository('models\Agent');
		
		$remittedToday = $arepo->getTotalTransactionToday($agent);
		if(($amount + $remittedToday) > $agent->getDailyTxnLimit()){
			$this->errorMessage = "Daily limit exceeded.";
			return FALSE;
		}
		
		$remittedThisMonth = $arepo->getTotalTransactionThisMonth($agent);
		if(($amount + $remittedThisMonth) > $agent->getMonthlyTxnLimit()){
			$this->errorMessage = "Monthly limit exceeded.";
			return FALSE;
		}

		return TRUE;
		
	}
	
	public function getErrorMessage(){
		return $this->errorMessage;
	}
}
