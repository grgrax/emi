<?php

namespace Transborder\AMLParser;

class AMLManager{
	private $ci;
	
	public function __construct(){
		$this->ci = & \CI::$APP;
	}
	
	public function getAMLAdapter($AMLType, $filePath, $filePath2 = NULL, $filePath3 = NULL){
		
		$type = strtoupper($AMLType);
		switch($type){
			case 'UN' : return new \Transborder\AMLParser\AML\Adapters\UN\AMLAdapter($filePath); break;
// 			case 'OFAC' : return new \Transborder\AMLParser\AML\Adapters\OFAC\AMLAdapter($filePath); break;
			case 'OFAC' : return new \Transborder\AMLParser\AML\Adapters\OFAC\AMLAdapter($filePath, $filePath2, $filePath3); break;
			case 'EU'	: return new \Transborder\AMLParser\AML\Adapters\EU\AMlAdapter($filePath);break;
			case 'LOCAL':return  new \Transborder\AMLParser\AML\Adapters\LOCAL\AMLAdapter($filePath);break;
			default : break;
		}
		
		
	}
	
	
	public function insert($arrayData, $AMLType){

		$batchSize = 20;
		$arrayCount= 0;
		
		$amlRepo = $this->ci->doctrine->em->getRepository('models\AML');
		$amlAliasRepo = $this->ci->doctrine->em->getRepository('models\AMLAlias');
		$amlAddRepo = $this->ci->doctrine->em->getRepository('models\AMLAddress');
		$amlDocRepo = $this->ci->doctrine->em->getRepository('models\AMLDocument');
			
		$amlRepo->removeAML($AMLType);
		
		foreach($arrayData as $key => $val){
				
			$group_id = $arrayData[$key]['group_id'];
			$aml_type = $arrayData[$key]['aml_type'];
			
			$aml = new \models\AML;
			$aml->setAmlType($arrayData[$key]['aml_type']);
			$aml->setGroupId($arrayData[$key]['group_id']);
			$aml->setGroupType($arrayData[$key]['group_type']);
			$aml->setTitle($arrayData[$key]['title']);
			$aml->setReferenceNumber($arrayData[$key]['reference_number']);
			$aml->setFirstname($arrayData[$key]['first_name']);
			$aml->setMiddleName($arrayData[$key]['middle_name']);
			$aml->setLastName($arrayData[$key]['last_name']);
			$aml->setNameOriginalScript($arrayData[$key]['name_original_script']);
			$aml->setDobType($arrayData[$key]['dob_type']);
			$aml->setDob($arrayData[$key]['dob']);
			$aml->setDobPlace($arrayData[$key]['dob_place']);
			$aml->setCountry($arrayData[$key]['country']);
			$aml->setDesignation($arrayData[$key]['designation']);
			$aml->setNationality($arrayData[$key]['nationality']);
			$aml->setProgram($arrayData[$key]['program']);
			$aml->setListedOn($arrayData[$key]['listed_on']);
			$aml->setSortKey($arrayData[$key]['sort_key']);
			$aml->setRemarks($arrayData[$key]['remarks']);
			$this->ci->doctrine->em->persist($aml);
			$alias_arr = $arrayData[$key]['alias'];
			foreach($alias_arr as $ak => $av){
				$alias_type = trim($alias_arr[$ak]['alias_type']);
				$alias_name = trim($alias_arr[$ak]['alias_name']);
				$alias_remarks = trim($alias_arr[$ak]['remarks']);
				
				$alias = new \models\AMLAlias;
				if($alias_name != ''){
					$alias->setAml($aml);
					$alias->setAliasType($alias_type);
					$alias->setAliasName($alias_name);
					$alias->setRemarks($alias_remarks);
					$this->ci->doctrine->em->persist($alias);
				}
			}
				
			$address_arr = $arrayData[$key]['address'];
			foreach($address_arr as $adk=>$adv){
				
				$add_desc = trim($address_arr[$adk]['description']);
				$add_address = trim($address_arr[$adk]['address']);
				$add_country = trim($address_arr[$adk]['country']);
				$add_remarks = trim($address_arr[$adk]['remarks']);
				
				$address = new \models\AMLAddress;
				if($add_desc != '' || $add_address != '' || $add_country != '' || $add_remarks != ''){
					$address->setAml($aml);
					$address->setDescription($add_desc);
					$address->setAddress($add_address);
					$address->setCountry($add_country);
					$address->setRemarks($add_remarks);
					$this->ci->doctrine->em->persist($address);
				}
			}
				
			$doc_arr = $arrayData[$key]['document'];
			foreach($doc_arr as $dk=>$dv){
				
				$doc_title = trim($doc_arr[$dk]['title']);
				$doc_desc = trim($doc_arr[$dk]['description']);
				$doc_ic = trim($doc_arr[$dk]['issued_country']);
				$doc_remarks = trim($doc_arr[$dk]['remarks']);
				
				$document = new \models\AMLDocument;
				if($doc_desc != ''){
					$document->setAml($aml);
					$document->setTitle($doc_title);
					$document->setDescription($doc_desc);
					$document->setIssuedCountry($doc_ic);
					$document->setRemarks($doc_remarks);
					$this->ci->doctrine->em->persist($document);
				}
			}
			
			if($arrayCount % $batchSize == 0){ 
				$this->ci->doctrine->em->flush();
				$this->ci->doctrine->em->clear();
			}
			$arrayCount++;	
		}
		
		$this->ci->doctrine->em->flush();
		if($aml->id()) return TRUE;
		else return FALSE;
	}
	
	
	
}