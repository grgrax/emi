<?php

namespace Transborder\AMLParser\AML\Adapters\UN;

use models\AML;

class AMLAdapter implements \Transborder\AMLParser\AML{
	
	private $filepath;
	private $doctrine;
	
	public function __construct($file_path){
		$this->doctrine = & \CI::$APP->doctrine;
		$this->filepath = $file_path;
	}
	
	
	
	public function import($fileOrder=NULL){
		$doc = new \DOMDocument();
		$doc->load($this->filepath);
		
		if($doc->getElementsByTagName("CONSOLIDATED_LIST")->length == 0) return NULL;
		
		$individuals = $doc->getElementsByTagName("INDIVIDUALS");
		$entities = $doc->getElementsByTagName("ENTITIES");
		
		$individuals_data = $doc->getElementsByTagName("INDIVIDUAL");
		$enities_data = $doc->getElementsByTagName("ENTITY");
		
		$individual = array();
		
		foreach($individuals_data as $indata){
			
			$dataid = $indata->getElementsByTagName("DATAID");
			$version = $indata->getElementsByTagName("VERSIONNUM");
			$fname = ($indata->getElementsByTagName("FIRST_NAME")->length != 0)? $indata->getElementsByTagName("FIRST_NAME")->item(0)->nodeValue : '';
			$second_name = ($indata->getElementsByTagName("SECOND_NAME")->length != 0) ? $indata->getElementsByTagName("SECOND_NAME")->item(0)->nodeValue : '';
			$un_list_type = $indata->getElementsByTagName("UN_LIST_TYPE");
			$reference_number = $indata->getElementsByTagName("REFERENCE_NUMBER");
			$listed_on = $indata->getElementsByTagName("LISTED_ON");
			$name_original_script = ($indata->getElementsByTagName("NAME_ORIGINAL_SCRIPT")->length != 0)? $indata->getElementsByTagName("NAME_ORIGINAL_SCRIPT")->item(0)->nodeValue : '';
			$comments1 = $indata->getElementsByTagName("COMMENTS1");
			
			$designation = ($indata->getElementsByTagName("DESIGNATION")->length != 0)? $indata->getElementsByTagName("DESIGNATION")->item(0)->nodeValue : '';
			$title = ($indata->getElementsByTagName("TITLE")->length != 0)? $indata->getElementsByTagName("TITLE")->item(0)->nodeValue : '';
			$nationality = ($indata->getElementsByTagName("NATIONALITY")->length != 0)? $indata->getElementsByTagName("NATIONALITY")->item(0)->nodeValue : '';
			$list_type = ($indata->getElementsByTagName("LIST_TYPE")->length != 0)? $indata->getElementsByTagName("LIST_TYPE")->item(0)->nodeValue : '';
			$last_date_updated = ($indata->getElementsByTagName("LAST_DATE_UPDATED")->length != 0)? $indata->getElementsByTagName("LAST_DATE_UPDATED")->item(0)->nodeValue : '';
			
			$individual_alias = $indata->getElementsByTagName("INDIVIDUAL_ALIAS");
			$individual_alias_array = array();
			if($individual_alias->length != 0){
				foreach($individual_alias as $ia){
					$quality = ($ia->getElementsByTagName("QUALITY")->length != 0)? $ia->getElementsByTagName("QUALITY")->item(0)->nodeValue : '';
					$ialias = ($ia->getElementsByTagName("ALIAS_NAME")->length != 0)? $ia->getElementsByTagName("ALIAS_NAME")->item(0)->nodeValue : '';
					$note = ($ia->getElementsByTagName("NOTE")->length != 0)? $ia->getElementsByTagName("NOTE")->item(0)->nodeValue : '';
					$individual_alias_array[] = array('alias_type'=>$quality, 'alias_name'=>$ialias, 'remarks'=>$note);
				}
			}

			$address_arr = array();
			$address = $indata->getElementsByTagName("INDIVIDUAL_ADDRESS");
			if($address->length != 0){
				foreach($address as $add){
					$street = ($add->getElementsByTagName("STREET")->length != 0)? trim($add->getElementsByTagName("STREET")->item(0)->nodeValue) : '';
					$city = ($add->getElementsByTagName("CITY")->length != 0)? trim($add->getElementsByTagName("CITY")->item(0)->nodeValue) : '';
					$country = ($add->getElementsByTagName("COUNTRY")->length != 0)? trim($add->getElementsByTagName("COUNTRY")->item(0)->nodeValue) : '';
					$add_det = ($street != '')? $street.', '.$city : $city; 
					$address_arr[] = array('description'=>'','address'=>$add_det,'country'=>$country,'remarks'=>'');
				}
			}
			else $address_arr[] = array('description'=>'','address'=>'','country'=>'','remarks'=>'');
			
			$date_of_birth = array();
			$dobirth = $indata->getElementsByTagName("INDIVIDUAL_DATE_OF_BIRTH");
			if($dobirth->length != 0){
				foreach($dobirth as $dob){
					$type_of_date = ($dob->getElementsByTagName("TYPE_OF_DATE")->length != 0)? $dob->getElementsByTagName("TYPE_OF_DATE")->item(0)->nodeValue : '';
					$year = ($dob->getElementsByTagName("YEAR")->length != 0)? $dob->getElementsByTagName("YEAR")->item(0)->nodeValue : '';
				}
			}
			
			$place_of_birth = array();
			$pobirth = $indata->getElementsByTagName("INDIVIDUAL_PLACE_OF_BIRTH");
			if($pobirth->length != 0){
				foreach($pobirth as $pob){
					$p_city = ($pob->getElementsByTagName("CITY")->length !=0)? $pob->getElementsByTagName("CITY")->item(0)->nodeValue : '';
					$p_state = ($pob->getElementsByTagName("STATE_PROVINCE")->length !=0)? $pob->getElementsByTagName("STATE_PROVINCE")->item(0)->nodeValue : '';
					$p_country = ($pob->getElementsByTagName("COUNTRY")->length !=0)? $pob->getElementsByTagName("COUNTRY")->item(0)->nodeValue : '';
					$place_of_birth = array('city'=>$p_city, 'state'=>$p_state, 'country'=>$p_country);
				}
			}else $place_of_birth = array('city'=>'', 'state'=>'', 'country'=>'');
			

			$idoc = $indata->getElementsByTagName("INDIVIDUAL_DOCUMENT");
			$document = array();
			if($idoc->length !=  0){
				foreach($idoc as $id){
					if($id->getElementsByTagName('TYPE_OF_DOCUMENT')->length != 0 || $id->getElementsByTagName('NUMBER')->length != 0 || $id->getElementsByTagName('ISSUING_COUNTRY')->length != 0){
						$doc_type = ($id->getElementsByTagName('TYPE_OF_DOCUMENT')->length != 0)? trim($id->getElementsByTagName('TYPE_OF_DOCUMENT')->item(0)->nodeValue) : '';
						$doc_number = ($id->getElementsByTagName('NUMBER')->length != 0)? trim($id->getElementsByTagName('NUMBER')->item(0)->nodeValue) : '';
						$doc_country = ($id->getElementsByTagName('ISSUING_COUNTRY')->length != 0)? trim($id->getElementsByTagName('ISSUING_COUNTRY')->item(0)->nodeValue) : '';
						$document[]=array('title'=>$doc_type, 'description'=>$doc_number, 'issued_country'=>$doc_country, 'remarks'=>'');
					}
					else{
						$document[]=array('title'=>'', 'description'=>'', 'issued_country'=>'', 'remarks'=>'');
					}
				}
			}
			else $document[]=array('title'=>'', 'description'=>'', 'issued_country'=>'', 'remarks'=>'');

			$sort_key = ($indata->getElementsByTagName("SORT_KEY")->length != 0)? $indata->getElementsByTagName("SORT_KEY")->item(0)->nodeValue : '';
			$sort_key_last_mod = ($indata->getElementsByTagName("SORT_KEY_LAST_MOD")->length != 0)? $indata->getElementsByTagName("SORT_KEY_LAST_MOD")->item(0)->nodeValue : '';
			
			$individual[]= array(
						'group_id'=> trim($dataid->item(0)->nodeValue),
						'first_name' => trim($fname),
						'middle_name'=> '',
						'last_name' => trim($second_name),
						'un_list_type' => trim($un_list_type->item(0)->nodeValue),
						'reference_number' => trim($reference_number->item(0)->nodeValue),
						'listed_on' => trim($listed_on->item(0)->nodeValue),
						'name_original_script' => trim($name_original_script),
						'designation' => trim($designation),
						'title' => trim($title),
						'nationality' => trim($nationality),
						'alias' => $individual_alias_array,
						'address' => $address_arr,
						'dob_type' => $type_of_date,
						'dob' => $year,
						'dob_place' => $p_city.', '.$p_state,
						'country' => $p_country,
						'program' => '',
						'document' => $document,
						'sort_key' => trim($sort_key),
						'group_type' => 'individual',
						'aml_type'=> AML::AML_TYPE_UN,
						'remarks' =>  $comments1->item(0)->nodeValue,
					);

			

		} 
		
		$entity = array();
		foreach($enities_data as $ent){
			$eid = $ent->getElementsByTagName("DATAID");
			$eversion = $ent->getElementsByTagName("VERSIONNUM");
			$efname = ($ent->getElementsByTagName("FIRST_NAME")->length != 0)? $ent->getElementsByTagName("FIRST_NAME")->item(0)->nodeValue : '';
			$esecond_name = ($ent->getElementsByTagName("SECOND_NAME")->length != 0) ? $ent->getElementsByTagName("SECOND_NAME")->item(0)->nodeValue : '';
			$eun_list_type = $ent->getElementsByTagName("UN_LIST_TYPE");
			$ereference_number = $ent->getElementsByTagName("REFERENCE_NUMBER");
			$elisted_on = $ent->getElementsByTagName("LISTED_ON");
			$ename_original_script = ($ent->getElementsByTagName("NAME_ORIGINAL_SCRIPT")->length != 0)? $ent->getElementsByTagName("NAME_ORIGINAL_SCRIPT")->item(0)->nodeValue : '';
			$ecomments1 = $ent->getElementsByTagName("COMMENTS1");
			$edesignation = ($ent->getElementsByTagName("DESIGNATION")->length != 0)? $ent->getElementsByTagName("DESIGNATION")->item(0)->nodeValue : '';
			$etitle = ($ent->getElementsByTagName("TITLE")->length != 0)? $ent->getElementsByTagName("TITLE")->item(0)->nodeValue : '';
			$enationality = ($ent->getElementsByTagName("NATIONALITY")->length != 0)? $ent->getElementsByTagName("NATIONALITY")->item(0)->nodeValue : '';
			$elist_type = ($ent->getElementsByTagName("LIST_TYPE")->length != 0)? $ent->getElementsByTagName("LIST_TYPE")->item(0)->nodeValue : '';
			$elast_date_updated = ($ent->getElementsByTagName("LAST_DATE_UPDATED")->length != 0)? $ent->getElementsByTagName("LAST_DATE_UPDATED")->item(0)->nodeValue : '';
			
			$entity_alias = $ent->getElementsByTagName("ENTITY_ALIAS");
			$entity_alias_arr = array();
			if($entity_alias->length != 0){
				foreach($entity_alias as $ealias){
					$quality = ($ealias->getElementsByTagName("QUALITY")->length != 0)? $ealias->getElementsByTagName("QUALITY")->item(0)->nodeValue : '';
					$ialias = ($ealias->getElementsByTagName("ALIAS_NAME")->length != 0)? $ealias->getElementsByTagName("ALIAS_NAME")->item(0)->nodeValue : '';
					$note = ($ealias->getElementsByTagName("NOTE")->length != 0)? $ealias->getElementsByTagName("NOTE")->item(0)->nodeValue : '';
					$entity_alias_arr[] = array('alias_type'=>$quality, 'alias_name'=>$ialias, 'remarks'=>$note); 
				}
			}
			
			$entity_address = $ent->getElementsByTagName("ENTITY_ADDRESS");
			$entity_address_arr = array();
			if($entity_address->length != 0){
				foreach($entity_address as $ea){
					$note = ($ea->getElementsByTagName("NOTE")->length != 0)? $ea->getElementsByTagName("NOTE")->item(0)->nodeValue : '';
					if($note != ''){
						list($desc,$add) = explode(':',$note);
						$con_array = explode(',',$add);
						$con = end($con_array);
					}
					else{
						$desc=''; 
						$add='';
						$con ='';
					}
					$entity_address_arr[] = array('description'=>$desc,'address'=>$add,'country'=>$con,'remarks'=>'');
				}
			}
			else $entity_address_arr[] = array('description'=>'','address'=>'','country'=>'','remarks'=>'');
			
			$sort_key = ($ent->getElementsByTagName("SORT_KEY")->length != 0)? $ent->getElementsByTagName("SORT_KEY")->item(0)->nodeValue : '';
			$sort_key_last_mod = ($ent->getElementsByTagName("SORT_KEY_LAST_MOD")->length != 0)? $ent->getElementsByTagName("SORT_KEY_LAST_MOD")->item(0)->nodeValue : '';
			$edoc_arr[] = array('title'=>'', 'description'=>'', 'issued_country'=>'', 'remarks'=>'');
			
			$entity[] = array(
					'group_id' => trim($eid->item(0)->nodeValue),
					'first_name' => trim($esecond_name.' '.$efname),
					'middle_name' => '',
					'last_name' => '',
					'un_list_type' => trim($eun_list_type->item(0)->nodeValue),
					'reference_number' => trim($ereference_number->item(0)->nodeValue),
					'listed_on' => trim($elisted_on->item(0)->nodeValue),
					'name_original_script' => trim($ename_original_script),
					'designation' => trim($edesignation),
					'title' => trim($etitle),
					'nationality' => trim($enationality),
					'alias' => $entity_alias_arr,
					'address' => $entity_address_arr,
					'dob_type' => '',
					'dob' => '',
					'dob_place' => '',
					'country' => '',
					'program' =>'',
					'document' => $edoc_arr,
					'sort_key' => $sort_key,
					'group_type' => 'entities',
					'aml_type'=> AML::AML_TYPE_UN,
					'remarks' => trim($ecomments1->item(0)->nodeValue),
					);
		}
		
		$data = array_merge($individual, $entity);
		return $data;
		
	}
	
}