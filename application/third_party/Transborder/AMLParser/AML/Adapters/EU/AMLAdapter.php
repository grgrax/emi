<?php

namespace Transborder\AMLParser\AML\Adapters\EU;

use models\AML;

class AMLAdapter implements \Transborder\AMLParser\AML{
	
	private $filepath;
	private $doctrine;
	
	public function __construct($file_path){
		$this->doctrine = & \CI::$APP->doctrine;
		$this->filepath = $file_path;
	}
	
	public function import($fileOrder=NULL){
		require_once APPPATH."third_party/Excel/excel_reader2.php";
		$data = new \Spreadsheet_Excel_Reader($this->filepath);
		$data->setOutputEncoding('CP1251');
		$data = $data->sheets[0]['cells'];
		$data1= array_shift($data);
		$data2= array_shift($data);
		$arrdata = array();
		$groupID = array();
		$newEntry = TRUE;
		
		foreach ($data as $e){
			$name6				=	isset($e[1]) ? $e[1]:'';
			$name1				=	isset($e[2]) ? $e[2]:'';
			$name2				=	isset($e[3]) ? $e[3]:'';
			$name3				=	isset($e[4]) ? $e[4]:'';
			$name4				=	isset($e[5]) ? $e[5]:'';
			$name5				=	isset($e[6]) ? $e[6]:'';
			$title				=	isset($e[7]) ? $e[7]:'';
			$dob				=	isset($e[8]) ? $e[8]:'';
			$town_O_B			=	isset($e[9]) ? $e[9]:'';
			$country_O_B		=	isset($e[10]) ? $e[10]:'';
			$nationality		=	isset($e[11]) ? $e[11]:'';
			$passport_details	=	isset($e[12]) ? $e[12]:'';
			$NI_no				=	isset($e[13]) ? $e[13]:'';
			$postion 			= 	isset($e[14]) ? $e[14]:'';
			$address1			=	isset($e[15]) ? $e[15]:'';
			$address2			=	isset($e[16]) ? $e[16]:'';
			$address3			=	isset($e[17]) ? $e[17]:'';
			$address4			=	isset($e[18]) ? $e[18]:'';
			$address5			=	isset($e[19]) ? $e[19]:'';
			$address6			=	isset($e[20]) ? $e[20]:'';
			$post_zip_code		=	isset($e[21]) ? $e[21]:'';
			$country			=	isset($e[22]) ? $e[22]:'';
			$otherInfo			=	isset($e[23]) ? $e[23]:'';
			$group_type			=	isset($e[24]) ? $e[24]:'';
			$alias_type			=	isset($e[25]) ? $e[25]:'';
			$regime				=	isset($e[26]) ? $e[26]:'';
			$listed_on			=	isset($e[27]) ? $e[27]:'';
			$last_update		=	isset($e[28]) ? $e[28]:'';
			$group_id			=	isset($e[29]) ? $e[29]:'';
			
			$first_name= ($group_type == 'Individual')?$name1:$name6;
			$last_name = ($group_type == 'Individual')?$name6:'';
			if($first_name !==''){
			
			$alias  	= array('alias_type'=>$alias_type, 'alias_name'=>$name3.' '.$name4.' '.$name5, 'remarks'=>'');
			$address	= array('description'=>$address4.' '.$address5,
									'address'=>$address1.' '.$address2.' '.$address3.' '.$address6,
									'country'=>$country,
									'remarks'=>$post_zip_code);
			$passport=array('title'=>'passport','description'=>$passport_details,'remarks'=>'','issued_country'=>''); 
			$ni = array('title'=>'national identification','description'=>$NI_no,'remarks'=>'','issued_country'=>''); 
			$documents=  array($passport,$ni);
			
			$arrdata[] 	= array(
								'aml_type'				=> AML::AML_TYPE_EU,
								'group_id' 				=> $group_id,
								'group_type'			=> $group_type,
								'reference_number'		=> '',
								'title'					=> $title,
								'first_name'			=> $first_name,
								'middle_name'			=> $name2,
								'last_name'				=> $last_name,
								'name_original_script' 	=> '',
								'dob_type'				=> '',
								'dob'					=> $dob,
								'dob_place'				=> $town_O_B,
								'country'				=> $country_O_B,
								'designation'			=> $postion,
								'nationality'			=> $nationality,
								'document' 				=> $documents,
								'address'				=> array($address),
								'alias'					=> array($alias),
								'program'				=> $regime,
								'listed_on'				=> $listed_on,
								'sort_key'				=> '',
								'remarks'				=> $otherInfo,
									);
			}
		}
		return $arrdata;
	}
	
	
	
}