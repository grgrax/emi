<?php


use App\Calculator;
use models\Discount;
use models\Rate;

class Installment_Controller extends Front_Controller{
	

	public function __construct(){
		parent :: __construct();
	}

	public function index(){		
		try {
			$installments=$this->doctrine->em->getRepository('models\Installment')->findAll();
			$this->_t['installments'] = $installments;
			$this->_t['maincontent'] = "installment/index";
			$this->load->theme("main", $this->_t);
		}
		catch (Exception $e) {
			$this->message->set($e->getMessage(), 'error', true, 'feedback');
			redirect('emi');			
		}
	}

	public function view($id){		
		try {
			$installmentDetails=$this->doctrine->em->find('models\Installment',$id);
			$this->_t['installmentDetails'] = $installmentDetails;
			$this->_t['maincontent'] = "installment/view";
			$this->load->theme("main", $this->_t);
		}
		catch (Exception $e) {
			$this->message->set($e->getMessage(), 'error', true, 'feedback');
			redirect('emi');			
		}
	}



}

