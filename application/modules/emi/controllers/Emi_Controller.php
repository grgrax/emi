<?php

use App\Calculator;
use App\FileGenerator;
use models\Rate;
use models\Installment;

class Emi_Controller extends Front_Controller{
	

	private $loanDispursementDate,$loanMaturityDate;
	
	private $__amount;
	private $__interest;
	private $__tenture;
	private $__age;
	private $__emiStarDate;
	private $__premiumPaymentMode;
	private $__AccidentalDeathBenefit=0;
	private $__TotalPermanentDisabilityBenefit=0;
	private $__premiumRate;
	private $__largeSumDiscount;
	private $__afterDiscount;
	private $__annualPremium;
	private $__discountOnAnnualPayment;
	private $__permiumAfterDiscount;
	private $__lessCorporateDiscount;
	private $__adbAmount;
	private $__tpdpwAmount;
	private $__monthlyPremiumAmount;
	private $__t24Username;    
	private $__t24Password;    
	private $__branchCode;    
	private $__customerIdentificationNumber;    
	private $__loanDisbursementAccount;    
	private $__pricipalLiquidationAccount;	    
	private $__interestLiquidationAccount;
	private $__installments;    

	private $__apiRequestData;    
	private $__apiResponseData;    
	private $__apiResponseCusomterNo;    

	private $ii;


	public function __construct(){
		try {
			parent :: __construct();
			$this->load->library('form_validation');
			$this->load->helper('emi');

		//get min and max
			$minyear=$this->doctrine->em->getRepository('models\Rate')->getMin(array('column'=>'year'));
			$maxyear=$this->doctrine->em->getRepository('models\Rate')->getMax(array('column'=>'year'));
			$min['year']=$minyear[0];
			$max['year']=$maxyear[0];
			$minage=$this->doctrine->em->getRepository('models\Rate')->getMin(array('column'=>'age'));
			$maxage=$this->doctrine->em->getRepository('models\Rate')->getMax(array('column'=>'age'));
			$min['age']=$minage[0];
			$max['age']=$maxage[0];
			$this->_t['min']=$min;
			$this->_t['max']=$max;

		//get discounts 
			$discounts=$this->doctrine->em->getRepository('models\Discount')->findAll();
			if(!$discounts) throw new Exception("Error Processing Request, Couldn't load discount from database", 1);
			$this->_t['discounts']=$discounts;			
		} catch (Exception $e) {
			echo $e->getMessage();			
		}
	}
	

	public function index(){
		try {

			$this->_t['result'] =false;
			$this->_t['cbsSuccess'] = false;			
			$this->_t['cbsErrorMsg']=$this->_t['cbsSuccessMsg'] = '';

			if($this->input->post()){				
				$this->_t['param']=$this->input->post();
				$this->form_validation->set_rules('principal', "Loan Amount", "required|number|greater_than[0]");
				$this->form_validation->set_rules('interest', "Interest Rate", "required|greater_than[0]");								
				$this->form_validation->set_rules('term', "Loan Tenure", "required|number|greater_than[8]|less_than[21]");
				$this->form_validation->set_rules('age', "Applicant's Age", "required|number|greater_than[17]|less_than[51]");
				$this->form_validation->set_rules('start_date', "Loan Disbursement Date", "required");
				$this->form_validation->set_rules('payment_mode', "Premium Payment Mode", "required|number");				
				if($this->form_validation->run($this))
				{
					$this->_t['result'] =true;					
					$result=$this->calculateInstallments();
					if(array_key_exists('error', $result)) throw new Exception('Unable to generate installments, '.$result['error'], 1);					
					if(!array_key_exists('installments', $result)) throw new Exception("Unable to generate installments", 1);					
					if(!array_key_exists('loanSchedule', $result)) throw new Exception("Unable to generate loanSchedule", 1);					
					$this->_t['installments'] = $result['installments'];	
					$this->__installments=$this->_t['installments'];    

					$this->_t['loanSchedule'] = $result['loanSchedule'];			
					
					//export
					$time=date('l jS \of F Y h:i:s A');
					$this->_t['time']=$time;
					$file_name=date('d-m-Yh:i:sA');
					if($this->input->post('export')){
						$this->output->set_header('Content-type: application/octet-stream');
						$this->output->set_header("Content-Disposition: attachment; filename=emi_schedule_$file_name.xls");
						$this->output->set_header("Pragma: no-cache");
						$this->output->set_header("Expires: 0");
						$this->load->view('admin/export', $this->_t);
						return;
					}
					//generte pdf
					if($this->input->post('generate_pdf')){
						$html=$this->load->view("admin/export",$this->_t,true);						
						$dompdf = new DOMPDF();
						$dompdf->load_html($html);
						$dompdf->render();
						$dompdf->stream("$file_name.pdf");
						return;
					}					
					// if($this->input->post('generate_file')){
					// 	$data=$this->generateFile();
					// 	$name = "file_$file_name.txt";
					// 	$this->load->helper('download');
					// 	force_download($name, $data);
					// }					
					if($this->input->post('send_to_cbs')){
						$data=$this->generateFile();

						show_pre($data);
							

						$url = "10.10.12.53:6080/bank-connect-web/webresources/info";    
						$curl = curl_init($url);
						curl_setopt($curl, CURLOPT_HEADER, false);
						curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
						curl_setopt($curl, CURLOPT_HTTPHEADER,array("Content-type: application/text"));
						curl_setopt($curl, CURLOPT_POST, true);
						curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
						$result = curl_exec($curl);
						$result=json_decode($result,true);
						
						// show_pre($result);

						if($result['success']){
							$responseData=explode(',', $result['response']);
							// echo count($responseData);
							$this->_t['cbsSuccess'] = true;
							if(count($responseData)>20){
								foreach ($responseData as $key => $value) {
								//echo "<br/>$key--$value";
								if($key==0){
									$codeId=$value;
									if(!$codeId) throw new Exception("No loand code/ID", 1);                                                                  
                                                                                $this->__apiRequestData=$data;
                                                                                $this->__apiResponseData=$responseData;
                                                                                $this->__apiResponseCusomterNo=$codeId;
                                                                                $this->save();
                                                                                $this->_t['cbsSuccessMsg'] = "Your loan is opened with code/ID {$codeId}, Please verify from T24";
									continue;

								}
								
								/*		
									if (strpos($value,'LIMIT.REFERENCE=') !== false) {
										$codeId=str_replace('LIMIT.REFERENCE=', ' ', $value);
								
										if(!$codeId) throw new Exception("No loand code/ID", 1);									
										$this->__apiRequestData=$data;
										$this->__apiResponseData=$responseData;
										$this->__apiResponseCusomterNo=$codeId;									
										//$this->save();
										$this->_t['cbsSuccessMsg'] = "Your loan is opened with code/ID {$codeId}, Please verify from T24";
									}
								*/
								}								
							}else{
								$this->_t['cbsErrorMsg'] = $result['response'];								
							}
						}else{
							$this->_t['cbsErrorMsg'] = $result['response'];															
						}

					}					
				}
			}
			$this->_t['maincontent'] = "emi/emi";
			$this->load->theme("main", $this->_t);				
		}
		catch (Exception $e) {
			$this->message->set($e->getMessage(), 'error', true, 'feedback');
			redirect(current_url());
		}
	}

	private function generateFile(){
		$this->_t['result'] =false;
		if($this->input->post()){
			$this->_t['param']=$this->input->post();
			$this->form_validation->set_rules('customer_username', "Customer Username", "required");				
			$this->form_validation->set_rules('customer_password', "Customer Password", "required");				
			$this->form_validation->set_rules('branch_code', "Bracnh Code", "required|exact_length[9]");	

			$this->form_validation->set_rules('customer', "Customer Identification Number", "required|numeric|greater_than[0]|exact_length[8]");				
				// $this->form_validation->set_rules('currency', "Currency", "required|greater_than[0]");
			$this->form_validation->set_rules('interest_liquidation_account', "Interest Liquidation Account", "required|numeric|greater_than[0]||exact_length[14]");							
			$this->form_validation->set_rules('loan_disbursment_account', "Loan Disbursment Account", "required|numeric|greater_than[0]|exact_length[14]");				
			$this->form_validation->set_rules('principal_liquidation_account', "Principal Liquidation Account", "required|numeric|greater_than[0]|exact_length[14]");
			$this->form_validation->set_rules('interest_liquidation_account', "Interest Liquidation Account", "required|numeric|greater_than[0]|exact_length[14]");								
			if($this->form_validation->run($this))
			{
				$this->_t['result'] =true;					

				$result=$this->calculateInstallments();
				if(!array_key_exists('installments', $result)) throw new Exception("Unable to generate installments", 1);					

				$installments=$result['installments'];
				$this->_t['installments']=$installments;
				$this->__installments=$this->_t['installments'];    



				$time=date('l jS \of F Y h:i:s A');
				$this->_t['time']=$time;
				$file_name=date('d-m-Yh:i:sA');


				$branchCode=$this->input->post('branch_code')?:null;
				$customerUsername=$this->input->post('customer_username')?:null;
				$customerPassword=$this->input->post('customer_password')?:null;
				$customerId=$this->input->post('customer')?:null;
				$loan_disbursment_account=$this->input->post('loan_disbursment_account')?:null;
				$principal_liquidation_account=$this->input->post('principal_liquidation_account')?:null;
				$interest_liquidation_account=$this->input->post('interest_liquidation_account')?:null;


				$this->__t24Username=$customerUsername;    
				$this->__t24Password=$customerPassword;    
				$this->__branchCode=$branchCode;    
				$this->__customerIdentificationNumber=$customerId;    
				$this->__loanDisbursementAccount=$loan_disbursment_account;    
				$this->__pricipalLiquidationAccount=$principal_liquidation_account;   
				$this->__interestLiquidationAccount=$interest_liquidation_account;   

				if(!$installments) throw new Exception("No installments", 1);							
				if(!$customerId) throw new Exception("No customer id", 1);								
				
				if(!$this->__amount) throw new Exception("No amount", 1);								
				if(!$this->loanDispursementDate) throw new Exception("No loan dispursement Date", 1);								

				$loanMaturityDate=new DateTime($installments[count($installments)-1]['due_date']);
				$this->loanMaturityDate=$loanMaturityDate;

				if(!$loanMaturityDate) throw new Exception("No loanMaturityDate", 1);								
				if(!$this->__interest) throw new Exception("No interest", 1);								
				if(!$loan_disbursment_account) throw new Exception("No loan_disbursment_account", 1);								
				if(!$principal_liquidation_account) throw new Exception("No principal liquidation account", 1);								
				if(!$interest_liquidation_account) throw new Exception("No interest liquidation account", 1);




				$data=$this->getFileDetails($installments,$branchCode,$customerUsername,$customerPassword,$customerId,
					$loan_disbursment_account,$principal_liquidation_account,$interest_liquidation_account);
				if(!$data) throw new Exception("Unable to generate file", 1);							

				return $data;
			}
		}
	}

	private function calculateInstallments(){

		try {

			$this->__age=$age=intval($this->input->post('age'));
			$this->__tenture=$term=intval($this->input->post('term'));
			$this->__amount=$amount=intval($this->input->post('principal'));
			$this->__interest=$interest=floatval($this->input->post('interest'));
			$this->loanDispursementDate=$loanDispursementDate=new DateTime($this->input->post('start_date'));
			$this->__emiStarDate=$emistartDate=new DateTime($this->input->post('emi_start_date'));
			$payment_mode=floatval($this->input->post('payment_mode'));


			



		//emi calculator
			$emi=array();
			$discount=$this->doctrine->em->getRepository('models\Discount')->find($payment_mode);
			if(!$discount) throw new Exception("Error Processing Request, Couldn't load discount from databse", 1);	
			$this->__premiumPaymentMode=$discount;

			$emi['paymentDiscount']=floatval($discount->getPaymentDiscount());
			$emi['corporateDiscount']=floatval($discount->getCorporateDiscount());

			$rateObj=$this->doctrine->em->getRepository('models\Rate')->findOneBy(array('age'=>$age,'year'=>$term));
			$mappedRate=$rateObj->getRate();
			if(!$mappedRate) throw new Exception("Couldn't load Rate from databse, Invalid Loan Tenure ($term) or age ($age)", 1);					

			$this->__premiumRate=$mappedRate;

			$emi['age']=$age;
			$emi['term']=$term;
			$emi['principal']=$amount;
			$emi['interest']=$interest;
			$emi['mappedRate']=$mappedRate;
			$emi['largeSumDiscount']=0;										
			$emi['largeSumDiscount']=$this->getLargeSumDiscount($amount);

			$this->__largeSumDiscount=$emi['largeSumDiscount'];
			
			$emi['afterDiscount']=$mappedRate-$emi['largeSumDiscount'];
			$this->__afterDiscount=$emi['afterDiscount'];

			$emi['annualPremium']=($emi['afterDiscount']*$amount/1000);
			$this->__annualPremium=$emi['annualPremium'];

			$emi['discountOnAnnualPayment']=($emi['annualPremium']*$emi['paymentDiscount']/100);
			$this->__discountOnAnnualPayment=$emi['discountOnAnnualPayment'];

			$emi['premiumAfterDiscount']=$emi['annualPremium']+$emi['discountOnAnnualPayment'];
			$this->__permiumAfterDiscount=$emi['premiumAfterDiscount'];

			$emi['lessCorporateDiscount']=$emi['premiumAfterDiscount']*$emi['corporateDiscount']/100;		
			$this->__lessCorporateDiscount=$emi['lessCorporateDiscount'];


		//benefits
			$benefitAmont=0;
			if($this->input->post('benefiet')==1 or $this->input->post('benefiet')==2){

				$this->__AccidentalDeathBenefit=true;

				$benefitAmontOne=$amount*0.1/100;
				if($benefitAmontOne>=3000){
					$benefitAmontOne=3000;
				}
				$emi['benefitAmontOne']=$benefitAmontOne;
				$this->__adbAmount=$emi['benefitAmontOne'];

				$benefitAmont+=$benefitAmontOne;
				if($this->input->post('benefiet')==2){
					$benefitAmontTwo=$amount*0.1/100;
					if($benefitAmontTwo>=3000){
						$benefitAmontTwo=3000;
					}
					$emi['benefitAmontTwo']=$benefitAmontTwo;
					$this->__tpdpwAmount=$emi['benefitAmontTwo'];
					$benefitAmont+=$benefitAmontTwo;
					$this->__TotalPermanentDisabilityBenefit=true;
				}						
			}
			$emi['benefitAmont']=$benefitAmont;
			$emi['monthlyPreiumAmount']=round(($emi['premiumAfterDiscount']+$emi['lessCorporateDiscount']+$benefitAmont)/12,2);
			$this->__monthlyPremiumAmount=$emi['monthlyPreiumAmount'];


			$this->_t['emi'] = $emi;
		//emi calculator

		//loan schedule
			$installment=0;
			$loanSchedule=array();
			$loanSchedule['age']=$age;
			$loanSchedule['term']=$term;
			$loanSchedule['amount']=$amount;
			$loanSchedule['interest']=$interest;
			$loanSchedule['date']=$loanDispursementDate;

			$calculator=new Calculator;
			$ourRate=2;
			$installment=round($calculator->calculate($amount, $term, $interest, $loanDispursementDate,$emistartDate),$ourRate);

			if($installment<=0) throw new Exception("Error Processing Request, invalid installment", 1);					

			$this->_t['loanSchedule'] = $loanSchedule;

		//get installments
			$rate=$interest/100;
			$term=$emi['term'];
			$totalMonths=$totalMonths = $term * 12;
			$installments=adjustInstallmentAmount($installment, $amount, $loanDispursementDate,$emistartDate, $totalMonths, $rate);					


		//hack principal 
			$last=count($installments);
			$p=0;
			foreach ($installments as $installment) {
				$p=$p+$installment['principal'];
			}

			if($p!=$amount){
				if($p>$amount){
					$diff=$p-$amount;
					$previous_p=$installments[$last-2]['principal'];
					$new_p=$previous_p-$diff;
					$installments[$last-2]['principal']=round($new_p,2);
				}
				elseif($amount>$p){
					$diff=$amount-$p;
					$previous_p=$installments[$last-2]['principal'];
					$new_p=$previous_p+$diff;
					$installments[$last-2]['principal']=round($new_p,2);
				}
			}
		//hack principal 


			return array('installments'=>$installments,'loanSchedule'=>$loanSchedule);

		} catch (Exception $e) {
			return array('error'=>$e->getMessage());
		}	

	}


	public function getLargeSumDiscount($amount){
		$largeSumDiscount=0;
		if($amount<=500000){
			$largeSumDiscount=0;
		}
		elseif(500000<$amount and $amount<=2500000 ){
			$largeSumDiscount=0.5;
		}
		elseif(2500000<$amount and $amount<=5000000 ){
			$largeSumDiscount=1;
		}
		elseif(5000000<$amount and $amount<=10000000 ){
			$largeSumDiscount=1.5;
		}
		elseif(10000000<$amount){
			$largeSumDiscount=2;
		}
		return $largeSumDiscount;
	}

	public function export(){
		
		show_pre($this->_t['installments']);
		die;
		$name=date('l jS \of F Y h:i:s A');
		$this->output->set_header('Content-type: application/octet-stream');
		$this->output->set_header("Content-Disposition: attachment; filename=emi_schedule_$name.xls");
		$this->output->set_header("Pragma: no-cache");
		$this->output->set_header("Expires: 0");
		$this->load->view('admin/export', $this->_t);
		return;
	}


	private function getFileDetails($installments,$branchCode,$customerUsername,$customerPassword,$customerId,$drawDownAccount,$prinLiqAcct,$intLiqAcct){
		try {


			$filegenerator=new FileGenerator;
			
			// $_currency='NPR';

			$_branchCode=$branchCode;
			$_customerId=$customerId;
			$_currency='NPR';
			$_drawDownAccount=$drawDownAccount;
			$_prinLiqAcct=$prinLiqAcct;
			$_intLiqAcct=$intLiqAcct;

			$_amount=$this->__amount;
			$_intersetRate=$this->__interest;
			$_valueDate=$this->loanDispursementDate->format('Ymd');
			$_finMatDate=$this->loanMaturityDate->format('Ymd');

			//prepare loan part
			$filegenerator->prepareloanPart($branchCode,$customerUsername,$customerPassword,$_customerId,$_currency,$_amount,$_valueDate,$_finMatDate,$_intersetRate,$_drawDownAccount,$_prinLiqAcct,
				$_intLiqAcct);
			$loanPart=$filegenerator->getloanPart();
			// show_pre($loanPart);

			//prepare filedata of loan part
			$fileHeadData='';
			foreach ($loanPart as $key => $value) {
				if($key=='no_head' or $key=='no_username' or $key=='no_password' or $key=='no_branch')
					$data=$value;
				elseif($key=='LIM.REV')
					$data="$key:=$value";
				elseif($key=='FORWARD.BACKWARD' or $key=='BASE.DATE.KEY')
					$data="$key::=$value,";
				else{
					$data="$key:=$value,";
				}
				$fileHeadData.=$data;
				// show_pre($fileHeadData);
			}

			// show_pre($fileHeadData);
			// die;

			//file loan schedule part 2
			$fileInstallments='';
			$c=1;
			$row=1;

			// $installments=array(
			// 	array('due_date' => '2014-07-10','interest_recovered' => '1','principal'=>'1' ),
			// 	array('due_date' => '2014-08-10','interest_recovered' => '2','principal'=>'0' ),
			// 	array('due_date' => '2014-09-10','interest_recovered' => '3','principal'=>'0' ),
			// 	);


		/*	foreach ($installments as $key => $value) {				

				$i=1;
				$date=str_replace("-","",$value['due_date']);

				// echo "row:$row===count".count($installments)."<hr/>";

				$comma=",";
				if($row===count($installments) && $value['principal']==0){
					//no comma if last row and principal = 0 for I
					$comma="";
				}

				$fileInstallments.="SCH.TYPE:$c:$i:=I,DATE:$c:$i:=$date,AMOUNT:$c:$i:={$value['interest_recovered']}$comma";
				// echo $fileInstallments;
				if($value['principal']>0){
					$c++;
					if($row===count($installments)){
					//no comma if last row 
						$comma="";
					} 
					$fileInstallments.="SCH.TYPE:$c:$i:=P,DATE:$c:$i:=$date,AMOUNT:$c:$i:={$value['principal']}$comma";
					// echo "<br/>".$fileInstallments;
				}
				// echo "<hr/>";
				$c++;
				$row++;
			}
		*/
			foreach ($installments as $key => $value) {	
				$m = $value['installment_number'];			
				$i=1;
				$date=str_replace("-","",$value['due_date']);
				// echo "row:$row===count".count($installments)."<hr/>";
				$comma=",";
				if($row===count($installments) && $value['principal']==0){
					//no comma if last row and principal = 0 for I
					$comma="";
				}
				if($m%12==0){
					$interest_recovered = round($value['installment']-$value['principal'],2);
					$fileInstallments.="SCH.TYPE:$c:$i:=I,DATE:$c:$i:=$date,AMOUNT:$c:$i:={$interest_recovered}$comma";					
				}else{
					$fileInstallments.="SCH.TYPE:$c:$i:=I,DATE:$c:$i:=$date,AMOUNT:$c:$i:={$value['interest_recovered']}$comma";					
				}
				// echo $fileInstallments;
				if($value['principal']>0){
					$c++;
					if($row===count($installments)){
						//no comma if last row 
						$comma="";
					} 
					$fileInstallments.="SCH.TYPE:$c:$i:=P,DATE:$c:$i:=$date,AMOUNT:$c:$i:={$value['principal']}$comma";
					// echo "<br/>".$fileInstallments;
				}
				// echo "<hr/>";
				$c++;
				$row++;
			}
			//finalize data 
			$finalFileData=$fileHeadData.$fileInstallments;

			if($finalFileData)	
				return $finalFileData;
			else 
				return false;

		} catch (Exception $e) {
			die($e->getMessage());
		}
	}

	public function show(){
		$class=get_class($installment);
		$m=get_class_methods($class);
		foreach($m as $key => $value) {
			if(strpos($value,'get')!==false){
				$method=$value."()";
				$class=$this->ii;
				echo $class->$method;
			}
		}
		die;
	}

	private function save(){

		
		try{
		
	
		$installment=new Installment;
		
		$installment->setAmount($this->__amount);
		$installment->setInterest($this->__interest);
		$installment->setTenture($this->__tenture);
		$installment->setAge($this->__age);
		$installment->setLoanDispbursementDate($this->loanDispursementDate);
		
		$installment->setEmiStarDate($this->__emiStarDate);
		$installment->setAccidentalDeathBenefit($this->__AccidentalDeathBenefit);
		$installment->setTotalPermanentDisabilityBenefit($this->__TotalPermanentDisabilityBenefit);
		
		$installment->setLargeSumDiscount($this->__largeSumDiscount);
		$installment->setAfterDiscount($this->__afterDiscount);
		$installment->setAnnualPremium($this->__annualPremium);
		$installment->setDiscountOnAnnualPayment($this->__discountOnAnnualPayment);
		$installment->setPermiumAfterDiscount($this->__permiumAfterDiscount);
		$installment->setLessCorporateDiscount($this->__lessCorporateDiscount);
		$installment->setAdbAmount($this->__adbAmount);
		$installment->setTpdpwAmount($this->__tpdpwAmount);
		$installment->setMonthlyPremiumAmount($this->__monthlyPremiumAmount);
		$installment->setT24Username($this->__t24Username);
		$installment->setT24Password($this->__t24Password);
		$installment->setBranchCode($this->__branchCode);
		$installment->setCustomerIdentificationNumber($this->__customerIdentificationNumber);
		$installment->setLoanDisbursementAccount($this->__loanDisbursementAccount);
		$installment->setInstallments($this->__installments);


		$installment->setPrincipalLiquidationAccount($this->__pricipalLiquidationAccount);
		$installment->setInterestLiquidationAccount($this->__interestLiquidationAccount);
		
		$installment->setPremiumRate($this->__premiumRate);
		$installment->setPremiumPaymentMode($this->__premiumPaymentMode);

		$installment->setApiRequestData($this->__apiRequestData);
		$installment->setApiResponseData($this->__apiResponseData);
		$installment->setApiCustomerNo($this->__apiResponseCusomterNo);

		$this->doctrine->em->persist($installment);

		// echo "getId:".$installment->getId(); echo "<br/>";
		// echo "getPremiumPaymentMode:".$installment->getPremiumPaymentMode()->id(); echo "<br/>";
		// echo "getAmount:".$installment->getAmount(); echo "<br/>";
		// echo "getInterest:".$installment->getInterest(); echo "<br/>";
		// echo "getTenture:".$installment->getTenture(); echo "<br/>";
		// echo "getAge:".$installment->getAge(); echo "<br/>";
		// echo "getLoanDispbursementDate:".$installment->getLoanDispbursementDate()->format('d-m-Y'); echo "<br/>";
		// echo "getEmiStarDate:".$installment->getEmiStarDate()->format('d-m-Y'); echo "<br/>";
		// echo "getAccidentalDeathBenefit:".$installment->getAccidentalDeathBenefit(); echo "<br/>";
		// echo "getTotalPermanentDisabilityBenefit:".$installment->getTotalPermanentDisabilityBenefit(); echo "<br/>";
		// echo "getPremiumRate:".$installment->getPremiumRate(); echo "<br/>";
		// echo "getLargeSumDiscount:".$installment->getLargeSumDiscount(); echo "<br/>";
		// echo "getAfterDiscount:".$installment->getAfterDiscount(); echo "<br/>";
		// echo "getAnnualPremium:".$installment->getAnnualPremium(); echo "<br/>";
		// echo "getDiscountOnAnnualPayment:".$installment->getDiscountOnAnnualPayment(); echo "<br/>";
		// echo "getPermiumAfterDiscount:".$installment->getPermiumAfterDiscount(); echo "<br/>";
		// echo "getLessCorporateDiscount:".$installment->getLessCorporateDiscount(); echo "<br/>";
		// echo "getAdbAmount:".$installment->getAdbAmount(); echo "<br/>";
		// echo "getTpdpwAmount:".$installment->getTpdpwAmount(); echo "<br/>";
		// echo "getMonthlyPremiumAmount:".$installment->getMonthlyPremiumAmount(); echo "<br/>";
		// echo "getT24Username:".$installment->getT24Username(); echo "<br/>";
		// echo "getT24Password:".$installment->getT24Password(); echo "<br/>";
		// echo "getBranchCode:".$installment->getBranchCode(); echo "<br/>";
		// echo "getCustomerIdentificationNumber:".$installment->getCustomerIdentificationNumber(); echo "<br/>";
		// echo "getLoanDisbursementAccount:".$installment->getLoanDisbursementAccount(); echo "<br/>";
		// echo "getInterestLiquidationAccount:".$installment->getInterestLiquidationAccount(); echo "<br/>";
		
		// echo "<hr/>getInstallments".$installment->getInstallments(); echo "<br/>";
		// echo "<hr/>getApiRequestData".$installment->getApiRequestData(); echo "<br/>";
		// echo "<hr/>getApiResponseData".$installment->getApiResponseData(); echo "<br/>";

		// echo $installment->getCreatedAt()->format('d-m-Y');; echo "<br/>";
		// echo "getApiCustomerNo".$installment->getApiCustomerNo(); echo "<br/>";


		// $m=get_class_methods($installment);
		// foreach($m as $key => $value) {
		// 	if(strpos($value,'get')!==false){
		// 		echo "<br/>$value";
		// 	}
		// }
		// die;
		 $this->doctrine->em->flush();

		}
		 catch (Exception $e) {
                        die($e->getMessage());
                }
		
//		$this->doctrine->em->flush();

	}

	function test(){
		$this->message->set('123', 'error', true, 'feedback');
		header('Location: '.'http://localhost/sun/live/emi?msg=test');
		redirect('emi');
	}
}

