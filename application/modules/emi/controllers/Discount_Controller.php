<?php

use App\Calculator;
use models\Discount;

class Discount_Controller extends Front_Controller{
	

	public function __construct(){
		parent :: __construct();
		$discounts=$this->doctrine->em->getRepository('models\Discount')->findAll();
		$this->_t['discounts'] = $discounts;
	}

	public function index(){		
		try {
			$this->_t['maincontent'] = "emi/discount/index";
			$this->load->theme("main", $this->_t);
		}
		catch (Exception $e) {
			$this->message->set($e->getMessage(), 'error', true, 'feedback');
			redirect('emi');			
		}
	}

	public function edit($id=null){
		try {
			if(!$id){
				$this->message->set('Error Processing Request', 'error', true, 'feedback');
				redirect('emi');
			} 
			$discount=$this->doctrine->em->getRepository('models\Discount')->find($id);
			if(!$discount){
				$this->message->set('Couldnt load discount', 'error', true, 'feedback');
				redirect('emi');
			} 

			if($this->input->post()){
				$this->form_validation->set_rules('payment_discount', "Payment Discount", "required|number|xss_clean");
				$this->form_validation->set_rules('corporate_discount', "Corporate Discount", "required|number|xss_clean");
				// show_pre($this->input->post());
				if($this->form_validation->run($this))
				{
					$discount->setPaymentDiscount($this->input->post('payment_discount'));
					$discount->setPaymentDiscount($this->input->post('payment_discount'));
					$this->doctrine->em->persist($discount);
					$this->doctrine->em->flush();
					$this->message->set("Updated successfully", 'success',TRUE,'feedback');
					redirect('emi/discount');
				}
			}
			$this->_t['discount'] = $discount;
			$this->_t['maincontent'] = "emi/discount/edit";
			$this->load->theme("main", $this->_t);			
		} catch (Exception $e) {
			$this->message->set($e->getMessage(), 'error', true, 'feedback');
			redirect(current_url());
		}
	}


	public function delete($id=null){
		try {
			if(!$id){
				$this->message->set('Error Processing Request', 'error', true, 'feedback');
				redirect('emi');
			} 
			$discount=$this->doctrine->em->getRepository('models\Discount')->find($id);
			if(!$discount){
				$this->message->set('Couldnt load discount', 'error', true, 'feedback');
				redirect('emi');
			} 
			$discount->setPaymentDiscount($this->input->post('payment_discount'));
			die('Under Development');
			$this->doctrine->em->persist($discount);
			$this->doctrine->em->flush();
			$this->message->set("Updated successfully", 'success',TRUE,'feedback');
			redirect('emi/discount');
		} catch (Exception $e) {
			$this->message->set($e->getMessage(), 'error', true, 'feedback');
			redirect(current_url());
		}
	}


}

