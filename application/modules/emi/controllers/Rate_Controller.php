<?php

use App\Calculator;
use models\Rate;

class Rate_Controller extends Front_Controller{
	

	public function __construct(){
		parent :: __construct();
		$rates=$this->doctrine->em->getRepository('models\Rate')->findAll();
		$this->_t['rates'] = $rates;
	}

	public function index(){		
		try {
			$this->_t['maincontent'] = "emi/rate/index";
			$this->load->theme("main", $this->_t);
		}
		catch (Exception $e) {
			$this->message->set($e->getMessage(), 'error', true, 'feedback');
			redirect('emi');			
		}
	}

	public function edit($id=null){
		try {
			if(!$id){
				$this->message->set('Error Processing Request', 'error', true, 'feedback');
				redirect('emi');
			} 
			$rate=$this->doctrine->em->getRepository('models\Rate')->find($id);
			if(!$rate){
				$this->message->set('Couldnt load Rate', 'error', true, 'feedback');
				redirect('emi');
			} 

			if($this->input->post()){
				$this->form_validation->set_rules('payment_Rate', "Payment Rate", "required|number|xss_clean");
				$this->form_validation->set_rules('corporate_Rate', "Corporate Rate", "required|number|xss_clean");
				// show_pre($this->input->post());
				if($this->form_validation->run($this))
				{
					$rate->setPaymentRate($this->input->post('payment_Rate'));
					$rate->setPaymentRate($this->input->post('payment_Rate'));
					$this->doctrine->em->persist($rate);
					$this->doctrine->em->flush();
					$this->message->set("Updated successfully", 'success',TRUE,'feedback');
					redirect('emi/Rate');
				}
			}
			$this->_t['rate'] = $rate;
			$this->_t['maincontent'] = "emi/rate/edit";
			$this->load->theme("main", $this->_t);			
		} catch (Exception $e) {
			$this->message->set($e->getMessage(), 'error', true, 'feedback');
			redirect(current_url());
		}
	}


	public function delete($id=null){
		try {
			if(!$id){
				$this->message->set('Error Processing Request', 'error', true, 'feedback');
				redirect('emi');
			} 
			$rate=$this->doctrine->em->getRepository('models\Rate')->find($id);
			if(!$rate){
				$this->message->set('Couldnt load Rate', 'error', true, 'feedback');
				redirect('emi');
			} 
			$rate->setPaymentRate($this->input->post('payment_Rate'));
			die('Under Development');
			$this->doctrine->em->persist($rate);
			$this->doctrine->em->flush();
			$this->message->set("Updated successfully", 'success',TRUE,'feedback');
			redirect('emi/Rate');
		} catch (Exception $e) {
			$this->message->set($e->getMessage(), 'error', true, 'feedback');
			redirect(current_url());
		}
	}

	public function import(){

		try {
			if($this->input->post()){

				if(isset($_FILES['city_file']))
				{
					$config['upload_path'] = './assets/uploads/temp/';
					$config['allowed_types'] = 'xls';
					$config['max_size']	= '1024';

					$this->load->library('upload', $config);
					$this->upload->initialize($config);

					// echo $this->upload->do_upload('city_file')?'s':$this->upload->display_errors();
					
					if ( ! $this->upload->do_upload('city_file'))
						$this->message->set($this->upload->display_errors(), 'error', TRUE, 'feedback');
					else
					{
						$updata = $this->upload->data();
						$this->readExcel($updata['full_path']);
						$this->message->set("Cities imported successfully!!",'success',TRUE,'feedback');
					}
					// redirect(current_url());
				}
			}

			$this->_t['maincontent'] = "emi/import";
			$this->load->theme("main", $this->_t);
			
		} catch (Exception $e) {
			die($e->getMessage());
		}

	}

	public function readExcel($filename)
	{
		try {
			require_once APPPATH."third_party/Excel/excel_reader2.php";
			$data = new \Spreadsheet_Excel_Reader($filename);
			$data->setOutputEncoding('CP1251');

			$data = $data->sheets[0]['cells'];
			$rRepo = $this->doctrine->em->getRepository('models\Rate');
			
			// show_pre($data);

			foreach($data as $k=>$e)
			{
				// echo $k;
				// if($e==1) continue;

				echo $age	= isset($e[1]) ? $e[1]:'';
				echo $year	= isset($e[2]) ? $e[2]:'';
				echo $r	= isset($e[3]) ? $e[3]:'';

				$row =  $rRepo->findBy(array('age'=>$age,'year'=>$year));
				
				/*
				echo "c:";
				show_pre($row);
				echo count($row);
				die;
				*/
				
				if(count($row)>0){
					unlink($filename);
					$this->message->set('Data '.$name.' already exists. Please check excel file once.', 'error', TRUE, 'feedback');
					redirect('emi/import');
				}

				$rate = new Rate();
				$rate->setAge($age);
				$rate->setYear($year);
				$rate->setRate($r);

				$this->doctrine->em->persist($rate);
			}
			$this->doctrine->em->flush();
			@unlink($filename);			
		} catch (Exception $e) {
			die($e->getMessage());
		}

	}

}

