<?php

namespace models;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo,
Doctrine\Common\Collections\ArrayCollection; 

if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * @ORM\Entity()
 * @ORM\Table(name="f1_discounts")
 */
class Discount{
	/**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
	private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;    
	/**
	 * @ORM\Column(name="payment_discount",type="decimal", precision=5, scale=2)
	 */
	private $paymentDiscount;
    /**
     * @ORM\Column(name="corporate_discount",type="decimal", precision=5, scale=2)
     */
    private $corporateDiscount;


    public function id()
    {
    	return $this->id;
    }

    public function getName()
    {
    	return $this->name;
    }

    public function setName($name)
    {
    	$this->name = $name;
    }

    public function getPaymentDiscount()
    {
    	return $this->paymentDiscount;
    }

    public function setPaymentDiscount($paymentDiscount)
    {
    	$this->paymentDiscount = $paymentDiscount;
    }

    public function getCorporateDiscount()
    {
    	return $this->corporateDiscount;
    }

    public function setCorporateDiscount($corporateDiscount)
    {
    	$this->corporateDiscount = $corporateDiscount;
    }

    public function getRow(){
        return $this->getName()." ( Pay. Dis.: ".$this->getPaymentDiscount()." ,Cor. Dis.: ".$this->getCorporateDiscount()." )";
    }                

 }