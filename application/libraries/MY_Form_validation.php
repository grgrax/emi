<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Form_validation extends CI_Form_validation{
    
    public $CI;
    
    function __construct(){
    	parent::__construct();
		$CI =& get_instance();
    }

    function run($module = '', $group = ''){
        (is_object($module)) AND $this->CI = &$module;
            return parent::run($group);
    }
    
	public function money($str){
		return (bool) preg_match('/^[0-9]*\.?[0-9]{1,2}+$/',$str);
	}
	
	public function phone($str){
		return ( ! preg_match("/^([a-z0-9_-\s@+(),])+$/i", $str)) ? FALSE : TRUE;
	}
	
	public function valid_fax($str){
		return ( ! preg_match("/^([0-9])+$/i", $str)) ? FALSE : TRUE;
	}
	public function valid_phone($str){
		return ( ! preg_match("/^([0-9\-+])+$/i", $str)) ? FALSE : TRUE;
	}

	function alpha_numeric_tb($str)
	{
		return ( ! preg_match("/^([a-z0-9_\-\s@\'\.])+$/i", $str)) ? FALSE : TRUE;
	}

	function valid_name($str)
	{
    return ( ! preg_match("/^([a-z])+$/i", $str)) ? FALSE : TRUE;
	}

	function alpha_dash_space($str)
	{
	    return ( ! preg_match("/^([-a-z_ ])+$/i", $str)) ? FALSE : TRUE;
	} 
	
	function valid_username($str)
	{
		return ( ! preg_match("/^([a-z0-9_\-\.\@])+$/i", $str)) ? FALSE : TRUE;
	}


	function valid_full_name($str) {
        $this->CI->form_validation->set_message('valid_full_name', 'The %s may only contain alphabet and spaces.');
        return ( ! preg_match("/^([\sa-z])+$/i", $str)) ? FALSE : TRUE;
    }
	
	function alpha_space($str) {
        $this->CI->form_validation->set_message('alpha_space', 'The %s may only contain alphabet and spaces.');
        return ( ! preg_match("/^([\sa-z])+$/i", $str)) ? FALSE : TRUE;
    }

	function alphanumeric_space($str) {
        $this->CI->form_validation->set_message('alphanumeric_space', 'The %s may only contain alphabet,number and spaces.');
        return ( ! preg_match("/^([\sa-z0-9])+$/i", $str)) ? FALSE : TRUE;
    }

	function decimal($value)
	{
		$CI =& get_instance();
		$CI->form_validation->set_message('decimal',
				'The %s is not a valid decimal number.');
	
		$regx = '/^[-+]?[0-9]*\.?[0-9]*$/';
		if(preg_match($regx, $value))
			return true;
		return false;
	}

	function debtor_citizenship_number($str) {
        $this->CI->form_validation->set_message('debtor_citizenship_number', 'The %s may contain only numbers, comma, dot, \, /, or - without spaces.');
        return (preg_match("/[^0-9,\.\-\/\\\]/", $str)) ? FALSE : TRUE;
    }

    function debtor_citizenship_issued_date($str) {
        $this->CI->form_validation->set_message('debtor_citizenship_issued_date', 'The %s must be in format YYYY-MM-DD.');
        return (preg_match("/^([0-9]{4}\-[0-9]{2}\-[0-9]{2})*$/", $str)) ? TRUE : FALSE;
    }

	function is_valid_captcha($str) {
		$this->CI->form_validation->set_message('is_valid_captcha', 'Captcha code mismatched.');
		return ($this->CI->session->userdata('captcha_code')==$str) ? TRUE: FALSE ;
    }

	function debtor_registration_number($str) {
        $this->CI->form_validation->set_message('debtor_registration_number', 'The %s may contain only numbers, comma, dot, (, ), \, /, or - without spaces.');
        return (preg_match("/[^0-9,\(\).\-\/\\\]/", $str)) ? FALSE : TRUE;
    }

	function collateral_serial_number($str) {
        $this->CI->form_validation->set_message('collateral_serial_number', 'The %s may contain only alphabets, numbers, comma, (, ), /, or - without spaces.');
        return (preg_match("/[^a-zA-Z0-9\,\(\)\-\/\\\]/", $str)) ? FALSE : TRUE;
    }

} 