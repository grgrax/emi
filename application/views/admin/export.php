  <h1>Emil Caluculation :</h1><i><?php echo $time; ?></i>
  <?php if(isset($emi)) { ?>
  <h3>Insurance Premium: </h3>
  <table>
   <tbody>
     <tr><td><b>Principal :</b></td><td><?php echo isset($emi['principal'])?number_format($emi['principal']):'N/A'; ?></td></tr>
     <tr><td><b>Interest :</b></td><td><?php echo isset($emi['interest'])?"{$emi['interest']} %":'N/A'; ?></td></tr>
     <tr><td><b>Term :</b></td><td><?php echo isset($emi['term'])?"{$emi['term']} years":'N/A'; ?></td></tr>
     <tr><td><b>Rate :</b></td><td><?php echo isset($emi['mappedRate'])?$emi['mappedRate']:'N/A'; ?></td></tr>
     <tr><td><b>Age :</b></td><td><?php echo isset($emi['age'])?"{$emi['age']} years old":'N/A'; ?></td></tr>
     <tr><td><b>Large Sum Discount :</b></td><td>-<?php echo isset($emi['largeSumDiscount'])?number_format($emi['largeSumDiscount'],2):'N/A'; ?></td></tr>
     <tr><td><b>After Discount :</b></td><td><?php echo isset($emi['afterDiscount'])?number_format($emi['afterDiscount'],2):'N/A'; ?></td></tr>
     <tr><td><b>Annual Premium :</b></td><td><?php echo isset($emi['annualPremium'])?number_format($emi['annualPremium'],2):'N/A'; ?></td></tr>
     <tr><td><b>Dis. on Annual payment :</b></td><td> <?php echo isset($emi['discountOnAnnualPayment'])?number_format($emi['discountOnAnnualPayment'],2):'N/A'; ?></td></tr>
     <tr><td><b>Prem. After Discount :</b></td><td><?php echo isset($emi['premiumAfterDiscount'])?number_format($emi['premiumAfterDiscount'],2):'N/A'; ?></td></tr>
     <tr><td><b>Less Corporate Discount :</b></td><td> <?php echo isset($emi['lessCorporateDiscount'])?number_format($emi['lessCorporateDiscount'],2):'N/A'; ?></td></tr>

     <?php if(isset($emi['benefitAmontOne'])) {?>
     <tr><td><b>ADB (Accidental Death Benefit) :</b></td><td> <?php echo number_format($emi['benefitAmontOne']); ?></td></tr>
     <?php if(isset($emi['benefitAmontTwo'])) {?>
     <tr><td><b>TPDPW (Total Permanent Disability Benefit) :</b></td><td> <?php echo number_format($emi['benefitAmontTwo']); ?></td></tr>
     <?php } } ?>

     <tr><td><b>Monthly Premium Amount (MPA) :</b></td><td><?php echo isset($emi['monthlyPreiumAmount'])?number_format($emi['monthlyPreiumAmount'],2):'N/A'; ?></td></tr>
   </tbody>
 </table>
 <?php } ?>

 <?php if(isset($installments)){ ?>
 <h3>Emi Schedule:</h3>
 <table>
   <thead>
     <tr>
       <th>S. No.</th>
       <th>Date</th>
       <th>Installment</th>
       <th>Premium</th>
       <th>Principal</th>
       <th>Interest Recovered</th>
     </tr>
   </thead>
   <tbody>
     <?php
     foreach ($installments as $installment) { $m=$installment['installment_number'];?>
     <tr <?php echo $m%12==0?'class="last_month"':'';?>>
       <td><?php echo $m;?></td>
       <td><?php echo $installment['due_date'];?></td>
       <td><?php echo number_format($installment['installment'],'2'); ?></td>
       <td><?php echo isset($emi['monthlyPreiumAmount'])?number_format($emi['monthlyPreiumAmount'],2):'N/A'; ?></td>
       <td><?php echo number_format($installment['principal'],2)?></td>
       <td>
         <?php 
         if($m%12==0){
          $principal=$installment['installment']-$installment['principal'];
          echo number_format($principal,2);
        }else{
          echo number_format($installment['interest_recovered'],2);
        }
        ?>
      </td>
    </tr>
    <?php } ?>         
  </tbody>
</table>
<?php } ?>