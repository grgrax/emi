<?php

use Transborder\TQL\TQLExecuter;

use Transborder\TQL\Parser;

use models\Transaction;

use Transborder\Common\Util\Numbers\NumbersWords;

class Test_Controller extends MY_Controller{
	
	public function __construct(){
		parent::__construct();
	}

	public function audit(){
		
		$test = $this->doctrine->em->getRepository('models\Test')->find(1);
		echo $test->id();
		exit();

	}

	public function add(){
		
		$test  = new models\Test();
		$test->setFileNumber("filenum2");
		$this->doctrine->em->persist($test);
// debtor 1
		$testDebtor =  new models\TestDebtor();
		$testDebtor->setName("name1");
		$testDebtor->setTest($test);

		$this->doctrine->em->persist($testDebtor);
// debtor 2
		$testDebtor =  new models\TestDebtor();
		$testDebtor->setName("name2");
		$testDebtor->setTest($test);

		$this->doctrine->em->persist($testDebtor);
// collateral 1

		$testCollateral = new models\TestCollateral();
		$testCollateral->setDescription('description1');
		$testCollateral->setTest($test);
		

		$this->doctrine->em->persist($testCollateral);
// collateral 2

		$testCollateral = new models\TestCollateral();
		$testCollateral->setDescription('description2');
		$testCollateral->setTest($test);
		
		$this->doctrine->em->persist($testCollateral);

// serial 1

		$testSerial = new models\TestSerial();
		$testSerial->setType('vehicle1');
		$testSerial->setTestCollateral($testCollateral);

		$this->doctrine->em->persist( $testSerial );
// serial 2

		$testSerial = new models\TestSerial();
		$testSerial->setType('vehicle2');
		$testSerial->setTestCollateral($testCollateral);

		$this->doctrine->em->persist( $testSerial );

// serial 3
		$testSerial = new models\TestSerial();
		$testSerial->setType('vehicle3');
		$testSerial->setTestCollateral($testCollateral);

		$this->doctrine->em->persist( $testSerial );



		$this->doctrine->em->flush();
		
		exit("added");
		
	}

	public function edit(){		

		$test = $this->doctrine->em->find('models\Test', 1);
		$test->setFileNumber('best');
		$this->doctrine->em->flush();

		exit("edited");

	}


	public function softDeleteTest(){
		$b = $this->doctrine->em->find('models\Common\Bank',5);
		$this->doctrine->em->remove($b);		
		$this->doctrine->em->flush();		
		
	}	
	
	public function parser(){
		$str = "PRINT asdd";
		$parser = new Parser($str);
		echo $parser->parse();
	}
	
	public function txnstatus($tx){
		$transaction = $this->doctrine->em->find('models\Transaction', $tx);

		$transaction->setStatus(Transaction::STATUS_APPROVED);
		$this->doctrine->em->persist($transaction);
		$this->doctrine->em->flush();
		
		
		$transaction->setStatus(Transaction::STATUS_PAID);
		$this->doctrine->em->persist($transaction);
		$this->doctrine->em->flush();
		
		$transaction->setStatus(Transaction::STATUS_SETTELED);
		$this->doctrine->em->persist($transaction);
		$this->doctrine->em->flush();
		
		$transaction->setStatus(Transaction::STATUS_RECONCILED);
		$this->doctrine->em->persist($transaction);
		$this->doctrine->em->flush();
	}
	
	
	public function validation(){
		var_dump($this->form_validation->alpha_numeric_tb('Rajesh @ Sharma'))."<br />";
		var_dump($this->form_validation->valid_username('broncha.rajesh.gmail.com'));
	}
	
	public function globalagentfilter(){
		$repo = $this->doctrine->em->getRepository('models\Transaction');
		$txns = $repo->getTransactionList(0,10);
// 		echo count($txns);
		exit;
		foreach($txns as $t){
			echo "Transaction START:<br/>";
			show_pre($t);
			echo "Transaction END:<br/>";
		}
	}
	
	public function logg($txn){
		$transaction = $this->doctrine->em->find('models\Transaction', $txn);
		
		/* @var $tlr Gedmo\Loggable\Entity\Repository\LogEntryRepository */
		$tlr = $this->doctrine->em->getRepository('models\Transaction\TransactionLog');
		
		$logEntries = $tlr->getLogEntries($transaction);
		
		/* @var $l models\Transaction\TransactionLog */
		foreach($logEntries as $l){
			show_pre($l->getData(),"Version :: ".$l->getVersion()." Datetime :: ".$l->getLoggedAt()->format('F j, Y H:i:s'));
		}
	}
	
	
	
	public function tql(){
		$tql = "select 
	concat_ws(',',ra.name,rac.name) as 'Remitting Agent',
	r.name as 'Remitter',t.tracking_number as 'Tracking Code',t.created as 'Created Date', 
	concat_ws(',',pa.name,pac.name) as 'Payout Agent', b.name as 'Beneficiary',
	rc.iso_code as 'Remitting Currency',
	t.remitting_amount as 'Remitted Amount', 
	pc.iso_code as 'Paying Currency',
	t.target_amount as 'Paying Amount'
from f1_transactions t
join f1_customer r on r.id = t.remitter_id
join f1_customer b on b.id = t.beneficiary_id
join f1_agents ra on ra.id = t.remitting_agent_id
join f1_cities rac on rac.id = ra.city_id
join f1_currencies rc on rc.id = t.remitting_currency_id
join f1_currencies pc on pc.id = t.receiving_currency_id
left join f1_agents pa on pa.id = t.payout_agent_id
left join f1_cities pac on pac.id = pa.city_id
where 1 = 1
filter:ra.id:dropdown
filter:b.name:text
and DATE(t.created) BETWEEN filter:from:date AND filter:to:date
order by concat_ws(',',ra.name,rac.name)
[	TABLIZE BY 'Remitting Agent'
	FILTERABLE BY 'Created Date' TYPE DATE
	AGGREGATE 'Paying Amount' SUM, 'Remitted Amount' SUM]
";
		/*
		 * 
		 * 
		 * 
		 * */

		$exec = new TQLExecuter($tql, $this->db);
		$result = $exec->getResult();
		show_pre($tql,"<b>Executed TQL</b>");
		
		echo "<b>{$result['rowCount']} records found.</b>";
		
		if($result['tablized']){
			foreach($result['result'] as $t => $d){
				if(isset($t['transactions']))
					$this->generateTable($d,$t);
			}
		}else{
			$this->generateTable($result,'TLJL');
		}
	}
	
	public function generateTable($table,$head){
		
		$txns = $table['transactions'];
		
		$first = $txns[0];
		$columns = array_keys($first);
		
		
		
		echo "<p>{$head} {$table['rowCount']}</p>";
		echo "<table border='1' width='100%' cellspacing='1' style='margin-bottom:20px;'>";
		foreach($txns as $tx){
			echo "<tr>";
			
			foreach($columns as $c){
				echo "<td>{$tx[$c]}</td>";
			}
			
			echo "</tr>";
		}
		
		if(isset($table['aggregates'])){
			$agg = $table['aggregates'];
			echo "<tr>";
			foreach($columns as $c){
				if(isset($agg[$c])){
					echo "<td>{$agg[$c]['SUM']}</td>"; 
				}else{
					echo "<td>&nbsp;</td>";
				}
			}
			echo "</tr>";
		}
		
		echo "</table>";
	}
	
	
	private function parseModifier(){
		
	}
	
	
}