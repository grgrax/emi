<?php

class Tclistener_Controller extends MY_Controller
{
	
	/**
	 * Default class map for wsdl=>php
	 * @access private
	 * @var array
	 */
	private static $classmap = array(
			"NotifyStatusRequest" => "Transborder\\Transconnect\\AlertService\\NotifyStatusRequest",
			"NotifyStatusResponse" => "Transborder\\Transconnect\\AlertService\\NotifyStatusResponse",
			"TcAlertWatchdogRequest" => "Transborder\\Transconnect\\AlertService\\TcAlertWatchdogRequest",
			"TcAlertWatchdogResponse" => "Transborder\\Transconnect\\AlertService\\TcAlertWatchdogResponse",
			"TransconnectAlertMapEntry" => "Transborder\\Transconnect\\AlertService\\TransconnectAlertMapEntry",
			"TransconnectTransactionAlertDetail" => "Transborder\\Transconnect\\AlertService\\TransconnectTransactionAlertDetail",
			"TransconnectAlertServiceStatus" => "Transborder\\Transconnect\\AlertService\\TransconnectAlertServiceStatus",
			"TransconnectTransactionAlertStatus" => "Transborder\\Transconnect\\AlertService\\TransconnectTransactionAlertStatus",
	);
	
	public function __construct(){
		
		log_message('info','Transconnect Listener Request');
		
		parent::__construct();
		$server = new SoapServer('./tcalertservice.wsdl',array('classmap' => self::$classmap));
		$server->setClass('Transborder\Transconnect\AlertService\TransconnectListener',$this);
		$server->handle();
		die();
	}
}