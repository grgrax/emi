<?php

class JS_Controller extends MY_Controller
{
	public function __construct(){
		$this->output->set_content_type('text/javascript');
	}
	
	public function core(){
		
		$this->load->helper('country/country');
		$allCountries = getCountries();
		
		$countries = array();
		foreach($allCountries as $country){
			$countries[] = array(	'id'	=>	$country->id(),
					'name'	=>	$country->getName());
		}
		
		
		$data = array(	'allCountries' => $countries,);
		
		$this->load->view('js/core', $data);
	}
}