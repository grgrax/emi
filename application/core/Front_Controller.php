<?php
class Front_Controller extends MY_Controller{
	public $templatedata = [] ;

	public $_t = array() ;

	public function __construct(){
		parent :: __construct();

		$this->load->library('breadcrumb');

		$this->load->library('session');

		$this->_t['mainstyler'] = csscrush_file( BASEPATH.'../assets/themes/' . config_item('current_theme') . '/css/mainstyler.css' );
		$this->_t['menustyler'] = csscrush_file( BASEPATH.'../assets/themes/' . config_item('current_theme') . '/css/menu.css' );
		$this->_t['printstyler'] = csscrush_file( BASEPATH.'../assets/themes/' . config_item('current_theme') . '/css/print.css' );
		$this->_t['themestyler'] = csscrush_file( BASEPATH.'../assets/themes/' . config_item('current_theme') . '/css/style.css' );
		$this->_t['frontstyler'] = csscrush_file( BASEPATH.'../assets/themes/' . config_item('current_theme') . '/css/frontstyle.css' );
		
		//check for any critical messages
		$_critical_messages = $this->message->get('alert','critical');

		if(count($_critical_messages) > 0 ){
				$this->_t['critical_alerts'] = $_critical_messages;
		}
		
		$_feedbacks = $this->message->get(FALSE,'feedback');

		if(count($_feedbacks) > 0){
			$this->_t['feedback'] = $_feedbacks;
		}	
		//can use both templatedata and _t
		$this->templatedata = $this->_t;
	}
}