<?php
class Admin_Controller extends MY_Controller
{
	protected $templatedata = array();
	protected $mainmenu  = null;
	public function __construct(){


		
		parent :: __construct();
		//system theme defination
		System::init();
		//$this->output->enable_profiler(TRUE);
		
		$this->load->library('session');
		$this->load->library('breadcrumb');
		
		
		// $this->templatedata['mainstyler'] = csscrush_file( BASEPATH.'../assets/themes/' . config_item('current_theme') . '/css/mainstyler.css' );
		$this->templatedata['custom'] = csscrush_file( BASEPATH.'../assets/themes/' . config_item('current_theme') . '/css/custom.css' );
		
		$this->templatedata['menustyler'] = csscrush_file( BASEPATH.'../assets/themes/' . config_item('current_theme') . '/css/menu.css' );
		$this->templatedata['printstyler'] = csscrush_file( BASEPATH.'../assets/themes/' . config_item('current_theme') . '/css/print.css' );
		$this->templatedata['themestyler'] = csscrush_file( BASEPATH.'../assets/themes/' . config_item('current_theme') . '/css/style.css' );
		
		if ($this->session->userdata('user_id') or $this->session->userdata('non_client_id'))
		{
					// redirect('auth/login/'.str_replace( 
					// 				array(
					// 					$this->config->item('url_suffix'), 
					// 					site_url(), 
					// 					'auth/authenticate', 
					// 					'auth/login'
					// 				), 
					// 				'', 
					// 				current_url()
					// 				)
					// );

			
		}else{
			// exit("Get Id");
			redirect('public');
		}


		$this->load->library('breadcrumb');
		$this->breadcrumb->append_crumb('Dashboard', site_url());

		$this->templatedata['mainmenu'] = $this->mainmenu;

		$this->templatedata['_CONFIG']	= $this->_CONFIG;
		$this->templatedata['flashdata'] = $this->session->flashdata('feedback');
		
		$this->templatedata['scripts'] = array();
		$this->templatedata['stylesheets'] = array();


		//check for any critical messages
		$_critical_messages = $this->message->get('alert','critical');

		if(count($_critical_messages) > 0 ){
			$this->templatedata['critical_alerts'] = $_critical_messages;
		}
		
		$_feedbacks = $this->message->get(FALSE,'feedback');

		if(count($_feedbacks) > 0){
			$this->templatedata['feedback'] = $_feedbacks;
		}


		$currentUser = Current_User::user();

		if($currentUser){
			if($currentUser->isDeleted() || $currentUser->isActive()==FALSE ){
			$this->message->set('You are either blocked or deleted by administrator.','error',TRUE,'feedback');
			redirect('auth/login');			
		}
		
		if (Current_User::isAlreadyLogged() && !config_item('allow_multiple_machine_session')) {
			
			$this->session->sess_destroy();
			$this->templatedata['maincontent'] = 'config/user-already-logged-in';
			$this->load->theme('master',$this->templatedata);		
		}
		
		$admin = Current_User::isSuperUser();
		
		if (is_numeric($main_user = $this->session->userdata('main_user'))) {
			
			$main_user = $this->doctrine->em->find('models\User', $main_user);
			if ($main_user) {
				$this->templatedata['user_switch'] = 	array(
					'text' => 'You are currently using STR as '. Current_User::user()->getUsername(). '. Revert back to <a href="' .site_url('auth/revert'). '" style="color:#f99;">Main User</a> when you are done.',
					'type' => 'warning',
					'layout' => 'top',
					'theme' => 'default',
					);
				
			}
			
		}
		
		if ($admin or $main_user) {} else {
			
			if (\Options::get('user1st_login','0')=='1') {
				
				if (($currentUser->isFirstLogin())) {
					
					$this->message->set("Logging in for first time! Please change your password.", 'error', TRUE, 'feedback');
					redirect(site_url('auth/changepwd'));
					
				}
				
			}
			
			if (\Options::get('userpwd_expirable','0') == '1'
				and	(\Options::get('userpwd_expiry_days','1000') >= '10')
				) {
				
				if ( 
						(isValidDate(($currentUser->pwdLastChangedOn()->format('Y-m-d')))) // bypass invalid timestamp
						and
						(time() - strtotime($currentUser->pwdLastChangedOn()->format('Y-m-d'))) // number of seconds from last pwd change to now
						>=
						((\Options::get('userpwd_expiry_days','1000')) * 86400) // number of seconds after which pwd expires
						) {
					
					$this->message->set("Your password has expired!! Please change your password.", 'error', TRUE, 'feedback');
				redirect(site_url('auth/changepwd'));
				
			}}
		}
	
		if (\Options::get('site_maintenance', '0')=='1') {

			$force_maintenance = TRUE;
			
			$autoresume = (\Options::get('site_maintenance_resume','0')=='1') ? TRUE : FALSE;
			
				if ($autoresume) {
					
					$resume_date_time = \Options::get('site_maintenance_resume_after', '0000-00-00 00:00');
					
					$resume_date = substr($resume_date_time, 0, 10);
					
					if (isValidDate($resume_date)) {
						
						$resume_timestamp = strtotime($resume_date_time.':00');
						
						if ($resume_timestamp > 0 and $resume_timestamp < time()) {
							
							$force_maintenance = FALSE;
							
						}
						
					}
					
				}
			
			if ($force_maintenance) {
				
				if ($admin) {
					$this->templatedata['site_maintenance'] = 	array(
						
						'text' => 'Site is Currently in Maintenance Mode. Please <a href="' .site_url('config/settings'). '#maintenance" target="_blank" style="color:#f99;">deactivate</a> this mode when you are done.',
						'type' => 'information',
						'layout' => 'top',
						'theme' => 'default',
						);
				} else {
					
					$this->templatedata['maincontent'] = 'config/site-maintenance';
					$this->load->theme('master',$this->templatedata);
				}
				
			}
		}
		}
	







}
}