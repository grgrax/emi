<?php
namespace DataFixtures;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\FixtureInterface;

class CountryFix implements FixtureInterface
{
	public function load(ObjectManager $manager){

		echo "\nCurrency Adding\n";

		$currency = new \models\Common\Currency;
		$currency->setName('Nepali Rupee');
		$currency->setIsoCode('NPR');
		// $currency->setCountry(NULL);
		$currency->setSymbol('NPR');
		$manager->persist($currency);
		$manager->flush();

		echo "\nCurrency Added\n";

		echo "\nCountry Adding \n";


		$fileContents = file(__DIR__.'/countries.txt');
		
		foreach ($fileContents as $file) {
				
			$c = explode(";", $file);
			$country = new \models\Common\Country;			
			$country->setName($c['2']);
				
			$country->setIso_2($c['3']);
			$country->setIso_3($c['4']);

			$dialingCode = str_replace(array('\n','\t','\n\n','\n\n'),'', $c['6']);
		
			$country->setDialingCode(trim($dialingCode));
			
			$manager->persist($country);
		}

		$manager->flush();

		echo "\nCountries Added\n";

		echo "\nStates Adding \n";

		$fileContents = file(__DIR__.'/states.txt');
		foreach ($fileContents as $file) {
				
			$c = explode(";", $file);
			$state = new \models\Common\State;	
			
			$country = $manager->getRepository('\models\Common\Country')->find($c['1']);

			$state->setCountry($country);
			
			$name = str_replace(array('\n','\t','\n\n','\n\n'),'', $c['2']);
		
			$state->setName(trim($name));
			
			$manager->persist($state);
		}

		$manager->flush();

		echo "\nStates Added Success fully \n";

		echo "\nCities Adding \n";

		$fileContents = file(__DIR__.'/cities.txt');
		foreach ($fileContents as $file) {
				
			$c = explode(";", $file);
			$city = new \models\Common\City;	
			
			$state = $manager->getRepository('\models\Common\State')->find($c['1']);

			$city->setState($state);


			$name = str_replace(array('\n','\t','\n\n','\n\n'),'', $c['2']);
		
			$city->setName(trim($name));
			
			$manager->persist($city);
		}

			$manager->flush();

		echo "\nCities Added Success fully\n";

	}
}