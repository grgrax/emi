<?php
namespace DataFixtures;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\FixtureInterface;

class UserFix implements FixtureInterface
{

	const ACCNUM = '1111111111';

	public function load(ObjectManager $manager){
		echo "\nUser Adding\n";

		$group1 = new \models\User\Group;
		$group1->setName('Super Admin');
		$group1->setDescription("Super Admin");
		$manager->persist($group1);

		$group2 = new \models\User\Group;
		$group2->setName('Registry Admin');
		$group2->setDescription("Registry Admin");
		$manager->persist($group2);

		$group3 = new \models\User\Group;
		$group3->setName('Client');
		$group3->setDescription("Client");
		$manager->persist($group3);
		
			$subGroup31 = new \models\User\Group;
			$subGroup31->setName('General Users');
			$subGroup31->setDescription("General Users");
			$subGroup31->setParentGroup($group3);
			$manager->persist($subGroup31);

			$subGroup32 = new \models\User\Group;
			$subGroup32->setName('Security Users');
			$subGroup32->setDescription("Security Users");
			$subGroup32->setParentGroup($group3);
			$manager->persist($subGroup32);
			
		$group4 = new \models\User\Group;
		$group4->setName('Bankclient');
		$group4->setDescription("Bankclient");
		$manager->persist($group4);
		
			$subGroup41 = new \models\User\Group;
			$subGroup41->setName('General Users');
			$subGroup41->setDescription("General Users");
			$subGroup41->setParentGroup($group4);
			$manager->persist($subGroup41);

			$subGroup42 = new \models\User\Group;
			$subGroup42->setName('Security Users');
			$subGroup42->setDescription("Security Users");
			$subGroup42->setParentGroup($group4);
			$manager->persist($subGroup42);

			$subGroup43 = new \models\User\Group;
			$subGroup43->setName('Teller');
			$subGroup43->setDescription("Teller");
			$subGroup43->setParentGroup($group4);
			$manager->persist($subGroup43);

		$group5 = new \models\User\Group;
		$group5->setName('Government Entity');
		$group5->setDescription("Government Entity");
		
		$manager->persist($group5);
		$manager->flush();
		

		$ledger = new \models\Ledger;

		$accountType = $manager->getRepository('models\AccountType')->findOneBy(array('type'=>'REGISTRY_ADMIN'));

		$ledger->setAccountType($accountType);
		$ledger->setIsUsed(TRUE);
		$ledger->setStatus(1);
		$ledger->setAccountNumber(self::ACCNUM);
		$ledger->setAccountHead('SUPER');
		$ledger->setBalance('0.00');
		$manager->persist($ledger);
		


		$user = new \models\User;
		$user->setUsername('superadmin');
		$user->setPassword('e10adc3949ba59abbe56e057f20f883e');
		$user->setFirstname('super');
		$user->setLastname('admin');

		$city = $manager->getRepository('models\Common\City')->findOneBy(array('name'=>'kathmandu'));
		$group = $manager->getRepository('models\User\Group')->findOneBy(array('name'=>'Super Admin'));
		
		$user->setCity($city);
		$user->setPhone('1234567890');
		$user->setMobile('1234567890');
		$user->setEmail('asd@asd.asd');
		$user->setGroup($group1);
		$user->setAccType($accountType);
		$user->setLedger($ledger);
		$user->setStatus(1);
		$user->setAccNumber(self::ACCNUM);
		$user->setApproved(1);
		$manager->persist($user);
		$manager->flush();

		echo "\nUser Added\n";
	}
}