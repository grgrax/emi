<?php
namespace DataFixtures;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\FixtureInterface;

class AccountTypeFix implements FixtureInterface
{

	public function load(ObjectManager $manager){

		echo "\n Account Types Adding \n";

		$accountType = new \models\AccountType;
		$accountType->setType('REGISTRY_ADMIN');
		$accountType->setAllowSubType('Y');
		$accountType->setMain('Y');
		$accountType->setDisplayName('Registry Admin');
		$manager->persist($accountType);

		

		$accountType = new \models\AccountType;
		$accountType->setType('CLIENT');
		$accountType->setAllowSubType('Y');
		$accountType->setMain('Y');
		$accountType->setDisplayName('Client');
		$manager->persist($accountType);

		$accountType = new \models\AccountType;
		$accountType->setType('GOVERNMENT_ENTITY');
		$accountType->setAllowSubType('Y');
		$accountType->setMain('Y');
		$accountType->setDisplayName('Govenrnment Entity');
		$manager->persist($accountType);

		$accountType = new \models\AccountType;
		$accountType->setType('NON_CLIENT');
		$accountType->setAllowSubType('N');
		$accountType->setMain('N');
		$accountType->setDisplayName('Non Client');
		$manager->persist($accountType);

		$accountType = new \models\AccountType;
		$accountType->setType('SUB_USER');
		$accountType->setAllowSubType('N');
		$accountType->setMain('N');
		$accountType->setDisplayName('Sub User');
		$manager->persist($accountType);

		echo "Account Types added \n\n";
		
		$manager->flush();
	}
}
