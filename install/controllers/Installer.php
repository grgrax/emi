<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');


/**
* Extending HMVC Controller
*/
class Installer extends MX_Controller
{
	private $languages	= array ('english');

	private $writeable_directories = array(
		'application/logs',
		'application/logs/SQLLogs',
		'application/models/proxies'
	);

	private $writeable_files = array(
		'application/config/production/config.php.inc',
		'application/config/production/database.php.inc'
	);

	public function __construct()
	{
		parent::__construct();
		$this->load->config('servers');
		$this->_set_language();
		$this->load->library('form_validation');
	}

	public function index()
	{
		$data['page_output'] = $this->parser->parse('main', $this->lang->language, TRUE);
		$this->load->view('global',$data);
	}
	
	public function step_1()
	{
		$this->session->set_userdata(array(
			'hostname' => $this->input->post('hostname'),
			'username' => $this->input->post('username'),
			'password' => $this->input->post('password'),
			'port' => $this->input->post('port'),
			'http_server' => $this->input->post('http_server')
		));

		$this->form_validation->set_rules(array(
			array(
				'field' => 'hostname',
				'label'	=> 'lang:server',
				'rules'	=> 'trim|required|callback_test_db_connection'
			),
			array(
				'field' => 'username',
				'label'	=> 'lang:username',
				'rules'	=> 'trim|required'
			),
			array(
				'field' => 'password',
				'label'	=> 'lang:password',
				'rules'	=> 'trim'
			),
			array(
				'field' => 'port',
				'label'	=> 'lang:portnr',
				'rules'	=> 'trim|required'
			)
		));

		if ( $this->form_validation->run() )
		{
			$this->session->set_flashdata('message', lang('db_success') );
			$this->session->set_flashdata('message_type', 'success');

			$this->session->set_userdata('step_1_passed', TRUE);
			redirect('installer/step_2');
		}

		
		$data->port = $this->session->userdata('port') ? $this->session->userdata('port') : 3306;

		$data = array_merge((array) $data,$this->lang->language);

		$this->load->view('global', array(
			'page_output' => $this->parser->parse('step_1', $data, TRUE)
		));
	}

	public function validate_mysql_db_name($db_name)
	{
		$this->form_validation->set_message('validate_mysql_db_name', lang('invalid_db_name'));
		return $this->installer_lib->validate_mysql_db_name($db_name);
	}

	public function test_db_connection()
	{
		if ( ! $this->installer_lib->test_db_connection())
		{
			$this->form_validation->set_message('test_db_connection', lang('db_failure') . mysql_error());
			return false;
		}
		
		return true;
	}

	public function step_2()
	{
		if ( ! $this->session->userdata('step_1_passed'))
		{
			$this->session->set_flashdata('message', lang('step1_failure'));
			$this->session->set_flashdata('message_type','failure');
			redirect('');
		}

		$data->php_min_version	= '5.3';
		$data->php_acceptable	= $this->installer_lib->php_acceptable($data->php_min_version);
		$data->php_version		= $this->installer_lib->php_version;

		$data->mysql->server_version_acceptable = $this->installer_lib->mysql_acceptable('server');
		$data->mysql->client_version_acceptable = $this->installer_lib->mysql_acceptable('client');
		$data->mysql->server_version = $this->installer_lib->mysql_server_version;
		$data->mysql->client_version = $this->installer_lib->mysql_client_version;

		$data->gd_acceptable = $this->installer_lib->gd_acceptable();
		$data->gd_version = $this->installer_lib->gd_version;

		$data->zlib_enabled = $this->installer_lib->zlib_enabled();

		$data->curl_enabled = $this->installer_lib->curl_enabled();

		$data->step_passed = $this->installer_lib->check_server($data);
		$this->session->set_userdata('step_2_passed', $data->step_passed);

		$final_data['page_output'] = $this->load->view('step_2', $data, TRUE);
		$this->load->view('global',$final_data);
	}

	public function step_3()
	{
		if ( ! $this->session->userdata('step_1_passed') OR !$this->session->userdata('step_2_passed'))
		{
			redirect('installer/step_2');
		}

		$this->load->helper('file');

		foreach($this->writeable_directories as $dir)
		{	
			$permissions['directories'][$dir] = is_really_writable('../' . $dir);
		}
		
		foreach($this->writeable_files as $file)
		{
			$permissions['files'][$file] = is_really_writable('../' . $file);
		}

		$data->step_passed = !in_array(FALSE, $permissions['directories']) && !in_array(FALSE, $permissions['files']);
		$this->session->set_userdata('step_3_passed', $data->step_passed);

		$data->permissions = $permissions;

		$data = (object) array_merge((array) $data,$this->lang->language);

		$final_data['page_output'] = $this->parser->parse('step_3', $data, TRUE);
		$this->load->view('global', $final_data);
	}

	public function step_4()
	{
		if ( ! $this->session->userdata('step_1_passed') OR ! $this->session->userdata('step_2_passed') OR ! $this->session->userdata('step_3_passed'))
		{
			redirect('installer/step_2');
		}
		
		$this->form_validation->set_rules(array(
			array(
				'field' => 'database',
				'label'	=> 'lang:database',
				'rules'	=> 'trim|required|callback_validate_mysql_db_name'
			),
			array(
					'field' => 'admin_contact',
					'label'	=> 'lang:admin_contact',
					'rules'	=> 'trim|required|valid_email'
			),
			array(
					'field' => 'tech_contact',
					'label'	=> 'lang:tech_contact',
					'rules'	=> 'trim|required|valid_email'
			)
		));

		if ($this->form_validation->run() == FALSE)
		{
			$final_data['page_output'] = $this->parser->parse('step_4', $this->lang->language, TRUE);
			$this->load->view('global', $final_data);
		}

		else
		{	
			$config = array(	
						'username'	=>	$this->session->userdata('username'),
						'password'	=>	$this->session->userdata('password'),
						'hostname'	=>	$this->session->userdata('hostname'),
						'database'	=>	$this->input->post('database')
				);
			
			$this->load->library('doctrine',$config);
			$this->load->library('transborder');
			
			$install = $this->transborder->install($_POST);
			if ($install['status'] === FALSE)
			{
				$this->session->set_flashdata('message', $this->lang->line('error_'.$install['code']) . $install['message']);
				$step_passed = FALSE;
				$final_data['page_output'] = $this->parser->parse('step_4', $this->lang->language, TRUE);
				$this->load->view('global', $final_data);
				
			}

			$this->session->set_userdata('step_4_passed', TRUE);
			$this->session->set_flashdata('message', lang('success'));
			$this->session->set_flashdata('message_type', 'success');
			$this->session->set_userdata('database',$this->input->post('database'));

			redirect('installer/step_5');
		}
	}

	public function step_5(){
		if ( ! $this->session->userdata('step_1_passed') 
				OR ! $this->session->userdata('step_2_passed') 
				OR ! $this->session->userdata('step_3_passed')
				OR ! $this->session->userdata('step_4_passed'))
		{
			redirect('installer/step_2');
		}
		
		
		$config = array(
				'username'	=>	$this->session->userdata('username'),
				'password'	=>	$this->session->userdata('password'),
				'hostname'	=>	$this->session->userdata('hostname'),
				'database'	=>	$this->session->userdata('database')
		);
		
		$this->load->library('doctrine',$config);
		$this->load->library('transborder');
		
		$crepo = $this->doctrine->em->getRepository('models\Common\Country');
		$timezones = $this->doctrine->em->getRepository('models\Common\TimeZone')->findAll();
		$timezones_data = array();
		$timezones_data[''] = ' -- Select Time Zone -- ';
		foreach($timezones as $tz){
			$timezones_data[$tz->id()] = $tz->getName();
		}
		
		$countries = $crepo->findAll();
		
		$countries_data = array();
		$countries_data2 = array();
		$countries_data2[''] = ' -- Select Country -- ';
		foreach($countries as $c){ 
			$countries_data[$c->id()] = $c->getName(); 
			$countries_data2[$c->id()] = $c->getName();
		}
		
		$template_data = $this->lang->language;
		$template_data['countries_multiselect'] = form_multiselect('operating_countries[]',$countries_data,$this->input->post('operating_countries'),'id="operating_countries"');
		$template_data['agent_country'] = form_dropdown('agent_country',$countries_data2,$this->input->post('agent_country'),'id="agent_country"');
		$template_data['timezone'] = form_dropdown('timezone',$timezones_data,$this->input->post('timezone'),'id="timezone"');
		
		$this->form_validation->set_rules(array(
				array(
						'field' => 'company_name',
						'label'	=> 'Company Name',
						'rules'	=> 'trim|required'
				),
				array(
						'field' => 'company_reg',
						'label'	=> 'Company Registration Number',
						'rules'	=> 'trim|required'
				),
				array(
						'field' => 'operating_countries',
						'label'	=> 'Operating Countries',
						'rules'	=> 'required'
				),
				array(
						'field' => 'branch_code',
						'label'	=> 'Branch Code',
						'rules'	=> 'trim|required'
				),
				array(
						'field' => 'phone',
						'label'	=> 'Phone Number',
						'rules'	=> 'trim|required'
				),
				array(
						'field' => 'contact_person',
						'label'	=> 'Contact Person',
						'rules'	=> 'trim|required'
				),
				array(
						'field' => 'contact_email',
						'label'	=> 'Contact Email',
						'rules'	=> 'trim|required|valid_email'
				),
				array(
						'field' => 'contact_mobile',
						'label'	=> 'Contact Person Mobile',
						'rules'	=> 'trim|required'
				),
				array(
						'field' => 'address',
						'label'	=> 'Address',
						'rules'	=> 'trim|required'
				),
				array(
						'field' => 'city',
						'label'	=> 'City',
						'rules'	=> 'trim|required'
				),
				array(
						'field' => 'state',
						'label'	=> 'State',
						'rules'	=> 'trim|required'
				),
				array(
						'field' => 'agent_country',
						'label'	=> 'Country',
						'rules'	=> 'required'
				),
				array(
						'field' => 'timezone',
						'label'	=> 'Timezone',
						'rules'	=> 'required'
				),
				array(
						'field' => 'currency_name',
						'label'	=> 'Currency Name',
						'rules'	=> 'trim|required'
				),
				array(
						'field' => 'currency_iso',
						'label'	=> 'Currency ISO Code',
						'rules'	=> 'trim|required'
				),
				array(
						'field' => 'account_no',
						'label'	=> 'Account Number',
						'rules'	=> 'required'
				),
				array(
						'field' => 'account_head',
						'label'	=> 'Payable/Receivable Account Name',
						'rules'	=> 'required'
				),
				array(
						'field' => 'balance',
						'label'	=> 'Payable/Receivable Account Name',
						'rules'	=> 'numeric|float'
				),
				array(
						'field' => 'com_account_balance',
						'label'	=> 'Commission Account Balance',
						'rules'	=> 'numeric|float'
				),
				array(
						'field' => 'txn_limit',
						'label'	=> 'Per Transaction Limit',
						'rules'	=> 'required|numeric|float'
				),
				array(
						'field' => 'daily_limit',
						'label'	=> 'Daily Transaction Limit',
						'rules'	=> 'required|numeric|float'
				),
				array(
						'field' => 'monthly_limit',
						'label'	=> 'Monthly Transaction Limit',
						'rules'	=> 'required|numeric|float'
				)
				
		));
		
		if ($this->form_validation->run() == FALSE)
		{
			$final_data['page_output'] = $this->parser->parse('step_5', $template_data, TRUE);
			$this->load->view('global', $final_data);
		}else{
			
			$agent_country = $this->input->post('agent_country');
			$opC = $this->input->post('operating_countries');
			if(!in_array($agent_country, $opC)){
				array_push($opC, $agent_country);
			}
						
			$setOption = $this->transborder->setOption('config_operating_countries', serialize($opC));
			$setCountry = $this->transborder->setOption('tb_country', $agent_country);
			$setup = $this->transborder->setupAgent($this->input->post());
			$operatingCountriesStatus = $this->doctrine->em->getConnection()->exec($setOption);
			$tbcountry = $this->doctrine->em->getConnection()->exec($setCountry);
			if ($setup === FALSE || $operatingCountriesStatus === FALSE)
			{
				$this->session->set_flashdata('message', "Something went wrong.");
				$step_passed = FALSE;
				
				$final_data['page_output'] = $this->parser->parse('step_5', $template_data, TRUE);
				$this->load->view('global', $final_data);
			
			}
			
			$this->session->set_userdata('step_5_passed', TRUE);
			redirect('installer/step_6');
			
			
		}
	}
	
	public function step_6(){
		if ( ! $this->session->userdata('step_1_passed')
				OR ! $this->session->userdata('step_2_passed')
				OR ! $this->session->userdata('step_3_passed')
				OR ! $this->session->userdata('step_4_passed')
				OR ! $this->session->userdata('step_5_passed'))
		{
			redirect('installer/step_2');
		}
		
		
		$config = array(
				'username'	=>	$this->session->userdata('username'),
				'password'	=>	$this->session->userdata('password'),
				'hostname'	=>	$this->session->userdata('hostname'),
				'database'	=>	$this->session->userdata('database')
		);
		
		$this->load->library('doctrine',$config);
		$this->load->library('transborder');
		
		$template_data = array();
		
		
		$this->form_validation->set_rules('first_name', 'First Name', 'required');
		$this->form_validation->set_rules('last_name', 'Last Name', 'required');
		$this->form_validation->set_rules('middle_name', 'Middle Name', 'trim');
		$this->form_validation->set_rules('user_code','User Code','required');
		$this->form_validation->set_rules('email', 'Email', 'valid_email|required');
		$this->form_validation->set_rules('mobile','Mobile Number','required|numeric');
		$this->form_validation->set_rules('address','Address','required');
		$this->form_validation->set_rules('username', 'Username', 'trim|required');
		$this->form_validation->set_rules('password','Password','trim|required|min_length[6]|matches[password_confirm]');
		$this->form_validation->set_rules('password_confirm','Password Confirm','trim|required');
		
		if ($this->form_validation->run() == FALSE)
		{
			$final_data['page_output'] = $this->parser->parse('step_6', $template_data, TRUE);
			$this->load->view('global', $final_data);
		}else{
						
			$setup = $this->transborder->setupSuperadmin($this->input->post());
				
			if ($setup === FALSE)
			{
				$this->session->set_flashdata('message', "Something went wrong.");
				$step_passed = FALSE;
		
				$final_data['page_output'] = $this->parser->parse('step_6', $template_data, TRUE);
				$this->load->view('global', $final_data);
			}

			$this->session->set_userdata('first_name',$this->input->post('first_name'));
			$this->session->set_userdata('last_name',$this->input->post('last_name'));
			$this->session->set_userdata('email',$this->input->post('email'));
			$this->session->set_userdata('user_username',$this->input->post('username'));
			$this->session->set_userdata('user_password',$this->input->post('password'));
			$this->session->set_userdata('step_6_passed', TRUE);
			redirect('installer/complete');
		}
		
	}

	public function complete()
	{
		
		if ( ! $this->session->userdata('username')) { redirect(site_url()); }
		
		$server_name = $this->session->userdata('http_server');
		$supported_servers = $this->config->item('supported_servers');
		
		$data = $this->session->userdata('username');
		
		$data = array_merge((array) $data, $this->lang->language);
		$data['user_firstname'] = $this->session->userdata('first_name');
		$data['user_lastname'] = $this->session->userdata('last_name');
		$data['user_email'] = $this->session->userdata('email');
		$data['user_username'] = $this->session->userdata('user_username');
		$data['user_password'] = $this->session->userdata('user_password');

		$data['website_url'] = 'http://'.$this->input->server('HTTP_HOST').preg_replace('/install\/index.php$/', '', $this->input->server('SCRIPT_NAME'));
		$this->session->sess_destroy();

		$data['page_output'] = $this->parser->parse('complete',$data, TRUE);
		$this->load->view('global',$data);
	}

	public function change($language)
	{
		if (in_array($language, $this->languages))
		{
			$this->session->set_userdata('language', $language);
		}

		redirect('installer');
	}

	private function _set_language()
	{
		if (in_array($this->session->userdata('language'), $this->languages))
		{
			$this->config->set_item('language', $this->session->userdata('language'));
		}
		$lang_file = $this->config->item('language') . '/' . $this->router->fetch_method() . '_lang';
		if (is_file(realpath(dirname(__FILE__) . '/../language/' . $lang_file . EXT)))
		{
			$this->lang->load($this->router->fetch_method());
		}
		$this->lang->load('global');
		define('DEFAULT_LANG', 'en_US');
	}
}

/* End of file installer.php */
