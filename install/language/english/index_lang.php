<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

$lang['header']		=	'Welcome';
$lang['thankyou']	= 	'Thank you for choosing Transborder!';
$lang['text']		=	'In case you have any problems installing the system don\'t worry, the installer will explain what you need to do.';
$lang['step1'] 		= 'Step 1';
$lang['link']		= 'Proceed to the first step';

/* End of file index_lang.php */