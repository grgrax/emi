<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['congrats']			= 'Congratulations';
$lang['intro_text']			= 'Transborder is now installed and ready to go! Please log into the admin panel with the following details.';
$lang['email']				= 'E-mail';
$lang['user']				= 'Username';
$lang['password']			= 'Password';
$lang['show_password']		= 'Show Password?';
$lang['outro_text']			= 'Finally, <strong>delete the installer from your server</strong> as if you leave it here it can be used to hack your application.';

$lang['go_website']			= 'Go to Application';
$lang['go_control_panel']	= 'Go to Control Panel';

/* End of file complete_lang.php */