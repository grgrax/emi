INSERT INTO `f1_banks` (`id`, `city_id`, `name`, `swiftCode`, `address`) VALUES
	(1, 12, 'Ace Development Bank Limited', 'ACE199', 'Naxal, Kathmandu'),
	(2, 12, 'Agriculture Dev. Bank. Ltd', 'AGR2', 'Kathmandu'),
	(3, 12, 'Alpic Everest Finance Ltd.', 'ALP3', 'Kathmandu Mall, Kathmandu'),
	(4, 37, 'Alpine Development Bank Limited', 'ALP4', 'Hetauda, Makawanpur'),
	(5, 13, 'Annapurna Development Bank Limited', 'ANN5', 'Banepa, Kavre'),
	(6, 45, 'Annapurna Finance Co Ltd', 'ANN6', 'Pokhara, Kaski'),
	(7, 45, 'Api Finance Ltd.', 'API7', 'Pokhara, Kaski'),
	(8, 13, 'Arniko Development Bank Limited', 'ARN8', 'Dhulekhel, Kavre'),
	(9, 33, 'Arun Finance Ltd', 'ARU9', 'Dharan, Sunsari'),
	(10, 67, 'Bageshowri Development Bank Limited', 'BAG10', 'Nepalgunj, Banke'),
	(11, 54, 'Bagmati  Development Bank Limited', 'BAG11', 'Hariwon, Sarlahi'),
	(12, 12, 'Baibhab Finance Ltd.', 'BAI12', 'Naya Baneshwor, Kathmandu'),
	(13, 12, 'Bank Of Asia Nepal Limited', 'BAN13', 'Kathmandu'),
	(14, 12, 'Bank Of Kathmandu  Limited', 'BAN14', 'Kathmandu'),
	(15, 12, 'Bhajuratna Finance & Saving Co. Ltd.', 'BHA15', 'Kanthipath, Kathmandu'),
	(16, 10, 'Bhaktpur Finance Ltd.', 'BHA16', 'Chyamsing, Bhaktpur'),
	(17, 67, 'Bhargab Bikash Bank Limited', 'BHA17', 'Nepalgunj, Banke'),
	(18, 61, 'Bhrikuti Bikash Bank Limited', 'BHR18', 'Butwal, Rupandehi'),
	(19, 31, 'Biratlaxmi Bikash Bank Limited', 'BIR19', 'Biratnagar, Morang'),
	(20, 45, 'Biswo Bikash Bank Limited', 'BIS20', 'Pokhara, Kaski'),
	(21, 13, 'Bright  Development Bank Limited', 'BRI21', 'Panouti, Kavre'),
	(22, 45, 'Business Development Bank Limited', 'BUS22', 'Pokhara, Kaski'),
	(23, 61, 'Butwal Finance Ltd.', 'BUT23', 'Butwal, Rupandehi'),
	(24, 12, 'Capital Merchant Banking & Finance Co. Ltd.', 'CAP24', 'Battisputali, Kathmandu'),
	(25, 14, 'Central Finance Ltd.', 'CEN25', 'Kupondole, Lalitpur'),
	(26, 12, 'Century Commercial Bank Limited', 'CEN26', 'Putalisadak, Kathmandu'),
	(27, 12, 'Citizens Bank International Limited', 'CIT27', 'Kathmandu'),
	(28, 45, 'City Development Bank Limited', 'CIT28', 'Pokhara, Kaski'),
	(29, 11, 'Civic  Development Bank Limited', 'CIV29', 'Dhadingbesi , Dhading'),
	(30, 12, 'Civil Bank Limited', 'CIV30', 'Kamaladi,kathmandu'),
	(31, 12, 'Civil Merchant Banking And Finance Ltd.', 'CIV31', 'Kuleshwor, Kathmandu'),
	(32, 12, 'Clean Energy Development Bank Limited', 'CLE32', 'Sitapaila,kathmandu'),
	(33, 12, 'Cmb Finance Ltd', 'CMB33', 'Kamalashhi, Kathmandu'),
	(34, 12, 'Commerz & Trust Bank Nepal Limited', 'COM34', 'Kamaladi,kathmandu'),
	(35, 38, 'Corporate Development Bank Limited', 'COR35', 'Birjung Parsa'),
	(36, 44, 'Cosmos  Development Bank Limited', 'COS36', 'Gorhabazar,gorkha'),
	(37, 13, 'Country Development Bank Limited', 'COU37', 'Banepa, Kavre'),
	(38, 12, 'Crystal Finance Ltd.', 'CRY38', 'Thapathali, Kathmandu'),
	(39, 12, 'Dcbl Bank Limited', 'DCB39', 'Kamaladi,kathmandu'),
	(40, 13, 'Diyalo Bikash Bank Limited', 'DIY40', 'Banepa, Kavre'),
	(41, 12, 'Everest Bank Limited', 'EVE41', 'Kathmandu'),
	(42, 61, 'Everest Finance Ltd.', 'EVE42', 'Siddharthanagar, Rurandehi'),
	(43, 2, 'Excel Development Bank Limited', 'EXC43', 'Birtamod, Jhapa'),
	(44, 45, 'Fewa Finance Ltd.', 'FEW44', 'Pokhara, Kaski'),
	(45, 45, 'Gandaki Bikash Bank Limited', 'GAN45', 'Pokhara, Kaski'),
	(46, 45, 'Garami Bikash Bank Limited', 'GAR46', 'Pokhara, Kaski'),
	(47, 6, 'Gaumukhi Bikash Bank Limited', 'GAU47', 'Bijuwar, Pyuthan'),
	(48, 59, 'Gaurishakar Development Bank Limited', 'GAU48', 'Kawasoti, Nawalparasi'),
	(49, 12, 'General Finance Ltd.', 'GEN49', 'Chabahil, Kathmandu'),
	(50, 12, 'Global Bank Limited', 'GLO50', 'Kathmandu'),
	(51, 12, 'Goodwill Finance Ltd.', 'GOO51', 'Dillibazar, Kathmandu'),
	(52, 12, 'Gorkha Bikash Bank Limited', 'GOR52', 'Putalisadak, Kathmandu'),
	(53, 12, 'Gorkha Finance Ltd.', 'GOR53', 'Hattisar, Kathmandu'),
	(54, 14, 'Guheshwori Merchant Banking & Finance Ltd.', 'GUH54', 'Pulchowk, Lalitpur'),
	(55, 57, 'Gulmi Bikash Bank Limited', 'GUL55', 'Tamghas, Gulmi'),
	(56, 12, 'H. & B. Development Bank Limited', 'H. 56', 'Kamalpokhari, Kathmandu'),
	(57, 12, 'Hama Merchant & Finance Ltd.', 'HAM57', 'Tripureshwor, Kathmandu'),
	(58, 15, 'Hamro Bikash Bank Limited', 'HAM58', 'Battar, Nuwakot'),
	(59, 12, 'Himalaya Fiance Ltd.', 'HIM59', 'Sudhara, Kathmandu'),
	(60, 12, 'Himalayan Bank Limited', 'HIM60', 'Kathmandu'),
	(61, 12, 'Icfc Finance Ltd.', 'ICF61', 'Bhatbhateni, Kathmandu'),
	(62, 12, 'Ime Financial Institution Ltd.', 'IME62', 'Panipokhari, Kathmandu'),
	(63, 12, 'Imperial Finance Ltd.', 'IMP63', 'Thapathali, Kathmandu'),
	(64, 38, 'Inbesta Finance Ltd.', 'INB64', 'Birgunj, Parsa'),
	(65, 13, 'Infrastructure Development Bank Limited', 'INF65', 'Banepa, Kavre'),
	(66, 61, 'Innovative  Development Bank Limited', 'INN66', 'Siddhardhanagar, Rupandehi'),
	(67, 12, 'International  Development Bank Limited', 'INT67', 'Taku, Kathmandu'),
	(68, 12, 'International Leasing & Finance Co. Ltd.', 'INT68', 'Nayabaneshwor, Kathmandu'),
	(69, 12, 'Janata Bank Nepal Limited', 'JAN70', 'New Baneshwor, Kathmandu'),
	(70, 6, 'Jhimruk Bikash Bank Limited', 'JHI72', 'Bagdula, Pyuthan'),
	(71, 12, 'Jyoti Development Bank Limited', 'JYO73', 'Kamalpokhari, Kathmandu'),
	(72, 30, 'Kabeli Bikash Bank Limited', 'KAB74', 'Dhankuta'),
	(73, 71, 'Kakre Bihar Bikash Bank Limited', 'KAK75', 'Birendranagar, Surkhet'),
	(74, 45, 'Kamana Bikash Bank Limited', 'KAM76', 'Lekhnath, Kaski'),
	(75, 43, 'Kanchan  Development Bank Limited', 'KAN77', 'Mahendranagar, Kanchanpur'),
	(76, 2, 'Kankai Bikash Bank Limited', 'KAN78', 'Dhamak, Jhapa'),
	(77, 67, 'Karnali Bikash Bank Limited', 'KAR79', 'Nepalgunj, Banke'),
	(78, 45, 'Kaski Finance Ltd.', 'KAS80', 'Pokhara, Kaski'),
	(79, 12, 'Kasthamandap Development Bank Limited', 'KAS81', 'Newroad, Kathmandu'),
	(80, 12, 'Kathmandu Finance Ltd.', 'KAT82', 'Dillibazar, Kathmandu'),
	(81, 32, 'Khandbari  Development Bank Limited', 'KHA83', 'Khandbari, Sankhuwasava'),
	(82, 12, 'Kist Bank Limited', 'KIS84', 'Anamnagar, Kathmandu'),
	(83, 12, 'Kuber Merchant Banking & Finance Ltd.', 'KUB85', 'Kamalpokhari, Kathmandu'),
	(84, 12, 'Kumari Bank Limited', 'KUM86', 'Kathmandu'),
	(85, 14, 'Lalitpur Finance Co. Ltd.', 'LAL87', 'Lagankhel, Lalitpur'),
	(86, 38, 'Laxmi Bank Limited', 'LAX88', 'Birgunj, Parsa'),
	(87, 12, 'Lord Buddha Finance Ltd.', 'LOR89', 'Fasikeb, Kathmandu'),
	(88, 12, 'Lotus Investment Finance Ltd.', 'LOT90', 'Newroad, Kathmandu'),
	(89, 36, 'Lumbini Bank Limited', 'LUM91', 'Narayangar, Chitwan'),
	(90, 12, 'Lumbini Finance & Leasing Co. Ltd.', 'LUM92', 'Thamel, Kathmandu'),
	(91, 45, 'Machapuchhre Bank Limited', 'MAC93', 'Pokhara,kaski'),
	(92, 12, 'Machhapuchhre Bank Limited', 'MBLBNK001', 'Putalisadak'),
	(93, 43, 'Mahakali Bikash Bank Limited', 'MAH94', 'Mahendranagar, Kanchanpur'),
	(94, 12, 'Mahalaxmi Finance Ltd.', 'MAH95', 'Putalisadak, Kathmandu'),
	(95, 66, 'Malika Development Bank Limited', 'MAL96', 'Dhangadhi, Kailali'),
	(96, 12, 'Manakamana Development Bank Limited', 'MAN97', 'Durbarmargh, Kathmandu'),
	(97, 44, 'Manasalu  Development Bank Limited', 'MAN98', 'Buspark,gorkha'),
	(98, 12, 'Manjushree Financial Institution Ltd.', 'MAN99', 'New Baneshwor, Kathmandu'),
	(99, 55, 'Matribhumi Bikash Bank Limited', 'MAT100', 'Sindhulimadi, Sindhuli'),
	(100, 12, 'Mega Bank Nepal Limited', 'MEG101', 'Kantipath, Kathmandu'),
	(101, 38, 'Mercantile Finance Co. Ltd.', 'MER102', 'Birgunj, Parsa'),
	(102, 12, 'Merchant Finance Co. Ltd.', 'MER103', 'Newroad, Kathmandu'),
	(103, 45, 'Metro Development Bank Limited', 'MET104', 'Pokhara, Kaski'),
	(104, 61, 'Mission Development Bank Limited', 'MIS105', 'Butwal, Rupandehi'),
	(105, 33, 'Miteri Development Bank Limited', 'MIT106', 'Dharan, Sunsari'),
	(106, 34, 'Mount Makalu  Development Bank Limited', 'MOU107', 'Basantapur,terathum'),
	(107, 45, 'Muktinath Bikash Bank Limited', 'MUK108', 'Pokhara, Kaski'),
	(108, 25, 'Multipurpose Finance Co. Ltd.', 'MUL109', 'Rajbiraj, Saptari'),
	(109, 12, 'Nabil Bank Ltd', 'NAB110', 'Kathmandu'),
	(110, 5, 'Namaste Bittiya Sansthan Ltd.', 'NAM111', 'Ghorai, Dang'),
	(111, 36, 'Narayani Development Bank Limited', 'NAR112', 'Ratnanagar, Chitwan'),
	(112, 12, 'Narayani National Finance Ltd', 'NAR113', 'Kalikasthan, Ktahmandu'),
	(113, 10, 'Nava Durga Finance Co. Ltd', 'NAV114', 'Itachhe, Bhaktpur'),
	(114, 12, 'Ndep Development Bank Limited', 'NDE115', 'Kamaladi, Kathmandu'),
	(115, 12, 'Nepal Bangladesh Bank Limited', 'NEP116', 'Kathmandu'),
	(116, 12, 'Nepal Bank Ltd', 'NEP117', 'Kathmandu'),
	(117, 61, 'Nepal Community  Development Bank Limited', 'NEP118', 'Butwal, Rupandehi'),
	(118, 45, 'Nepal Consumer  Development Bank Limited', 'NEP119', 'Pokhara, Kaski'),
	(119, 61, 'Nepal Credit & Commerce Bank Limited', 'NEP120', 'Siddharthanagar'),
	(120, 12, 'Nepal Express Finance Ltd.', 'NEP121', 'Sundhara, Kathmandu'),
	(121, 12, 'Nepal Finance Ltd', 'NEP122', 'Kamaladi, Kathmandu'),
	(122, 12, 'Nepal Housing &  Merchant Finance Ltd.', 'NEP123', 'Dillibazar, Kathmandu'),
	(123, 12, 'Nepal Housing Development Finance Co Ltd.', 'NEP124', 'Bijulibazar, Kathmandu'),
	(124, 31, 'Nepal Industrial & Commercial Bank Limited', 'NEP125', 'Biratnagar, Morang'),
	(125, 12, 'Nepal Industrial Development Corporation', 'NEP126', 'Durbar Marg, Kathmandu'),
	(126, 12, 'Nepal Investment Bank Ltd', 'NEP127', 'Kathmandu'),
	(127, 12, 'Nepal Sbi Bank Limited', 'NEP128', 'Kathmandu'),
	(128, 12, 'Nepal Share Markets And Finance Ltd', 'NEP129', 'Ramshahapath, Kathmandu'),
	(129, 12, 'Nidc Capital Markets Ltd', 'NID130', 'Kamalpokhari, Kathmandu'),
	(130, 74, 'Nilgiri Bikash Bank Limited', 'NIL131', 'Beni, Myagdi'),
	(131, 12, 'Nmb Bank Limited', 'NMB132', 'Babarmahal, Kathmandu'),
	(132, 45, 'Om Finance Ltd.', 'OM 133', 'Pokhara, Kaski'),
	(133, 61, 'Pachhimanchal Finance Co. Ltd.', 'PAC134', 'Butwal, Rupandehi'),
	(134, 46, 'Pacific  Development Bank Limited', 'PAC135', 'Beshishahar Lamjung'),
	(135, 61, 'Pashimanchal Development Bank Limited', 'PAS136', 'Mitrapark, Rupandehi'),
	(136, 13, 'Pashupati Development Bank Limited', 'PAS137', 'Banepa, Kavre'),
	(137, 14, 'Patan Finance Co. Ltd.', 'PAT138', 'Pulchowk, Lalitpur'),
	(138, 45, 'Pathibhara Bikash Bank Limited', 'PAT139', 'Pokhara, Kaski'),
	(139, 12, 'Peoples Finance Ltd', 'PEO140', 'Mahaboudha, Kathmandu'),
	(140, 45, 'Pokhara Finance Ltd.', 'POK141', 'Pokhara, Kaski'),
	(141, 12, 'Prabhu Finance Ltd.', 'PRA142', 'Lainchur, Kathmandu'),
	(142, 14, 'Premier Finance Co. Ltd.', 'PRE143', 'Kumaripati, Lalitpur'),
	(143, 12, 'Prime Commercial Bank Limited', 'PRI144', 'Kathmandu'),
	(144, 13, 'Professional Bikash Bank Limited', 'PRO145', 'Banepa, Kavre'),
	(145, 12, 'Prudential Finance Company Ltd.', 'PRU146', 'Dillibazar, Kathmandu'),
	(146, 38, 'Public Development Bank Limited', 'PUB147', 'Birjunj, Parsa'),
	(147, 61, 'Purnima Bikash Bank Limited', 'PUR148', 'Siddhardhanagar, Rupandehi'),
	(148, 67, 'Raptibheri Bikash Bank Limited', 'RAP149', 'Nepalgunj, Banke'),
	(149, 71, 'Rara Bikash Bank Limited', 'RAR150', 'Birendranagar, Surkhet'),
	(150, 12, 'Rastriya Banijya Bank Ltd', 'RAS151', 'Kathmandu'),
	(151, 12, 'Reliable Finance Ltd.', 'REL152', 'Sundhara, Kathmandu'),
	(152, 12, 'Reliance Finance Ltd.', 'REL153', 'Praarsani Marg, Kathmandu'),
	(153, 57, 'Resunga Bikash Bank Limited', 'RES154', 'Tamghas, Gulmi'),
	(154, 59, 'Rising Development Bank Limited', 'RIS155', 'Gaidakot, Nawalparasi'),
	(155, 12, 'Royal Merchant Banking & Finance Ltd.', 'ROY156', 'Durbarmarg, Kathmandu'),
	(156, 14, 'Sagarmatha Merchant Banking & Finance Ltd.', 'SAG157', 'Maanvawan, Lalitpur'),
	(157, 54, 'Sahara  Development Bank Limited', 'SAH158', 'Mahangawa, Sarlahi'),
	(158, 50, 'Sahayogi Bikash Bank Limited', 'SAH159', 'Janakpur,dhanusha'),
	(159, 11, 'Samabriddhi  Development Bank Limited', 'SAM160', 'Gajuri,dhading'),
	(160, 13, 'Samjhana Finance Co. Ltd.', 'SAM161', 'Banepa, Kavre'),
	(161, 45, 'Sangrila Bikash Bank Limited', 'SAN162', 'Newroad Pokhara, Kaski'),
	(162, 12, 'Sanima Bikash Bank Limited', 'SAN163', 'Kamalpokhari, Kathmandu'),
	(163, 66, 'Seti Bittiya Sansthan. Ltd', 'SET164', 'Tikapur, Kailali'),
	(164, 61, 'Sewa Bikash Bank Limited', 'SEW165', 'Butwal, Rupandehi'),
	(165, 12, 'Shikhar Finance Ltd.', 'SHI166', 'Thapathali, Kathmandu'),
	(166, 61, 'Shine  Development Bank Limited', 'SHI167', 'Butwal, Rupandehi'),
	(167, 12, 'Shree Investment & Finance Co. Ltd.', 'SHR168', 'Dillibazar, Kathmandu'),
	(168, 31, 'Shrijana Finance Ltd.', 'SHR169', 'Biratnagar, Morang'),
	(169, 36, 'Shubhechchha Bikash Bank Limited', 'SHU170', 'Narayangadh, Chitwan'),
	(170, 12, 'Siddhartha Bank Limited', 'SID171', 'Kathmandu'),
	(171, 12, 'Siddhartha Development Bank Limited', 'SID172', 'Tinkune,kathmandu'),
	(172, 61, 'Siddhartha Finance Ltd.', 'SID173', 'Siddharthanagar, Purandehi'),
	(173, 17, 'Sindhu Bikash Bank Limited', 'SIN174', 'Barhabise, Sindhupalchowk'),
	(174, 12, 'Social  Development Bank Limited', 'SOC175', 'Naxal, Kathmandu'),
	(175, 12, 'Standard Chartered Bank Nepal Ltd', 'STA176', 'Kathmandu'),
	(176, 12, 'Standard Finance Ltd.', 'STA177', 'Narayanchaur, Kathmandu'),
	(177, 12, 'Subhalaxmi Finance Ltd.', 'SUB178', 'Naxal, Kathmandu'),
	(178, 12, 'Sunrise Bank Limited', 'SUN179', 'Kathmandu'),
	(179, 51, 'Surya  Development Bank Limited', 'SUR180', 'Charikot, Dolkha'),
	(180, 12, 'Suryadarshan Finance Co.ltd.', 'SUR181', 'Newbaneshwor, Kathmandu'),
	(181, 12, 'Swostik Merchant Finance Co Ltd', 'SWO182', 'Khichapokhari, Kathmandu'),
	(182, 61, 'Tinau Bikash Bank Limited', 'TIN183', 'Butwal, Rupandehi'),
	(183, 12, 'Tourism  Development Bank Limited', 'TOU184', 'Thamel, Kathmandu'),
	(184, 36, 'Triveni Development Bank Limited', 'TRI185', 'Narayangadh, Chitwan'),
	(185, 36, 'Uddyam Development Bank Limited', 'UDD186', 'Narayangadh, Chitwan'),
	(186, 12, 'Union Finance Ltd.', 'UNI187', 'Kamaladi, Kathmandu'),
	(187, 12, 'Unique Financial Institution Ltd.', 'UNI188', 'Putalisadak, Kathmandu'),
	(188, 35, 'United Development  Bank Limited', 'UNI189', 'Jeetpur, Bara'),
	(189, 12, 'United Finance Co. Ltd.', 'UNI190', 'Durbarmarg, Kathmandu'),
	(190, 12, 'Universal Finance Ltd.', 'UNI191', 'Kantipath, Kathmandu'),
	(191, 12, 'Valley Finance Ltd.', 'VAL192', 'Maharagunj, Kathmandu'),
	(192, 67, 'Vibor Bikash Bank Limited', 'VIB193', 'Nepalgunj, Banke'),
	(193, 5, 'Western Development Bank Limited', 'WES194', 'Gorahi, Dang'),
	(194, 37, 'World Merchant Banking 7 Finance Ltd.', 'WOR195', 'Hetauda, Makawanpur'),
	(195, 37, 'Yeti Finance Ltd.', 'YET196', 'Hetauda, Makawanpur'),
	(196, 12, 'Zenith Finance Ltd.', 'ZEN197', 'Newroad, Kathmandu');