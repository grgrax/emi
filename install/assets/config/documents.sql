INSERT INTO `f1_idocuments` (`id`, `name`, `description`, `created`) VALUES
	(1, 'Citizenship', 'National Identification Card', now()),
	(2, 'Passport', 'International Identification Card', now());
