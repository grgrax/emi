INSERT INTO `f1_groups` (`id`, `name`, `description`, `active`, `created`) VALUES
	(1, 'Super Admin', 'The Super Admin', 1, '2012-09-25 12:48:20'),
	(2, 'Principal Agent Admin', 'The admin of the Principal Agent', 1, '2012-09-25 12:50:33'),
	(3, 'Checker', 'The Subagent Admin/Checker', 1, '2012-09-25 12:52:10'),
	(4, 'Maker', 'The maker group', 1, '2012-09-25 12:54:26');