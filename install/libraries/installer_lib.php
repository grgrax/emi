<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

class Installer_lib {

	private $ci;
	public $php_version;
	public $mysql_server_version;
	public $mysql_client_version;
	public $gd_version;

	function __construct()
	{
		$this->ci =& get_instance();
	}


	function php_acceptable($version = NULL)
	{
		$this->php_version = phpversion();

		return ( version_compare(PHP_VERSION, $version, '>=') ) ? TRUE : FALSE;
	}


	/**
	 * @param 	string $type The MySQL type, client or server
	 * @return 	string The MySQL version of either the server or the client
	 *
	 * Function to retrieve the MySQL version (client/server)
	 */
	public function mysql_acceptable($type = 'server')
	{
		if ($type == 'server')
		{
			$server 	= $this->ci->session->userdata('hostname') . ':' . $this->ci->session->userdata('port');
			$username 	= $this->ci->session->userdata('username');
			$password 	= $this->ci->session->userdata('password');

			if ( $db = @mysql_connect($server,$username,$password) )
			{
				$this->mysql_server_version = @mysql_get_server_info($db);
				@mysql_close($db);
				return ($this->mysql_server_version >= 5) ? TRUE : FALSE;
			}
			else
			{
				@mysql_close($db);
				return FALSE;
			}
		}

		else
		{
			$this->mysql_client_version = preg_replace('/[^0-9\.]/','', mysql_get_client_info());
			return ($this->mysql_client_version >= 5) ? TRUE : FALSE;
		}
	}

	public function gd_acceptable()
	{
		if (function_exists('gd_info'))
		{
			$gd_info = gd_info();
			$this->gd_version = preg_replace('/[^0-9\.]/','',$gd_info['GD Version']);
			return ($this->gd_version >= 1) ? TRUE : FALSE;
		}
		else
		{
			return FALSE;
		}
	}

	public function zlib_enabled()
	{
		return extension_loaded('zlib');
	}

	public function check_server($data)
	{
		if ( ! $this->php_acceptable($data->php_min_version) )
		{
			
			return FALSE;
		}
		if ( ! $this->mysql_acceptable('server') )
		{
			return FALSE;
		}
		if ( ! $this->mysql_acceptable('client') )
		{
			return FALSE;
		}

		if ( $this->gd_acceptable() === FALSE || $this->zlib_enabled() === FALSE)
		{
			return 'partial';
		}
		return TRUE;

	}

	public function verify_http_server($server_name)
	{
		if ($server_name == 'other')
		{
			return 'partial';
		}

		$supported_servers = $this->ci->config->item('supported_servers');

		return array_key_exists($server_name, $supported_servers);
	}

	 public function validate_mysql_db_name($db_name)
	 {
	 	$expr = '/[^A-Za-z0-9_-]+/';
	 	return !(preg_match($expr,$db_name)>0);
	 }

	public function test_db_connection()
	{
		$hostname = $this->ci->session->userdata('hostname');
		$username = $this->ci->session->userdata('username');
		$password = $this->ci->session->userdata('password');
		$port	  = $this->ci->session->userdata('port');

		return @mysql_connect("$hostname:$port", $username, $password);
	}

	public function install($data)
	{
		$server 	= $this->ci->session->userdata('hostname') . ':' . $this->ci->session->userdata('port');
		$username 	= $this->ci->session->userdata('username');
		$password 	= $this->ci->session->userdata('password');
		$database 	= $data['database'];

		$user_salt		= substr(md5(uniqid(rand(), true)), 0, 5);
		$data['user_password'] 	= sha1($data['user_password'] . $user_salt);

		include '../system/cms/config/migration.php';

		$user_sql = file_get_contents('./sql/default.sql');
		$user_sql = str_replace('{PREFIX}', $data['site_ref'].'_', $user_sql);
		$user_sql = str_replace('{EMAIL}', $data['user_email'], $user_sql);
		$user_sql = str_replace('{USER-NAME}', mysql_escape_string($data['user_name']), $user_sql);
		$user_sql = str_replace('{DISPLAY-NAME}', mysql_escape_string($data['user_firstname'] . ' ' . $data['user_lastname']), $user_sql);
		$user_sql = str_replace('{PASSWORD}', mysql_escape_string($data['user_password']), $user_sql);
		$user_sql = str_replace('{FIRST-NAME}', mysql_escape_string($data['user_firstname']), $user_sql);
		$user_sql = str_replace('{LAST-NAME}', mysql_escape_string($data['user_lastname']) , $user_sql);
		$user_sql = str_replace('{SALT}', $user_salt, $user_sql);
		$user_sql = str_replace('{NOW}', time(), $user_sql);
		$user_sql = str_replace('{MIGRATION}', $config['migration_version'], $user_sql);

		if ( ! $this->db = mysql_connect($server, $username, $password) )
		{
			return array('status' => FALSE,'message' => 'The installer could not connect to the MySQL server or the database, be sure to enter the correct information.');
		}
		
		if ($this->mysql_server_version >= '5.0.7')
		{
			@mysql_set_charset('utf8', $this->db);
		}

		if ( ! empty($data['create_db'] ))
		{
			mysql_query('CREATE DATABASE IF NOT EXISTS '.$database, $this->db);
		}
		if ( !mysql_select_db($database, $this->db) )
		{
			return array(
				'status'	=> FALSE,
				'message'	=> '',
				'code'		=> 101
			);
		}

		if ( ! $this->_process_schema($user_sql, FALSE) )
		{
			return array(
				'status'	=> FALSE,
				'message'	=> mysql_error($this->db),
				'code'		=> 104
			);
		}

		mysql_query(sprintf(
			"INSERT INTO core_sites (name, ref, domain, created_on) VALUES ('Default Site', '%s', '%s', '%s');",
			$data['site_ref'],
			preg_replace('/^www\./', '', $_SERVER['SERVER_NAME']),
			time()
		));

		mysql_close($this->db);
		if ( ! $this->write_db_file($database) )
		{
			return array(
						'status'	=> FALSE,
						'message'	=> '',
						'code'		=> 105
					);
		}
		if ( ! $this->write_config_file() )
		{
			return array(
						'status'	=> FALSE,
						'message'	=> '',
						'code'		=> 106
					);
		}

		return array('status' => TRUE);
	}

	private function _process_schema($schema_file, $is_file = TRUE)
	{
		if ( $is_file == TRUE )
		{
			$schema 	= file_get_contents('./sql/' . $schema_file . '.sql');
		}
		else
		{
			$schema 	= $schema_file;
		}
		$queries 	= explode('-- command split --', $schema);

		foreach($queries as $query)
		{
			$query = rtrim( trim($query), "\n;");

			@mysql_query($query, $this->db);

			if (mysql_errno($this->db) > 0)
			{
				return FALSE;
			}
		}

		return TRUE;
	}

	function write_db_file($database)
	{
		$server 	= $this->ci->session->userdata('hostname');
		$username 	= $this->ci->session->userdata('username');
		$password 	= $this->ci->session->userdata('password');
		$port		= $this->ci->session->userdata('port');

		$template 	= file_get_contents('./assets/config/database.php');

		$replace = array(
			'__HOSTNAME__' 	=> $server,
			'__USERNAME__' 	=> $username,
			'__PASSWORD__' 	=> $password,
			'__DATABASE__' 	=> $database,
			'__PORT__' 		=> $port ? $port : 3306
		);

		$new_file  	= str_replace(array_keys($replace), $replace, $template);

		$handle 	= @fopen('../system/cms/config/database.php','w+');

		if ($handle !== FALSE)
		{
			return @fwrite($handle, $new_file);
		}

		return FALSE;
	}

	function write_config_file()
	{
		$template = file_get_contents('./assets/config/config.php');

		$server_name = $this->ci->session->userdata('http_server');
		$supported_servers = $this->ci->config->item('supported_servers');
		if ($supported_servers[$server_name]['rewrite_support'] !== FALSE)
		{
			$index_page = '';
		}

		else
		{
			$index_page = 'index.php';
		}
		$new_file = str_replace('__INDEX__', $index_page, $template);
		$handle = @fopen('../system/cms/config/config.php','w+');
		if ($handle !== FALSE)
		{
			return fwrite($handle, $new_file);
		}

		return FALSE;
	}

	public function curl_enabled()
    {
		return (bool) function_exists('curl_init');
    }
}

/* End of file installer_lib.php */