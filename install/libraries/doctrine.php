<?php 

use Transborder\Common\Util\TBSQLLogger;

use Doctrine\Common\ClassLoader,
    Doctrine\ORM\Configuration,
    Doctrine\ORM\EntityManager,
    Doctrine\Common\Cache\ArrayCache,
	Doctrine\Common\Annotations\AnnotationReader,
	Doctrine\ORM\Mapping\Driver\AnnotationDriver,
    Doctrine\DBAL\Logging\EchoSqlLogger,
	Doctrine\DBAL\Event\Listeners\MysqlSessionInit,
	Doctrine\ORM\Tools\SchemaTool,
	Doctrine\Common\EventManager,
	Gedmo\Timestampable\TimestampableListener,
	Gedmo\Sluggable\SluggableListener,
	Gedmo\Tree\TreeListener,
	Gedmo\SoftDeleteable\SoftDeleteableListener,
	Gedmo\Loggable\LoggableListener;
if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Doctrine {

 /**
 * 
 * @var Doctrine\ORM\EntityManager
 */	
  public $em = null;
  
  /**
   * 
   * @var Doctrine\ORM\Tools\SchemaTool
   */
  public $tool = null;

  public function __construct($dbconfig)
  {
  	$CI = &get_instance();
	
	require_once '../application/third_party/Doctrine/Common/ClassLoader.php';

    $doctrineClassLoader = new ClassLoader('Doctrine',  '../application/third_party');
    $doctrineClassLoader->register();
    
	// Set up Gedmo
	$classLoader = new ClassLoader('Gedmo', '../application/third_party');
	$classLoader->register();
	
	// Set up Transborder
	$classLoader = new ClassLoader('Transborder', '../application/third_party');
	$classLoader->register();
	
	$F1softExtensionsLoader = new ClassLoader('F1soft', '../application/third_party');
	$F1softExtensionsLoader->register();
	
	// Set up models loading
	$entitiesClassLoader = new ClassLoader('models', '../application/');
	$entitiesClassLoader->register();
	
	foreach (glob(APPPATH.'modules/*', GLOB_ONLYDIR) as $m) {
		$module = str_replace(APPPATH.'modules/', '', $m);
		$entitiesClassLoader = new ClassLoader($module, '../application/modules');
		$entitiesClassLoader->register();
	}

    $proxiesClassLoader = new ClassLoader('Proxies', '../application/models/proxies');
    $proxiesClassLoader->register();

    
    
    // ensure standard doctrine annotations are registered
    Doctrine\Common\Annotations\AnnotationRegistry::registerFile(
    	'../application/third_party/Doctrine/ORM/Mapping/Driver/DoctrineAnnotations.php'
    );
    
    Doctrine\Common\Annotations\AnnotationRegistry::registerFile(
    		'../application/third_party/Transborder/REST/Annotations/Arg.php'
    );
    	
    
    // Set up caches
    
    $cache = new Doctrine\Common\Cache\ArrayCache;
    
    $annotationReader = new Doctrine\Common\Annotations\AnnotationReader;
    $cachedAnnotationReader = new Doctrine\Common\Annotations\CachedReader(
    		$annotationReader, // use reader
    		$cache // and a cache driver
    );
    
    // create a driver chain for metadata reading
    $driverChain = new Doctrine\ORM\Mapping\Driver\DriverChain();
    
    // load superclass metadata mapping only, into driver chain
    // also registers Gedmo annotations.NOTE: you can personalize it
    Gedmo\DoctrineExtensions::registerAbstractMappingIntoDriverChainORM(
    		$driverChain, // our metadata driver chain, to hook into
    		$cachedAnnotationReader // our cached annotation reader
    );
    
    // now we want to register our application entities,
	// for that we need another metadata driver used for Entity namespace
	$annotationDriver = new Doctrine\ORM\Mapping\Driver\AnnotationDriver(
	    $cachedAnnotationReader, // our cached annotation reader
	    array('../application/models/') // paths to look in
	);
    
	
	// NOTE: driver for application Entity can be different, Yaml, Xml or whatever
	// register annotation driver for our application Entity namespace
	$driverChain->addDriver($annotationDriver, 'models');
    
	
	$logger = new TBSQLLogger();
	
    // general ORM configuration
	$config = new Doctrine\ORM\Configuration;
	$config->setProxyDir('../application/models/proxies');
    $config->setProxyNamespace('Proxies');
	$config->setAutoGenerateProxyClasses(TRUE); // this can be based on production config.
	// register metadata driver
	$config->setMetadataDriverImpl($driverChain);
	// use our allready initialized cache driver
	$config->setMetadataCacheImpl($cache);
	$config->setQueryCacheImpl($cache);
	
// 	$config->setSQLLogger($logger);
	
	// Third, create event manager and hook prefered extension listeners
	$evm = new Doctrine\Common\EventManager();

    // gedmo extension listeners

	// sluggable
	$sluggableListener = new Gedmo\Sluggable\SluggableListener;
	// you should set the used annotation reader to listener, to avoid creating new one for mapping drivers
	$sluggableListener->setAnnotationReader($cachedAnnotationReader);
	$evm->addEventSubscriber($sluggableListener);
	
	//loggable
	$loggableListener = new Gedmo\Loggable\LoggableListener;
	$loggableListener->setAnnotationReader($cachedAnnotationReader);
	$evm->addEventSubscriber($loggableListener);
	
	
	// timestampable
	$timestampableListener = new Gedmo\Timestampable\TimestampableListener;
	$timestampableListener->setAnnotationReader($cachedAnnotationReader);
	$evm->addEventSubscriber($timestampableListener);
	
	
	
	//soft delete
	$softdeletelistener = new Gedmo\SoftDeleteable\SoftDeleteableListener;
	$softdeletelistener->setAnnotationReader($cachedAnnotationReader);
	$evm->addEventSubscriber($softdeletelistener);
	
    //setup soft delete filter
    $config->addFilter('soft-deleteable', 'Gedmo\SoftDeleteable\Filter\SoftDeleteableFilter');
    
    $config->addFilter('f1soft-pagent', 'Transborder\DoctrineExtensions\Filters\PAgentFilter');
     
    // Database connection information
    $connectionOptions = array(
        'driver'	=> 'pdo_mysql',
        'user' 		=> $dbconfig['username'],
        'password' 	=> $dbconfig['password'],
        'host' 		=> $dbconfig['hostname'],
        'dbname'	=> $dbconfig['database']
    );

    // Create EntityManager
    $this->em = EntityManager::create($connectionOptions, $config,$evm);
//     $this->em->getFilters()->enable('soft-deleteable');
//     $this->em->getFilters()->enable('f1soft-pagent');
	// Schema Tool
	$this->tool = new SchemaTool($this->em);
	
  }
	
}