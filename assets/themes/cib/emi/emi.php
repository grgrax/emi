<div id="print_div">

  <span class="client_logo">
    <img src="<?php echo site_url('assets/images/logo.jpg')?>" alt="Sunrise Bank Limited">
  </span>
  <br>
  
  <?php if(isset($cbsSuccess) && $cbsSuccess===true){ ?>
  <div class="alerts">
    <?php if(isset($cbsSuccessMsg) && $cbsSuccessMsg!=''){?>
    <div class="response success">
      <?php echo "<p>".$cbsSuccessMsg."</p>"; ?>      
    </div>
    <?php } elseif(isset($cbsErrorMsg) && $cbsErrorMsg){ ?>
    <div class="response danger">
      <p><?php echo $cbsErrorMsg?></p>      
    </div>
    <?php } ?>
  </div>
  <br>
  <?php } elseif(isset($cbsErrorMsg) && $cbsErrorMsg){ ?>
  <div class="response danger">
    <p><?php echo $cbsErrorMsg?></p>      
  </div>
  <?php } ?>

  <form method="post" name="support" class="validForm"> 
    <span class="no_print">
      <fieldset>               
        <div class="grid_12">
          <h2> Emi Calculator (Sunrise Surakshit Ghar Karja) </h2>

          <div class="grid_5">
            <p class="element">
              <label>Loan Amount</label>
              <input type="text" name="principal" id="principal" class="required"
              placeholder="5000000"
              value="<?php echo set_value('principal');?>"/> (NPR.)
            </p>
            <p class="element">
              <label>Interest Rate</label>
              <input type="text" name="interest" id="interest"  class="required"
              placeholder="10.50"
              value="<?php echo set_value('interest');?>"/> %
            </p>
            <p class="element">
              <label>Loan Tenure</label>
              <input type="text" name="term"  id="term" class="required"
              placeholder="15"
              value="<?php echo set_value('term');?>"/> <i><?php echo "Range {$min['year']} - {$max['year']}";?></i> (years)
            </p>
            <p class="element">
              <label>Applicant's Age</label>
              <input type="text" name="age" id="age"  class="required" 
              placeholder="35"
              value="<?php echo set_value('age');?>"/> <i><?php echo "Range {$min['age']} - {$max['age']}";?></i>
            </p>
            <p class="element">
              <label>Loan Disbursement Date</label>
              <?php isset($_POST['start_date'])?$date=$this->input->post('start_date'):$date=date('Y-m-d'); ?>
              <input type="text" name="start_date" id="start_date" autocomplete="off" class="datepicker required"
              value="<?php echo set_value('start_date',$date);?>"/> 
            </p> 
            <p class="element">
              <label>Emi Start Date</label>
              <?php isset($_POST['emi_start_date'])?$emi_date=$this->input->post('emi_start_date'):$emi_date=date('Y-m-d'); ?>
              <input type="text" name="emi_start_date" id="emi_start_date" autocomplete="off" class="datepicker required"
              value="<?php echo set_value('emi_start_date',$emi_date);?>"/> 
            </p> 
            <p class="element">
              <label>Premium Payment Mode</label>
              <select name="payment_mode" id="payment_mode" class="required">
                <option value=""> Select </option>
                <?php foreach ($discounts as $discount) { ?>
                <option value="<?php echo $discount->id();?>" 
                  <?php if(isset($_POST['payment_mode'])){
                    echo $discount->id()==$this->input->post('payment_mode')?'selected':'';
                  }
                  ?>>
                  <?php echo $discount->getName();?> 
                  ( Pay. Dis.: <?php echo $discount->getPaymentDiscount();?> ,
                  Cor. Dis.: <?php echo $discount->getCorporateDiscount();?> )
                </option>        
                <?php } ?>
              </select>
            </p> 

            <p class="element">
              <label>Include Benefit</label>
              <input type="checkbox" name="benefiet" id="benefiet_1" autocomplete="off" 
              value="1"
              <?php echo isset($_POST['benefiet'])?($_POST['benefiet']>0?'checked':''):''?>
              /> ADB (Accidental Death Benefit)
            </p>
            <p class="element">
              <label></label>
              <input type="checkbox" name="benefiet" id="benefiet_2" autocomplete="off" 
              value="2"
              <?php echo isset($_POST['benefiet'])?($_POST['benefiet']==2?'checked':''):''?>
              /> TPDPW (Total Permanent Disability Benefit)
            </p>
            <p class="element">
              <i>  
                Please note that while taking TPDPW, option 1(ADB) also to be mandatorily taken.      
              </i>
            </p>
            <hr>
            <p class="element">
             <label>&nbsp;</label> 
             <input type="submit" name="calculate" value="Calculate"/>
             <input type="reset" name="reset" value="Clear" id="clear"/>              
           </p>

           <?php if(isset($installments) && count($installments)>0){?>
           <span class="file_generator_parameters">
             <p class="element">
              Loan Generation Parameter.      
            </p>
            <p class="element">
              <label>T24 Username</label>
              <input type="text" name="customer_username" id="customer_username" 
              placeholder="SMSBNK"
              value="<?php echo set_value('customer_username');?>"/>
            </p>
            <p class="element">
              <label>T24 Password</label>
              <input type="password" name="customer_password" id="customer_password" 
              placeholder="654321"
              value="<?php echo set_value('customer_password');?>"/>
            </p>
            <p class="element">
              <label>Branch Code</label>
              <input type="text" name="branch_code" id="branch_code" 
              placeholder="NP0010002"
              value="<?php echo set_value('branch_code','NP0010002');?>"/>
            </p>
            <p class="element">
              <label>Customer Identification Number</label>
              <input type="text" name="customer" id="customer" 
              placeholder="10073316"
              value="<?php echo set_value('customer');?>"/>
            </p>
            <p class="element">
              <label>Loan Disbursment Account</label>
              <input type="text" name="loan_disbursment_account" id="loan_disbursment_account" 
              placeholder="00210073316019"
              value="<?php echo set_value('loan_disbursment_account');?>"/>
            </p>
            <p class="element">
              <label>Principal Liquidation Account</label>
              <input type="text" name="principal_liquidation_account" id="principal_liquidation_account" 
              placeholder="00210073316019"
              value="<?php echo set_value('principal_liquidation_account');?>"/>
            </p>            
            <p class="element">
              <label>Interest Liquidation Account</label>
              <input type="text" name="interest_liquidation_account" id="interest_liquidation_account" 
              placeholder="00210073316019"
              value="<?php echo set_value('interest_liquidation_account');?>"/>
            </p>          
          </span>
          <p class="element">
           <label>&nbsp;</label> 
           <!-- <input type="submit" name="generate_file" value="G"/> -->
           <input type="submit" name="send_to_cbs" value="Send to CBS"/>
           <input type="reset" name="reset" value="Clear" id="clear_cbs"/>             
         </p>
         <?php } ?>



       </div>
     </fieldset>
   </span>

   <?php if($this->input->post() && $result && $emi) { ?>
   <div class="grid_8">
     <h3>Insurance Premium: </h3><hr>
     <p class="element">
      <label>Principal</label> : <?php echo isset($emi['principal'])?number_format($emi['principal']):'N/A'; ?>
    </p>
    <p class="element">
      <label>Interest Rate</label> : <?php echo isset($emi['interest'])?"{$emi['interest']} %":'N/A'; ?>
    </p>
    <p class="element">
      <label>Term</label> : <?php echo isset($emi['term'])?"{$emi['term']} years":'N/A'; ?>
    </p>
    <p class="element">
      <label>Premium Rate</label> : <?php echo isset($emi['mappedRate'])?$emi['mappedRate']:'N/A'; ?>
    </p>
    <p class="element">
      <label>Age</label> : <?php echo isset($emi['age'])?"{$emi['age']} years old":'N/A'; ?>
    </p>
    <p class="element">
      <label>Large Sum Discount</label> : -<?php echo isset($emi['largeSumDiscount'])?number_format($emi['largeSumDiscount'],2):'N/A'; ?>
    </p>
    <p class="element">
      <label>After Discount</label> : <?php echo isset($emi['afterDiscount'])?number_format($emi['afterDiscount'],2):'N/A'; ?>
    </p>
    <p class="element">
      <label>Annual Premium</label> : 
      <?php echo isset($emi['annualPremium'])?number_format($emi['annualPremium'],2):'N/A'; ?>
      <span class="no_print">
        <?php 
        if(ENVIRONMENT=='development' && isset($emi['annualPremium'])){
          echo " ---- {$emi['afterDiscount']} x ".number_format($emi['principal'])." /1000"; 
        }
        ?>
      </span>
    </p>
    <p class="element">
      <label>Dis. on Annual payment</label> : 
      <?php echo isset($emi['discountOnAnnualPayment'])?number_format($emi['discountOnAnnualPayment'],2):'N/A'; ?>
      ---- Payment Discount: 
      <i>
        <?php 
        if(isset($emi['paymentDiscount'])){
          echo number_format($emi['paymentDiscount'],2);
        }
        ?>
        %</i>
        <span class="no_print">
          <?php 
          if(ENVIRONMENT=='development' &&  isset($emi['paymentDiscount'])){
            echo " ---- {$emi['paymentDiscount']} x ".number_format($emi['annualPremium'])." /100"; 
          }
          ?>
        </span>

      </p>

      <p class="element">
        <label>Prem. After Discount</label> : <?php echo isset($emi['premiumAfterDiscount'])?number_format($emi['premiumAfterDiscount'],2):'N/A'; ?>
      </p>
      <p class="element">
        <label>Less Corporate Discount</label> : 
        <?php echo isset($emi['lessCorporateDiscount'])?number_format($emi['lessCorporateDiscount'],2):'N/A'; ?>
        ---- Corporate Discount: <i><?php echo isset($emi['corporateDiscount'])?number_format($emi['corporateDiscount'],2):'N/A'; ?>%</i>
        <span class="no_print">
          <?php 
          if(ENVIRONMENT=='development' &&  isset($emi['corporateDiscount'])){
            echo " ---- {$emi['corporateDiscount']} x ".number_format($emi['premiumAfterDiscount'])." /100"; 
          }
          ?>
        </span>
      </p>

      <?php if(isset($emi['benefitAmontOne'])) {?>
      <p class="element">
        <label>ADB (Accidental Death Benefit)</label> : 
        <?php echo number_format($emi['benefitAmontOne']); ?>
      </p>
      <?php if(isset($emi['benefitAmontTwo'])) {?>
      <p class="element line">
        <label>TPDPW (Total Permanent Disability Benefit)</label> : 
        <?php echo number_format($emi['benefitAmontTwo']); ?>
      </p>
      <?php } ?>
      <?php } ?>

      <p class="element">
        <label>Monthly Premium Amount (MPA)</label> : <?php echo isset($emi['monthlyPreiumAmount'])?number_format($emi['monthlyPreiumAmount'],2):'N/A'; ?>
        <span class="no_print">
          <?php 
          if(ENVIRONMENT=='development' && isset($emi['corporateDiscount'])){
            echo " ---- (". number_format($emi['premiumAfterDiscount'],2)." + ".number_format($emi['lessCorporateDiscount'])." /12)"; 
            if(isset($emi['benefitAmontOne'])){
              echo " + {$emi['benefitAmontOne']}";
              if(isset($emi['benefitAmontTwo']))
                echo " + {$emi['benefitAmontTwo']}";
            }
          }
          ?>
        </span>

      </p>
    </div>
    <?php } ?>

    <?php if($this->input->post() && $result && isset($installments)) {?>
    <br>
    <div class="grid_8">
     <h3>Emi Schedule: </h3><hr>

     <span class="no_print">
       <input type="submit" name="export" value="Export XL"/>
       <input type="submit" name="generate_pdf" value="Export PDF"/>
       <a href="" class="button" id="btn-print" onclick="return printDiv('print_div')" />Print</a>         
     </span>
     <br>

     <table>
       <thead>
         <tr>
           <th>S. No.</th>
           <th>Date</th>
           <th>Installment</th>
           <th>Premium</th>
           <th>Principal</th>
           <th>Interest</th>
         </tr>
       </thead>
       <tbody>
         <?php
         foreach ($installments as $installment) { $m=$installment['installment_number'];?>
         <tr <?php echo $m%12==0?'class="last_month"':'';?>>
           <td><?php echo $m;?></td>
           <td><?php echo $installment['due_date'];?></td>
           <td>
             <?php 
             echo number_format($installment['installment'],'2'); 
             ?>
           </td>
           <td><?php echo isset($emi['monthlyPreiumAmount'])?number_format($emi['monthlyPreiumAmount'],2):'N/A'; ?></td>
           <td><?php echo number_format($installment['principal'],2)?></td>
           <td>
             <?php 
             if($m%12==0){
              $principal=$installment['installment']-$installment['principal'];
              echo number_format($principal,2);
            }else{
              echo number_format($installment['interest_recovered'],2);
            }
            ?>
          </td>
        </tr>
        <?php } ?>         
      </tbody>
    </table>
  </div>
  <?php } ?>

</div>
</form>
</div>



<script>
  $(function(){

    $('#benefiet_2').click(function() {
      if ($(this).is(':checked')) {
        // console.log('checked');
        if($("#benefiet_1").is(':checked')===false){
          alert('Option 1(ADB) also to be mandatorily taken.');
          $("#benefiet_1").attr('checked',true);
        }
      } 
    });

    $('#benefiet_1').click(function() {
      if($("#benefiet_2").is(':checked') && $(this).is(':checked')===false){
        alert('Option 1 (ADB) also to be mandatorily taken.');
        $("#benefiet_1").attr('checked',true);
        return false;
      } 
    });

    // clear_fields();
    $('#clear').click(function(){
      clear_fields();
      return false;
    });
    function clear_fields(){
      $('#age,#term,#principal,#interest,#payment_mode,#start_date').val('');
      $('#payment_mode').val('');
      $('#benefiet_1,#benefiet_2').attr('checked',false);  
      $('#start_date').val('<?php echo date("Y-m-d")?>');    
    }

    // clear_fields();
    $('#clear_cbs').click(function(){
      clear_cbs_fields();
      return false;
    });
    function clear_cbs_fields(){
      $('#customer_username,#customer_password,#customer,#loan_disbursment_account,#principal_liquidation_account,#interest_liquidation_account').val('');
      $('#branch_code').val('NP0010002');      
    }

    //file_generator_parameters
    $('input[name="send_to_cbs"]').hover(function(e){
      $('span.file_generator_parameters input').each(function(){
        $(this).addClass('required');
      });
    })
    $('input[name="generate_file"]').hover(function(e){
      $('span.file_generator_parameters input').each(function(){
        $(this).addClass('required');
      });
    })
    //remove required class while just calculating emi
    $('input[name="calculate"]').hover(function(e){
      $('span.file_generator_parameters input').each(function(){
        $(this).removeClass('required');
      });
    })

    $('span.file_generator_parameters label').each(function(){
      $(this).append('<em class="required">*</em>');
    });
    
    
    var cbsSuccess='<?php echo (isset($cbsSuccess) && $cbsSuccess)?true:false?>';
    if(cbsSuccess=='true'){
      var msg='<?php echo (isset($cbsSuccess))?$cbsSuccessMsg:'';?>';
      alert(msg);
      cbsSuccess=false;
      cbsSuccessMsg=false;
    }

  });
</script>


