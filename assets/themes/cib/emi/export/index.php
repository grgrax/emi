   <h3>Emi Schedule: </h3><hr>
   <table>
     <thead>
       <tr>
         <th>S. No.</th>
         <th>Date</th>
         <th>Installment</th>
         <th>Premium</th>
         <th>Principal</th>
         <th>Interest Recovered</th>
       </tr>
     </thead>
     <tbody>
       <?php
       foreach ($installments as $installment) { $m=$installment['installment_number'];?>
       <tr <?php echo $m%12==0?'class="last_month"':'';?>>
         <td><?php echo $m;?></td>
         <td><?php echo $installment['due_date'];?></td>
         <td><?php echo number_format($installment['installment'],'2'); ?></td>
         <td><?php echo isset($emi['monthlyPreiumAmount'])?number_format($emi['monthlyPreiumAmount'],2):'N/A'; ?></td>
         <td><?php echo number_format($installment['principal'],2)?></td>
         <td>
           <?php 
           if($m%12==0){
            $principal=$installment['installment']-$installment['principal'];
            echo number_format($principal,2);
          }else{
            echo number_format($installment['interest_recovered'],2);
          }
          ?>
        </td>
      </tr>
      <?php } ?>         
    </tbody>
  </table>
