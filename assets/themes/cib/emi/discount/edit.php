<div class="grid_12">
  <form method="post" name="support" class="validForm">               
    <h2> Edit Discount : <?php echo $discount->getName();?></h2>
    <p class="element">
      <label>Payment Discount</label>
      <input type="text" name="payment_discount" class="payment_discount" 
      value="<?php echo set_value('payment_discount',$discount->getPaymentDiscount());?>"/> 
    </p>
    <p class="element">
      <label>Corporate Discount</label>
      <input type="text" name="corporate_discount" class="corporate_discount" 
      value="<?php echo set_value('corporate_discount',$discount->getCorporateDiscount());?>"/> 
    </p>
    <p class="element">
     <label>&nbsp;</label> 
     <input type="submit" name="submit" value="Submit"/>
     <a href="<?php echo Site_url('emi/discount')?>" class="button">Cancel</a>
   </p>
 </form>
</div>