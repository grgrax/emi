<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title> Sunrise Bank Limited </title>
  <link rel="icon" type="image/png" href="<?php echo theme_url().'images/favicon/favico_16.png'?>" />

  <!-- apply css for dialog window  -->
  <link rel="stylesheet" href="<?php echo theme_url() ?>css/ui/jquery-ui-1.8.21.custom.css">

  <link rel="stylesheet" href="<?php echo theme_url() ?>css/custom.css" >
  <link rel="stylesheet" href="<?php echo theme_url() ?>css/sunrise.css" >
  
  

  <!-- later added scripts -->


  <script>var base_url = "<?php echo base_url(); ?>";</script>
  
  <script src="<?php echo theme_url() ?>js/jquery.min.js"></script>
  <script src="<?php echo theme_url() ?>js/jqueryui/jquery-ui.min.js"></script> 
  <!-- older -->
  <script src="<?php echo theme_url() ?>js/jquery.validate.js"></script>
  <!-- custom -->
  <script src="<?php echo theme_url() ?>js/common.js"></script>

</head>

<body class="front">

  <div class="wrapper">

    <div class="header no_print">
      <div class="logo-header">
       <!--  <a href="<?php echo base_url()."public"?>"><img src="<?php echo theme_url()."images/new/"; ?>logo.png"></a> -->
     </div>
     <div class="login">

    </div>
  </div>

  <div class="nav no_print">
    <div class="menu">
      <ul>
       <li><a href="<?php echo base_url()."emi"?>">Home</a></li> 
       <li><a href="<?php echo base_url().'emi/rate'?>">Rate</a>
        <!--  <ul>
         <li><a href="<?php echo base_url().'emi/rate'?>">List Rate</a></li>
           <li><a href="<?php echo base_url().'emi/rate/import'?>">Import Rate</a></li>
         </ul> -->
       </li>
       <li><a href="<?php echo site_url('emi/discount')?>">Discount</a>
        <!--  <ul>
         <li><a href="<?php echo site_url('emi/discount')?>">List Discount</a></li>
           <li><a href="<?php echo site_url('emi/discount/add')?>">Add Discount</a></li>
         </ul> -->
       </li>
       <!-- <li><a href="<?php echo base_url().'emi/loan'?>">Loan Schedule</a></li> -->
     </ul>
   </div>

 </div>

 <div class="banner">
  <!-- <img src="<?php echo theme_url()."images/" ?>Dhaulagiri.jpg"> -->
</div>
<div class="main-content">
