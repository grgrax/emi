<?php use models\PrivilegeCard;
?>

<p class="element">
		<label>Card Number<em class="required">*</em>
		</label><input type="text" name="cardnumber"
			class="text ui-widget-content required">
	</p>
	
	<p class="element">
		<label>Created Date
		</label><input type="text" name="created_date"  
		class="text ui-widget-content birthdatepicker" >
	</p>
	
	<p class="element">
		<label>Issued Date<em class="required">*</em>
		</label><input type="text" name="issued_date"
		class="text ui-widget-content birthdatepicker" >
	</p>
	
	<p class="element">
		<label>Expiry Date<em class="required">*</em>
		</label><input type="text" name="expiry_date"
		class="text ui-widget-content birthdatepicker">
	</p>
	
	<p class="element">
		<label>Status</label>
		<select name="status" id="status" >
		       <option value="<?php echo  PrivilegeCard::CARD_ACTIVE; ?>">Active</option>
				<option value="<?php echo  PrivilegeCard::CARD_BLOCKED; ?>">Blocked</option>				
				<option value="<?php echo  PrivilegeCard::CARD_EXPIRED; ?>">Expired</option>
				<option value="<?php echo  PrivilegeCard::CARD_ISSUED; ?>">Issued</option>
				<option value="<?php echo  PrivilegeCard::CARD_NOT_IN_USE; ?>">Not In Use</option>
		</select>
		
		
	</p>

	<p class="element">
		<label>Issued By<em class="required">*</em>
		</label><input type="text" name="issued_by"
			class="text ui-widget-content required">
	</p>
	
   <?php $customer = \Current_user::user(); ?>
  
	<p class="element">
		<label for="customer">Customer</label>
		<input type="text" name="customer" class="text ui-widget-content customer"  value="<?php echo  $customer->getFirstname();?>" />
	</p>
	
	
	<?php $privilegeCardTypes = $this->doctrine->em->getRepository('models\PrivilegeCardType')->findAll();?>
	<p class="element">
	    <label for="privilege_card_type">Privilage Card Type</label> 
		<select name="privilege_card_type" id="privilege_card_type">
					<option value="">-- Select Card Type --</option>
					<?php foreach($privilegeCardTypes as $pct):
					echo '<option value="'.$pct->id().'">'.$pct->getName().'</option>';endforeach;?>
					</select>
	</p>
	<input type = "hidden" name="customer_id" value="<?php echo $customer->id();?>" />	
	<p class="element">
		<label>Country<em class="required">*</em>
		</label>
			<input type="text" name="country"
			class="text ui-widget-content required"  value="<?php echo $country;?>">
	</p>
	
	
