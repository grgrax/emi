jQuery.widget('f1soft.docmgmt',{
	options: {
		customer_id: null,
		remote: Transborder.config.base_url+'customer/ajax/getDocs/',
		saveURL: Transborder.config.base_url+'customer/ajax/addDoc/',
		updateURL: Transborder.config.base_url+'customer/ajax/updateDoc/',
		width: 600,
		height: 550,
		addDoc : true,
		editDoc : true,
		title: "Customer Documents"
	},
	
	_create: function(){
	},
	close: function(){
	  	 this.dialog.dialog('close');
	  	},
	
	_init: function(){
		this._createWrapper();
		var self = this;
		this.element.bind('click',function(e){
			e.preventDefault();
			self.dialog.dialog('open');
		});
	},
	
	
	
	open: function(){
		this.dialog.dialog('open');
	},
	
	
	
	_createWrapper: function(){
		this.wrapper = $('<div>')
			.addClass('f1-customer-doc-mgmt-widget-wrapper')
			.attr({
				title: this.options.title
			});
		
		this.listwrap = $("<div class='doc-list-wrapper'>")
			.appendTo(this.wrapper);
		
		if(this.options.addDoc === true)
		{
			this.addDocButton = $("<button>")
			.text("Add New Document")
			.addClass("search-button f1widget-button")
			.appendTo(this.wrapper)
			.button();
			
			this._prepareForm();
		}
	
		this.remoteResponse = $('<p>')
			.hide()
			.addClass('error')
			.appendTo(this.form);
		
		this._getDocs();
		
		this._initDialog();
	},
	
	_getDocs: function(){
		var self = this;
		var editDocument = this.options.editDoc === true ? 1 : 0;
		
		$.ajax({
			url: this.options.remote+this.options.customer_id+'/'+editDocument,
			headers: {'Accept' : "text/html"},
			success: function(html){
				self.listwrap.html(html);

						var editButton = self.listwrap.find('a.doc-edit-btn');
			  		editButton.click(function(e){
			  		e.preventDefault();
			  		self.addDocButton.hide();
			  		self._prepareDocumentUpdateForm($(this).data('document'));
			  	 });	
			}
		});
	},
	
	
	
	_prepareForm: function(){
		var self = this;
		this.form = $("<form class='doc-mgmt-add-new-frm' enctype='multipart/form-data'>")
			.appendTo(this.wrapper);
		
		this.form.load(Transborder.config.base_url+'customer/ajax/getDocumentEntryForm/'+this.options.customer_id,function(){
			self.form.find('.f1tb_c_add_state').remoteChained(self.form.find('.f1tb_c_add_country'), Transborder.config.base_url+'country/ajax/chainProcessor');
			self.form.find('.f1tb_c_add_city').remoteChained(self.form.find('.f1tb_c_add_state'), Transborder.config.base_url+'country/ajax/chainProcessor');
			
			self.form.find('.tbdatepicker.from').datepicker({
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
			    changeYear: true,
				minDate :"+0D"
			});
			
			self.form.find('.issudeDate').datepicker({
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
			    changeYear: true
			});
			
			self._createUploader();
			
			self.addButton = $("<button>")
				.text("Save")
				.addClass("search-button")
				.appendTo(self.form)
				.button(); 
			
			self.cancelButton = $("<button>")
				.text("Cancel")
				.addClass("search-button")
				.appendTo(self.form)
				.button(); 
				
			self.addButton.click(function(e){
				e.preventDefault();
				var valid = self.form.validate({errorElement: 'span'}).form();
				if(valid){

						self._saveDoc();	
				}
			});
			
			self.cancelButton.click(function(e){
				e.preventDefault();
				self.form.find('input[type="text"], input[type="hidden"], select').val('');
				self.wrapper.find('form.doc-mgmt-add-new-frm').slideUp();
			});
		});
		
		this.addDocButton.click(function(){
					self.wrapper.find('form.doc-mgmt-add-new-frm').slideDown();
						self.form.find('.qq-upload-list').html('');
		});
		//
		self.form.hide();
		//
	},
	
 _prepareDocumentUpdateForm: function(doc){
  	 var self = this;
  	this.wrapper.find('form.doc-mgmt-edit-frm').remove();
  	this.wrapper.find('form.doc-mgmt-add-new-frm').hide();
  	
  	this.form = $("<form class='doc-mgmt-edit-frm' enctype='multipart/form-data'>").appendTo(this.wrapper);
  	
  	 this.form.load(Transborder.config.base_url+'customer/ajax/getDocumentEditForm/'+doc, function(){
  	
  	  	 self.form.find('.f1tb_c_edit_city').remoteChained(self.form.find('.f1tb_c_edit_state'), Transborder.config.base_url+'country/ajax/chainProcessorEdit');
  	  	
  	  	 self.form.find('.tbdatepicker.from').datepicker({
  	  	 dateFormat: 'yy-mm-dd',
  	  	 changeMonth: true,
  	  	 changeYear: true,
  	  	 minDate :"+0D"
  	  	 });
  	  	
  	  	 self.form.find('.issudeDate').datepicker({
  	  	 dateFormat: 'yy-mm-dd',
  	  	 changeMonth: true,
  	  	 changeYear: true
  	  	 });
  	
  	 self._createUploader();
  	
  	 self.updateButton = $("<button>")
  	 .text("Update")
  	 .addClass("search-button")
  	 .appendTo(self.form)
  	 .button();
  	
  	 self.cancelButton = $("<button>")
  	 .text("Cancel")
  	 .addClass("search-button")
  	 .appendTo(self.form)
  	 .button();
  	
  	 self.updateButton.click(function(e){
  	 e.preventDefault();
  	 var valid = self.form.validate({errorElement: 'span'}).form();
  			if(valid){
  				self._updateDoc();
  				}
  			});
  
  				self.cancelButton.click(function(e){
  				e.preventDefault();
  				self.form.find('input[type="text"], input[type="hidden"], select').val('');
  				self.form.slideUp();
  				self.wrapper.find('form.doc-mgmt-edit-frm').remove();
  				self.addDocButton.show();
  	 });
  	
  	 });
  	
  	 this.form.slideDown();
  	
  	},
	
	_uploadDoc: function(){
		var self = this;
		
	},
	_saveDoc: function(){
		var self = this,
		_d = this.form.serialize();
		
		this.wrapper.mask('Adding document..');
		
		jQuery.post(this.options.saveURL+this.options.customer_id, _d,function(d){
			self._handleResponse(d);
		},'json');
	},
	
	_updateDoc: function(){
		var self = this;
		_d = this.form.serialize();
		
		this.wrapper.mask('Updating document..');
		
		jQuery.post(this.options.updateURL+this.options.customer_id, _d,function(d){
			self._handleResponse(d);
		},'json');
		
	},
	
	_updateDoc: function(){
	  	var self = this;
	  	 _d = this.form.serialize();
	  	
	  	 this.wrapper.mask('Updating document..');
	  	
	  	 jQuery.post(this.options.updateURL+this.options.customer_id, _d,function(d){
	  	 self._handleResponse(d);
	  	 },'json');
	  	
	  	 },
	
	_handleResponse: function(data){
		if(data.status == 'success'){

	 this.wrapper.unmask();
		  
		   this._cleanup();
		  
		   this.form.find('input[type="text"], input[type="hidden"], select').val('');
		  
		  this.wrapper.find('form').hide();
		  
		  this.wrapper.find('form.doc-mgmt-edit-frm, form.doc-mgmt-add-new-frm').remove();
		  
		   this._getDocs();
		   this._prepareForm();
		  
		   this._initDialog();
		  
		   this.addDocButton.show();
			
			
			
				/*
			
			this.listwrap.children('table').find('tr.doc-mgmt-none').remove();
			
			var file = this.form.find('#id-file').get(0).value;
			
			var link = ( file == "")? '&nbsp;' : '<a href="'+Transborder.config.base_url+'customer/downloadDocument/'+file+'" title="Download Document" class="action-icon"><span class="ui-icon ui-icon-arrowthickstop-1-s"></span></a>';
			
			var expField = this.form.find('input[name=remitter_id_expiry]'),
				_date = expField.datepicker( "getDate" );
			var expdate =  _date == null ? 
						'--' : 
						Transborder.utility.monthShort[_date.getMonth()]+' '+_date.getDate()+', '+_date.getFullYear();
						//expField.datepicker( "getDate" );
			
			var html = '<tr><td>'+
						this.form.find('select[name=remitter_id_type] option:selected').text()+'</td><td>'+
						this.form.find('input[name=remitter_id_number]').val()+'</td><td>'+
						this.form.find('select[name=issued_country_remitter] option:selected').text()+'</td><td>'+
						this.form.find('select[name=c_state] option:selected').text()+'</td><td>'+
						this.form.find('select[name=c_city] option:selected').text()+'</td>'+
						'<td>'+expdate+'</td>'+
						'<td>'+link+'</td></tr>';
			
			this.form.find('select[name=remitter_id_type] option:selected').remove();
			
			this.listwrap.children('table').append(html);
			this.wrapper.unmask();
			this.form.find('select, input').val('');
			this.form.slideUp();
//			this.options.onSave.call(self, data);
//			this.close();
*/
		}else{
			this.remoteResponse.html(data.description).show();
		}
	},
	
	_initDialog: function(){
		var self = this;
		this.dialog = this.wrapper.dialog({
			autoOpen: false,
		    height: "auto",
		    resizable: false,
		    width: self.options.width,
		    modal: true,
		    create: function(){
		    	$(this).css("maxHeight", self.options.height);
		    },
		    close: function(e, ui){
		    	self._cleanup();	
		    	if(self.options.addDoc === true)
		    		self.form.find('.qq-upload-list').html('');
		    }
		});
	},
	
	_createUploader: function (){
		var self = this;
		
    	var uploader = new qq.FileUploader({
	        element: self.form.find('#doc-mgmt-files').get(0),
	        action: Transborder.config.base_url+'customer/ajax/uploadDoc',
	        uploadButtonText:"Add a File",
	        multiple: false,
	        onComplete:function(id, fileName, res){
	            var input = self.form.find('#id-file').val(res.filename);

				var _item = $(uploader._listElement).find('li').eq(id);
				var _remover = $('<span class="qq-remove-file" title="Remove this file" />');

				_remover.data('input',input);
				_item.append(_remover);

				_remover.bind('click',function(){
					var _item = $(this).closest('li');
					var input = $(this).data('input');

					input.remove();
					_item.slideUp();
				});
	        }
	    });
	},
	
	_cleanup: function(){
		this.remoteResponse.html('').hide();
		this.wrapper.find('form.doc-mgmt-edit-frm').remove();
	}
});