jQuery.widget('f1soft.customeredit',{
	options: {
		customer_id: null,
		remote: Transborder.config.base_url+'remittance/ajax/getCustomerById/',
		saveURL: null,
		data: null,
		city: null,
		state: null,
		country: null,
		width: 500,
		height: 550,
		title: "Edit Customer",
		receiver: false,
		agent : null
	},
	
	_create: function(){
		
	},
	
	_init: function(){
		this._createWrapper();
		var self = this;
		this.element.bind('click',function(e){
			e.preventDefault();
			self.dialog.dialog('open');
		});
	},
	
	open: function()
	{
		this.dialog.dialog('open');
	},
	
	_createWrapper: function(){
		this.wrapper = $('<div>')
			.addClass('f1-edit-customer-widget-wrapper')
			.attr({
				title: this.options.title
			});
		
		this.remoteResponse = $('<p>')
			.hide()
			.addClass('error')
			.appendTo(this.wrapper);
		
		this.form = $("<form>")
			.appendTo(this.wrapper);
	
		
		
		this._getTemplate();
		
		this._initDialog();
	},
	
	_getDetails: function(){
		
	},
	
	_getTemplate: function(){
		var self = this,
		template = this.options.receiver ? 'getCustomerEditBeneficiary':'getCustomerEdit';
		
		this.form.load(Transborder.config.base_url+'remittance/ajax/'+template+'/'+this.options.customer_id+'/html/'+this.options.agent,function(){
			self._initDialog();
			
			if( self.options.receiver )
				self.form.find('.f1tb_c_edit_state').remoteChained(self.form.find('.f1tb_c_edit_country'), Transborder.config.base_url+'country/ajax/chainProcessorEdit');
		
			
			//
			self.form.find('.f1tb_c_edit_city').remoteChained(self.form.find('.f1tb_c_edit_state'), Transborder.config.base_url+'country/ajax/chainProcessorEdit');
			
			//self.find('.f1tb_c_edit_country').trigger('change');
			
			self.form.find('.tbdatepicker').datepicker({
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
			    changeYear: true,
			    yearRange: '1940:-18y'
			});
			
			self.addButton = $("<button>")
				.text("Save")
				.addClass("search-button")
				.appendTo(self.form)
				.button(); 
				
			self.addButton.click(function(e){
				e.preventDefault();
				var valid = self.form.validate({errorElement: 'span'}).form();
				if(valid){
					self._saveCustomer();	
				}
			});
		});
	},
	
	_saveCustomer: function(){
		var self = this,
		_d = this.form.serialize();
		
		this.wrapper.mask('Saving Customer..');
		var template = this.options.receiver ? 'getCustomerEditBeneficiary':'getCustomerEdit';
		jQuery.post(Transborder.config.base_url+'remittance/ajax/'+template+'/'+this.options.customer_id, _d,function(d){
			self._handleResponse(d);
		},'json');
	},
	
	_handleResponse: function(data){
		if(data.status == true){
			this.options.onSave.call(self, data);
			this.close();
		}else{
			this.remoteResponse.html(data.responseMessage).show();
			this.wrapper.unmask();
		}
	},
	
	_initDialog: function(){
		var self = this;
		this.dialog = this.wrapper.dialog({
			autoOpen: false,
		    height: "auto",
		    resizable: false,
		    width: self.options.width,
		    modal: true,
		    create: function(){
		    	$(this).css("maxHeight", self.options.height);
		    },
		    close: function(e, ui){
		    	self._cleanup();		    	
		    }
		});
	},
	
	close: function(){
		this.remoteResponse.html('').hide();
		this.wrapper.unmask();
		this.dialog.dialog('close');
	},
	
	_cleanup: function(){
//		this.form.find('input').val('');
	}
	
});