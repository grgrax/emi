/*
 * Response from remote should be in this structure
 * //		return {
//			numcolumns : 4,
//			columns: ['fname','lname','phone','email'],
//			data: [
//		        	{
//		        		fname: "Rajesh",
//		        		lname: "Sharma",
//		        		phone: "9801079789",
//		        		email: "broncha@rajesharma.com"
//		        	},
//		        	{
//		        		fname: "Broncha",
//		        		lname: "Sharma",
//		        		phone: "9801079888",
//		        		email: "bloncha@rajesharma.com"
//		        	},
//		        	{
//		        		fname: "David",
//		        		lname: "Harper",
//		        		phone: "9801079444",
//		        		email: "rajesh.sharma@f1soft.com"
//		        	},
//		        	{
//		        		fname: "Rajesh",
//		        		lname: "Hamal",
//		        		phone: "9801079666",
//		        		email: "broncha.rajesh@gmail.com"
//		        	}
//		        ]
//		};
 */
String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
};

jQuery.widget('f1soft.searchwidget',{
	
	options: {
		title: "Search Widget",
		msgConfirmUse: "Are you sure?",
		width: 600,
		height: 200,
		confirmSelection: false,
		preload:false,
		preloadData:{},
		fields: ['name'],
		onSelect: null,
		onAppendResultRow: null,
		noResultMessage: 'No results found',
		remote: '',
		inbuiltFilters:{
			_dest_country : "Country"
		}
	},
	
	_create: function(){
		
		this._createWrapper();
		
		var self = this;
		this.element.bind('click',function(e){
			e.preventDefault();
			self.dialogUI.dialog('open');
			
		});
	},
	
	_init: function(){
		//this.searchResult.hide();
	},
	
	_createWrapper: function(){
		var self = this;
		this.searchFormWrapper = $('<div>')
									.addClass('f1-searchwidget-wrapper')
									.attr({
										title: this.options.title
									});
		
		this.filterWrap = $("<div>")
								.appendTo(this.searchFormWrapper);
		
		this.searchForm = $("<form>")
								.appendTo(this.filterWrap);
		
		this.searchTable = $("<table class='filter-table'>")
								.appendTo(this.searchForm);
		
		var numRows = Math.ceil(this.options.fields.length/4),
			filterRowsHeader = [],
			filterRowsInput = [];
		
		for( var j = 0; j < numRows ; j++){
			var _rH = $("<tr>").appendTo(this.searchTable),
				_rI = $("<tr>").appendTo(this.searchTable);
			
			filterRowsHeader.push(_rH);
			filterRowsInput.push(_rI);
		}
		
		for(var i =0,row = 0; i< this.options.fields.length; i++){
			
			if(i > 0 &&  (i % 4) == 0){
				row += 1;
			}
			
			var label = this.options.fields[i].capitalize();
			
			if( this.options.inbuiltFilters.hasOwnProperty(this.options.fields[i])){
				label = this.options.inbuiltFilters[this.options.fields[i]];
				
			}
			
			var htd = $("<td class='filter-head'>")
						.text(label)
						.appendTo(filterRowsHeader[row]);
			
			var itd = $("<td>")
				.appendTo(filterRowsInput[row]);
			
			this._filterField(this.options.fields[i], itd);
			
		}
		
		this.searchButton = $("<button>")
								.text("Search")
								.addClass("search-button")
								.appendTo(this.searchFormWrapper)
								.button(); 
		
		this.searchFormWrapper.appendTo('body').hide();
		
		this.dialogUI = this.searchFormWrapper.dialog({
							autoOpen: false,
						    height: "auto",
						    resizable: false,
						    width: this.options.width,
						    modal: true,
						    create: function(){
						    	$(this).css("maxHeight", self.options.height);
						    },
						    open: function(){
						    	$(this).find('input[name="firstname"]').focus();
						    },
						    close: function(e, ui){
						    	//self._cleanup();
								self.searchForm.find('input').val('');
						    }
						});
		
		this.searchResult = $('<table>')
					.addClass('ui-widget ui-widget-content ui-searchwidget-result')
					.attr({
						width: '100%',
						'cellpadding': 0,
						'cellspacing': 0
					})
					.appendTo(self.searchFormWrapper)
					.hide();
		
		this.searchButton.click(function(e){
			self._cleanup();
			if( self.options.preload )
				self._getData(self.options.preloadData);
			else
				self._getData();
		});
	},
	
	reload: function(data){
		this._cleanup();
		this._getData(data);
	},
	_cleanup: function(){
		this.searchResult.html('').hide();
	},
	
	close: function(){
		this.dialogUI.dialog( "close" );
	},
	
	_setResult: function(data){
		var self = this;
		var head = '<thead><tr>';
		
		for(var i = 0; i < data.numcolumns; i++){
			head += '<th>'+data.columns[i].capitalize()+'</th>';
		}
		
		head += '</tr></thead>';
		
		this.searchResult.append(head);
		
		if(data.data.length == 0){
			this.searchResult.append('<td colspan="'+data.numcolumns+'">'+this.options.noResultMessage+'</td>');
		}
		
		$.each(data.data,function( j, d ){
			var _tr = $('<tr>'),
				_r = '';
			
			for(var i = 0; i < data.numcolumns; i++){
				var _c = data.columns[i];
				_r += '<td>'+d[_c]+'</td>';
			}
			_tr.append(_r);
			
			_tr.data('result_row', d);
			
			_tr.bind('click',function(e){
				if(self.options.confirmSelection){
					var really = confirm(self.options.msgConfirmUse);
					if( really ){
						var close = self.options.onSelect.call(self, jQuery(this).data('result_row'));
						if( close != false)
							self.close();
					}
				}else{
					var close = self.options.onSelect.call(self, jQuery(this).data('result_row'));
					if( close != false)
						self.close();
				}
			});
			
			self.searchResult.append(_tr);
			if( self.options.onAppendResultRow !== null )
				self.options.onAppendResultRow.call(self, _tr);
		});
		
		this.searchResult.show();
	},
	
	_getData: function(data){
		var self = this,
			_d = data ? data:this.searchForm.serialize();
		
		jQuery.post(this.options.remote, _d,function(d){
			self._setResult(d);
		},'json');
	},
	
	_filterField: function(field, holder){
		if(this.options.inbuiltFilters.hasOwnProperty(field)){
			switch(field){
				case '_dest_country':
					var sel = $('<select>')
								.attr({
									type: "text",
									name: field,
								})
								.appendTo(holder);
					sel.append('<option value="">Filter Country</option>');
					sel.append(Transborder.utility.buildSelectOption(Transborder.destinationCountries, function(item, option){
						if(option == 'value') return item.id;
						else return item.name;
					}));
				break;
			}
		}else{
			$('<input>')
				.attr({
					type: "text",
					name: field.split(' ').join('_'),
					'autocomplete':"off"
				})
				.addClass('text ui-widget-content')
				.appendTo(holder);
		}
		
	}
});