$(function(){
	$('input.required, textarea.required, select.required').prev('label').append('<em class="required">*</em>');
	$(".datepicker").datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: 'yy-mm-dd'
	});

	$(".hasDatepicker").datepicker({
		format : 'yy-mm-dd'
	});

	// $(".validForm").validate({
	// 	errorElement: "span"
	// });
	
	$('input:submit').on('click',function(){	
		
		//initialize validation
		$(this).closest('form').validate({
			errorElement: "span",
			errorPlacement: function(error, element) {
				if (element.attr("id") == "bankreceipt_payment_for") {
					error.insertAfter($(element).parent('p').find('span').last());
				} else {
					error.insertAfter(element);
				}
			},
			ignore: ":hidden",
			submitHandler: function(form) {

				//check for invalid characters (allows only keyboard strings)
				var checkInvalid = false;
				$(form).find("input, textarea, select").each(function(){
					if(!($(this).val().match(/^[\x20-\x7F\n\s]*$/)))
					{
						alert("Contains Invalid Characters!");
						checkInvalid = true;
					}
				});

				//return if invalid characers present
				if(checkInvalid){ return false; }

				//add disable submit button class
			    if($(form).find('input[type="submit"]').hasClass('submit-disabled'))
			    {
			    	return false;
			    }else{
			    	// $(form).find('input[type="submit"]').addClass('submit-disabled');
			    }

			    form.submit();
			}
		});
	});

	$('.btn-confirm').on('click',function(){
   	return confirm('Are you sure want to Continue ?');
	});

	//pdf validation for debtor
	$('input[name="pdf"]').on('change',function(){
//	$('#pdf_input').on('change',function(){
		var filename=$(this).val();
		// var re = /\..+$/;
		var re = /\.pdf$/;
		var ext = filename.match(re);
      if (ext=='.pdf') {
      } else {
        $(this).val('');
        alert("Invalid file type, please select another file");
        return false;
      }
	});

	$(".reset").click(function(){
		clear();
		return false;
	});

});

    function printDiv(divID) {
        var divElements = document.getElementById(divID).innerHTML;

        // var oldPage = document.body.innerHTML;
		  var oldPage = document;
		        
        // js script is not working after page get printed
        // document.body.innerHTML =  "<body>" +  divElements + "</body>";

        
        window.print();
        // document.body.innerHTML = oldPage;
        return false;
    }

	function clear(){
		$('input[type="text"], textarea').val("");
	}

	function checkFileUpload(file, maxSize, allowedType)
	{
		//check allowed file type
		var validFile = false;
		for (i = 0; i < allowedType.length; i++) {
		    if(file.type == allowedType[i])
		    {
		    	validFile = true;
		    }
		}
		if(!validFile){
			alert("Invalid file type. Please select another file.");
        	return false;
		}

		//check maxfile size
		if(file.size >= maxSize)
		{
			maxSize = maxSize/1024/1024;
			alert("Maximum file size allowed is "+ maxSize + "MB.");
			return false;
		}
		return true;		
	}

