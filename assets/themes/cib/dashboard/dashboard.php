<div class="grid_12">
<div class="section">
<h2>Welcome to <?php echo Current_User::user()->getAccType()->getDisplayName();?> Dashboard</h2>
			<div class="info">
				From this screen, you may go to the screens listed below. 
			</div>

			<div class="info-menu">
				<div class="section">
					<div class="info-icons">
						<a href="<?php echo base_url()."user/changepwd" ?>">
						<div class="grid_4">
							<ul>
								<div class="grid_3">
									<li class="info-icon-11" id="info-icons">

									</li>
								</div>	
								<div class="grid_9">
									<li class="info-text">
										
											<span>Change Password</span>
										</a>
									</li>
								</div>	
							</ul>
						</div>
						</a>

						<?php if(config_access()):?>
							<a href="<?php echo site_url('config');?>">
							<div class="grid_4">
								<ul>
									<div class="grid_3">
										<li class="info-icon-12" id="info-icons">

										</li>
									</div>	
									<div class="grid_9">
										<li class="info-text">
											
												<span>Configuration</span>
											</a>
										</li>
									</div>	
								</ul>
							</div>
							</a>
						<?php endif;?>
						<?php if(user_access('administer user')){ ?>
						<a href="<?php echo base_url()."user" ?>">
						<div class="grid_4">
							<ul>
								<div class="grid_3">
									<li class="info-icon-13" id="info-icons">

									</li>
								</div>	
								<div class="grid_9">
									<li class="info-text">
										
											<span>List (Clients / Gov / Reg Admin)</span>
										</a>
									</li>
								</div>	
							</ul>
						</div>
						</a>
						<?php } ?>

						<?php if(user_access('manage sub users')){ ?>
						<a href="<?php echo base_url()."user/SubUser" ?>">
							<div class="grid_4">
								<ul>
									<div class="grid_3">
										<li class="info-icon-14" id="info-icons">

										</li>
									</div>	
									<div class="grid_9">
										<li class="info-text">
											
												<span>Manage Sub Users </span>
											</a>
										</li>
									</div>	
								</ul>
							</div>
							</a>
						<?php } ?>
						<?php if(user_access('search for notices of security interest')){ ?>
							<a href="<?php echo base_url()."search" ?>">
							<div class="grid_4">
								<ul>
									<div class="grid_3">
										<li class="info-icon-3" id="info-icons">
											
										</li>
									</div>
									<div class="grid_9">
										<li class="info-text">
											
												<span>Search for notices</span>
											</a>
										</li>
									</div>	
								</ul>
							</div>
							</a>	
						<?php } ?>
	
						<?php if(user_access('regenerate certified search')){ ?>
						<a href="<?php echo base_url()."search/regenerateCertified/" ?>">
							<div class="grid_4">
								<ul>
									<div class="grid_3">
										<li class="info-icon-14" id="info-icons">

										</li>
									</div>	
									<div class="grid_9">
										<li class="info-text">
												<span>Regenerate Certified Search</span>
											</a>
										</li>
									</div>	
								</ul>
							</div>
							</a>
						<?php } ?>
						<?php if(user_access('list refund request')){?>
						<a href="<?php echo base_url()."refund" ?>">
						<div class="grid_4">
							<ul>
								<div class="grid_3">
									<li class="info-icon-19" id="info-icons">
										
									</li>
								</div>
								<div class="grid_9">
									<li class="info-text">
										
											<span>List Refund Request</span>
										</a>
									</li>
								</div>
							</ul>
						</div>
						</a>
						<?php } ?>
						<?php if(user_access('view report')){?>
						<a href="<?php echo base_url()."report/viewlist" ?>">
						<div class="grid_4">
							<ul>
								<div class="grid_3">
									<li class="info-icon-23" id="info-icons">
										
									</li>
								</div>
								<div class="grid_9">
									<li class="info-text">
										
											<span>List Reports</span>
										</a>
									</li>
								</div>
							</ul>
						</div>
						</a>
						<?php } ?>

						<?php if(user_access('view other statement')){ ?>
						<a href="<?php echo base_url()."account/viewOthersStatement";?>">
						<div class="grid_4">
							<ul>
								<div class="grid_3">
									<li class="info-icon-7"  id="info-icons">
										
									</li>
								</div>
								<div class="grid_9">
									<li class="info-text">										
											<span>View Others Statement</span>										
									</li>
								</div>
							</ul>
						</div>
						</a>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
</div>
