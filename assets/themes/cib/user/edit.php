<?php
use models\User;
use models\User\Group;
?>


<form class="validate" action="" method="post" name="addUser">
	<div class="grid_12">
		<h2>Edit User </h2>
		<div class="grid_6">
			<fieldset id="general">
				<legend>General</legend>
				<div class="section">
					
					<p class="element">
						<label> Account Name</label> <input type="text" name="acc_name" autocomplete="off"
						class="required" value="<?php echo $user->getAccName()?>" />
					</p>

					<p class="element">
						<label> Country </label> 
						<select name="country" id="country" class="required">
							<?php foreach(getOperatingCountries() as $k=>$v){ ?>
							<option value="<?php echo $k ?>" selected="selected"><?php echo $v ?></opption>
								<?php } ?>
							</select>
						</p>

						<p class="element">
							<label> District </label> 
							<select name="district" id="district" class="required">
								<?php foreach($cities as $k=>$v):?>
									<option value="<?php echo $k ?>" <?php echo $user->getCity()->id()==$k?"selected":"";?>>
										<?php echo $v;?>
									</option>
								<?php endforeach?>
							</select>
						</p>
						
						<p class="element">
							<label> VDC/MCP/MP </label> 
							<input name="local_gov" id="local_gov" class="required" placeholder="Enter your vdc/mcp/mp or village or town" value="<?php echo $user->getLocalGov() ?>">
							<label class="info">Enter your vdc/mcp/mp or village or town</label>
						</p>							

						
						<p class="element">
							<label> Address </label> 
							<textarea rows="5" cols="18" name="address" id="address" class="required"
							placeholder="Enter Your house number, tole name, ward number "><?php echo $user->getAddress();?></textarea>
							<label class="info">Enter Your house number, tole name, ward number, & vdc/mcp/mp</label>
						</p>
						
						<p class="element">
							<label> Account Type</label>
							<input type="text" name="acc_type" value="<?php echo $user->getAccType()->getDisplayName();?>"  readonly="readonly" />
						</p>

					</p>
					<p class="element">
						<label> Group </label> 
						<select name="group" class="required">
							<?php 
							$userGroup = $user->getGroup();
							$userGroupId = ($userGroup) ? $userGroup->id() : 0 ;

							?>
							<option value=""> --select one-- </option>
							<?php foreach($groups as $group):?>
								<?php  if($group['group_id'] == 1) continue; ?>
								<option value="<?php echo $group['group_id'] ?>" <?php echo $group['group_id'] == $userGroupId ? "selected":"";?>>
									<?php echo $group['name'];?>
								</option>
							<?php endforeach?>
						</select>
					</p>

					<?php /* ?>
						<?php 
						//get user bank branch 
						if(($user->getAccType()->getMain()=='Y') && $user->getBankBranch() !=''):
						?>
						<p class="element">
						<label> Bank Branch</label> : <b><?php echo ($user->getBankBranch()) ? $user->getBankBranch()->getName() : '' ?></b>
						</p>
						<?php elseif($user->getAccType()->getMain()!='Y'): ?>
						<p class="element">
							<label> Bank Branch</label> 
							<select name="bankbranch" >
							<option value="">--select one--</option>
							<?php foreach($branches as $branch){?>
								
							<?php $selected = ($branch->id() == $user->getBankBranch()->id()) ? 'selected="selected"' : '' ?>
							<option <?php echo $selected; ?>  value="<?php echo trim($branch->id()) ?>"><?php echo $branch->getName() ?></option>
							<?php } ?>
							</select>
						</p>
						<?php endif; ?>

						<?php */ ?>


						
					</div>
				</fieldset>
			</div>


			<div class="grid_6">

				<fieldset id="security_administrator">
					<legend>Security Administrator</legend>
					<div class="section">
						<p class="element">
							<label> Username</label>
							<input type="text" name="username" value="<?php echo $user->getUsername();?>" readonly/>
						</p>
						<p class="element">
							<label>Primary Name</label>
							<input type="text" name="first_name" value="<?php echo set_value('first_name',$user->getFirstname());?>" class="required"  />
						</p>
						
						<p class="element" <?php if($user->getMiddlename()=='') echo 'style="display: none;"';?>>
							<label>Second Name</label>
							<input type="text" name="middle_name" value="<?php echo $user->getMiddlename();?>" />
						</p>
						
						<p class="element">
							<label>Surname</label>
							<input type="text" name="last_name" value="<?php echo $user->getLastname();?>" class="required"  />
						</p>

						<p class="element">
							<label>Telephone/Mobile</label> <input type="text" name="phone" autocomplete="off"
							class="required" value="<?php echo $user->getPhone();?>" />
						</p>
						<p class="element">
							<label> Fax </label>
							<input type="text" name="fax" value="<?php echo $user->getFax();?>" class="required" />
						</p>						
						<p class="element">
							<label>Email</label>
							<input type="text" name="email" class="required" value="<?php echo $user->getEmail();?>" />
						</p>
						<p class="element">
							<label> New Password</label>
							<input type="password" name="newpwd" />
						</p>
						<p class="element">
							<label> Confirm  Password</label>
							<input type="password" name="conpwd" />
						</p>
						<p class="element">
							<label>Status</label>
							<select name="status" id="status">
								<?php foreach (\models\User::$status_types as $key => $value) { ?>
									<?php if($key == 0) continue; ?>
									<?php $selected = ($key == $user->getStatus()) ? 'selected="selected"' : '' ?>
									<option <?php echo $selected; ?> value="<?php echo $key ?>"><?php echo $value ?></option>
									<?php } ?>
								</select>
							</p>



						</div><!-- section ends -->
					</fieldset>	
				</div><!-- grid_8 ends -->


				<div class="clear"></div>
				<p class="element">
					<label>&nbsp;</label> 
					<input id="confirm-submit" type="submit" value="Submit" /> 
					<a href="<?php echo Site_url('user')?>" class="button">Cancel</a>
				</p>

			</div><!-- grid_12 ends -->
		</form>