<div class="grid_12">
	<h2>Edit Group Permissions [ <?php echo $group->getName();?> ]</h2>
	<div class="section">
		<form name="transborder_group_permissions" method="post" action="" id="permission_form">
			<table border="0px" cellspacing=1 cellpadding=1 >
				<tr>
					<th width="3%"></th>
					<th>Permission</th>
					<th>Description</th>
				</tr>

				<?php foreach($all_permissions as $module=>$permissions){
					if (!is_array($permissions)) continue;
					?>
					<tr><td colspan="3" style="font-weight:bolder;text-transform:uppercase;"><?php echo $module?> Permissions</td></tr>
					<?php 					
					foreach ($permissions as $name=>$descr){
						$depends = '';
						if (!isset($db_permissions[$name])) continue;				
						?>
						<tr>
							<td>
								<?php 
								$chk = (in_array($db_permissions[$name], $group_permissions)) ? " checked" : "";
								if(is_array($descr)){
									foreach ($descr['depends_on'] as $val){
										$depends .= str_replace(' ', '-', $val)." ";
									}
								}
								else {
									$depends = '';
								}
								?>
								<input id="<?php echo str_replace(' ', '-', $name); ?>" 
								depends="<?php echo $depends; ?>"
								type="checkbox" value="<?php echo $db_permissions[$name]?>" 
								name="permission[<?php echo $db_permissions[$name]?>]"
								<?php echo $chk;?> 
								class="checkIt <?php if ($chk != '') echo 'default'?> " />
							</td>
							<td><?php echo ucwords($name);//.'--'.$db_permissions[$name]; ?></td>
							<td><?php echo (is_array($descr)) ? $descr['description'] :  $descr; ?></td>

						</tr>

						<?php } ?>

						<tr><td colspan="3" align="center" style="font-weight:bolder">&nbsp;</td></tr>

						<?php } ?>

					</table>
					<div class="tablefooter">
						<div class="actions">
							<input type="submit" class="button small save-perms" value="Save Permissions" name="save_perms"/>
							<input type="reset" class="button small" value="Reset" />
							<a href="<?php echo site_url('user/group')?>" class="button">Cancel</a>
						</div>

						<div class="clear"></div>
					</div>
				</form>
			</div>
		</div>

		<script>

			$(document).ready(function(e){

				if ($('.checkIt:checked').length == $('.checkIt').length) $('#select_all').attr('checked',true);
				$('#select_all').bind('click',function(){
					$('.checkIt').attr('checked', $(this).attr('checked'));
					var title = ($(this).attr('checked')) ? 'deselect all' : 'select all';
					$(this).attr('title', title);

				});
				$('.checkIt').click(function(){ if(!$(this).attr('checked')) $('#select_all').attr('checked',false); });
				$('.save-perms').click(function() {
					if ($('.checkIt:checked').length < 1) {
						alert('Please set atleast one permission.');
						return false;
					}
				});

				$(".checkIt").click(function(e){
					var depends_on = $(this).attr('depends');
					var splitted_depends = depends_on.split(" "); 
					var checked = $(this).attr('checked');

					if(checked){
						if(depends_on){
							$.each(splitted_depends, function(i, val){
								$("#"+val).attr('checked','checked')
							});
						}

					}
					else{
						if(depends_on){
							$.each(splitted_depends, function(i, val){
								$("#"+val).removeAttr('checked','checked')
							});
						}
					}

				});

			});
</script>