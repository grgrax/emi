<div class="grid_12">
	<h2>Sub Groups and Permissions</h2>
	
	<div class="section">
		<table border=0 cellspacing=1 cellpadding=1>
        			<tr>
        				<th class="serial" width="3%">SN.</th>
        				<th>Group Name</th>
        				<th>Description</th>
        				<th>Number of users</th>
        				<th width="15%">Actions</th>
        			</tr>
        			<?php 
        				if(count($groups) > 0):
        					$count = 1;
        					foreach($groups as $g):
        			?>
        				<tr>
        				<td><?php echo $count++;?></td>
        				<td><?php echo $g['name'];?></td>
        				<td><?php echo $g['description'];?></td>
        				<td><?php // echo $g['numusers'] 
        					 echo (isset($numusers[$g['group_id']])) ? $numusers[$g['group_id']] : 0 ?></td>
        				<td class="actions">
        					<?php 
        						if($g['group_id'] != 1):
	        						echo action_button('permissions',"user/group/permissions/".$g['group_id'],array('title'	=>	'Edit Permissions'));
	        						echo action_button('copy','#',array('title'	=>	'Copy to create a new Group','class'	=>	'clone-group', 'id'	=>	'gid-'.$g['group_id']));
	        						echo action_button('edit',"user/group/edit/".$g['group_id'],array('title'	=>	'Edit Group'));
	        					endif;
        					?>
        				</td>
        			</tr>
        				<?php 
        					endforeach;
        				endif;
        			?>
        			
        		</table>
        	</div>
</div>
<script type="text/javascript">
$(function(){
	$('.clone-group').click(function(e){
		e.preventDefault();
		var _id = $(this).attr('id').split('-'),
			gid = _id[1];
		
		$('.container').mask("Please wait while we copy the group.");

		$.ajax({
			type	:'GET',
			url		:'<?php echo base_url().'user/group/copygroup/'?>'+gid,
			data	:null,
			success	:function(res){
					res = $.parseJSON(res);
					$('.container').unmask();
					if(res.response == 'success'){
						window.location = '<?php echo base_url().'user/group/edit/'?>'+res.group_id;
					}else{
						alert('An error occurred while copying the group. Please try again.');
					}
				},
			failure	:function(){
					
				},
			
		});
		
		
	});
});
</script>