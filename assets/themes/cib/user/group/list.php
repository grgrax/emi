	<div class="grid_12">
		<h2>Groups and Permissions</h2>

		<div class="controls">
			<a href="<?php echo site_url('user/group/add')?>" class="button" >Add a Group</a>
		</div>

		<div class="section">
			<table border=0 cellspacing=1 cellpadding=1>
				<tr>
					<th class="serial" width="3%">SN.</th>
					<th>Group Name</th>
					<th>Description</th>
					<th>Number of users</th>
					<th>Number of sub groups</th>
					<th width="15%">Actions</th>
				</tr>
				<?php 
				if(count($groups) > 0):
					$count = 1;
				foreach($groups as $g):
					$group = $this->doctrine->em->getRepository('models\User\Group')->find($g['group_id']);

				?>
				<tr>
					<td><?php echo $count++;?></td>
					<td><?php echo $g['name'];?></td>
					<td><?php echo $g['description'];?></td>
					<td><?php echo $group->getUsers()->count();  ?></td>
					<td><?php echo $group->getSubGroups()->count();  ?> </td>	 

					<td class="actions">
						<?php 
						if($g['group_id'] != 1){
							$gid=$g['group_id'];
							echo action_button('permissions',"user/group/permissions/$gid",array('title'	=>	'Edit Permissions'));
							echo action_button('copy','#',array('title'	=>	'Copy to create a new Group','class'	=>	'clone-group', 'id'	=>	'gid-'.$gid));
							echo action_button('edit',"user/group/edit/$gid",array('title'	=>	'Edit Group'));
							echo action_button('add',"user/group/add/$gid",array('title'=>'add sub group'));
							echo action_button('view',"user/group/view_sub_groups/$gid",array('title'=>'view sub group'));	        					
						}
						?>
					</td>
				</tr>
				<?php 
				endforeach;
				endif;
				?>

			</table>
		</div>
	</div>
	<script type="text/javascript">
		$(function(){
			$('.clone-group').click(function(e){
				e.preventDefault();
				var _id = $(this).attr('id').split('-'),
				gid = _id[1];

				$('.container').mask("Please wait while we copy the group.");

				$.ajax({
					type	:'GET',
					url		:'<?php echo base_url().'user/group/copygroup/'?>'+gid,
					data	:null,
					success	:function(res){
						res = $.parseJSON(res);
						$('.container').unmask();
						if(res.response == 'success'){
							window.location = '<?php echo base_url().'user/group/edit/'?>'+res.group_id;
						}else{
							alert('An error occurred while copying the group. Please try again.');
						}
					},
					failure	:function(){

					},

				});


			});
		});
	</script>