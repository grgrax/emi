<div class="grid_12">
<h2>Edit Profile ( <?php echo $user->getUsername();?> ) </h2>
<form class="validate" action="" method="post" name="addUser">

<fieldset id="general">
<legend>General Details</legend>
<div class="section">
<p class="element">
<label>Primary Name</label>
<input type="text" name="first_name" value="<?php echo $user->getFirstname();?>" class="required" />
</p>

<p class="element">
<label> Second Name</label>
<input type="text" name="middle_name" value="<?php echo $user->getMiddlename();?>" />
</p>

<p class="element">
<label>Surname</label>
<input type="text" name="last_name" value="<?php echo $user->getLastname();?>" class="required" />
</p>


<p class="element">
<label> VDC/MCP/MP </label> 
<select name="local_gov" id="local_gov" class="required">
<option value="" >--- Select --- </option>
<?php foreach(\models\User::$VDC_MCP_MP as $k=>$v){ ?>
<option value="<?php echo $k ?>" <?php echo $user->getLocalGov()==$k?"selected":"";?>>
<?php echo $v ?>
</opption>
<?php } ?>
</select>
</p>

<p class="element">
<label> Address </label> 
<textarea rows="5" cols="18" name="address" id="address" class="required"
placeholder="Enter Your house number, tole name, ward number "><?php echo $user->getAddress();?></textarea>
<span style="float:right; text-align:justify">Enter Your house number,<br/> tole name, ward number, <br/> & vdc/mcp/mp</span>
</p>

<p class="element">
<label>Email</label>
<input type="text" name="email" class="required" value="<?php echo $user->getEmail();?>" />
</p>
<p class="element">
<label>Phone / Mobile</label>
<input type="text" name="phone" value="<?php echo $user->getPhone();?>" class="required"/>
</p>

<p class="element">
<label>Fax</label>
<input type="text" name="fax" class="required"  value="<?php echo $user->getFax();?>" />
</p>
<p class="element">
<label> Account Type</label>
<input type="text" name="acc_type" value="<?php echo $user->getAccType()->getDisplayName();?>"  readonly="readonly" />
</p>
<p class="element">
<label> Group </label>
<input type="text" name="group" value="<?php echo $user->getGroup()->getName();?>"  readonly="readonly" />
</p>

<p class="element">
<label> Username</label>
<input type="text" name="username" value="<?php echo $user->getUsername();?>"  readonly="readonly" />
</p>



<div class="clear"></div>

<p class="element">
<label>&nbsp;</label> 
<input type="submit" value="Update" /> 
<a href="<?php echo Site_url()?>" class="button">Cancel</a>
</p>
</div>

</div>

</form>
