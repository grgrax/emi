<div class="grid_12">
        <h2>Change password</h2>
        <fieldset>
                <form class="validate" action="" method="post">
                        <p class="element">
                                <label>Old Password</label>
                                <input type="password" name="oldpwd" class="required" />
                        </p>
                        <p class="element">
                                <label> New Password</label>
                                <input type="password" name="newpwd" class="required" />
                        </p>
                        <p class="element">
                                <label> Confirm  Password</label>
                                <input type="password" name="conpwd" class="required" />
                        </p>
                        <p>
                                <label>&nbsp;</label>
                                <input type="submit" value="Change" />
                                <a href="<?php echo site_url('dashboard')?>" class="button">Cancel</a>
                        </p>
                </form> 
        </fieldset>
</div>
        