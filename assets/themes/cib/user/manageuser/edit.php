<?php 
use models\User;
use models\User\Group;

?>
<div class="grid_12">
	<form class="validate" action="" method="post" name="addUser">
		<h2>Edit a Sub User</h2>
		<fieldset id="general-details">
			<legend>General Details</legend>
			
			<p class="element">
				<label>Group</label> 
				<select name="sub_group" class="required">
					<?php foreach (getSubGroups($user) as $sg) { ?>
						<option <?php if($sg->id() == $user->getGroup()->id()) echo 'selected' ?> value="<?php echo $sg->id() ?>"> <?php echo $sg->getName() ?> </option>
						<?php } ?>
					</select>
				</p>
				<p class="element">
					<label>Primary Name</label> <input type="text" name="first_name" autocomplete="off"
					class="required" value="<?php echo $user->getFirstname();?>" />
				</p>
				<p class="element">
					<label>Second Name</label> <input type="text" name="middle_name" autocomplete="off"
					class="" value="<?php echo $user->getMiddlename();?>"/>
				</p>
				<p class="element">
					<label>Surname</label> <input type="text" name="last_name" autocomplete="off"
					class="required" value="<?php echo $user->getLastname();?>" />
				</p>
				<p class="element">
					<label>Telephone/Mobile</label> <input type="text" name="phone" autocomplete="off"
					class="required" value="<?php echo $user->getPhone();?>" />
				</p>


				<p class="element">
					<label> Fax </label>
<!-- 					<input type="text" name="fax" value="<?php echo isset($_POST)?set_value('fax'):$user->getFax();?>" class="required" />
					 -->					
					 <input type="text" name="fax" value="<?php echo $user->getFax();?>" class="required" />
				</p>
				<p class="element">
					<label>Email</label> <input type="email" name="email" autocomplete="off"
					class="required"  value="<?php echo $user->getEmail();?>" />
				</p>
				<?php /* ?>
					<p class="element">
					<label> VDC/MCP/MP </label> 
					<select name="local_gov" id="local_gov" class="required">
						<option value="" >--- Select --- </option>
						<?php foreach(\models\User::$VDC_MCP_MP as $k=>$v){ ?>
							<option value="<?php echo $k ?>" <?php echo $user->getLocalGov()==$k?"selected":"";?>>
								<?php echo $v ?>
							</opption>
							<?php } ?>
						</select>
					</p>
					<p class="element">
						<label> Address </label> 
						<textarea rows="5" cols="20" name="address" id="address" class="required"
						placeholder="Enter Your house number, tole name, ward number & vdc/mcp/mp"><?php echo $user->getAddress();?></textarea>
						<span style="float:right;">Enter Your house number,<br/> tole name, ward number, <br/> & vdc/mcp/mp</span>
					</p>
					<?php */ ?>					
					<p class="element">
						<label>Login User ID</label> <input type="text" name="username" autocomplete="off"
						class="required"  value="<?php echo $user->getUsername();?>" readonly/>
					</p>
					<p class="element">
						<label> New Password</label>
						<input type="password" name="newpwd" class="" />
					</p>
					<p class="element">
						<label> Confirm  Password</label>
						<input type="password" name="conpwd" class="" />
					</p>
					<p class="element">
						<label>Status</label>
						<select name="status" id="status">
							<option value="">--select one--</option>
							<?php foreach (\models\User::$status_types as $key => $value) { ?>
								<?php if($key == 0) continue; ?>
								<?php $selected = ($key == $user->getStatus()) ? 'selected="selected"' : '' ?>
								<option <?php echo $selected; ?> value="<?php echo $key ?>"><?php echo $value ?></option>
								<?php } ?>
							</select>
						</p>
						<div class="clear"></div>
						<p class="element">
							<label>&nbsp;</label> <input type="submit" value="Update" />
							<a href="<?php echo Site_url('user/SubUser')?>" class="button">Cancel</a>
						</p>
					</fieldset>
				</form>
			</div>
