<?php 
use models\User;
use models\User\Group;
?>

<div class="grid_12">
	<h2>Create a Sub User</h2>
	<form class="validate" action="" method="post" name="addUser">
		<fieldset id="general-details">
			<legend>General Details</legend>
			<p class="element">
				<label>Group</label> 


				<select name="sub_group" class="required">
					<?php foreach ($subGroups as $sg) { ?>
						<option value="<?php echo $sg->id() ?>"> <?php echo $sg->getName() ?> </option>
						<?php } ?>
					</select>
				</p>


				<p class="element">
					<label>Primary Name</label> <input type="text" name="first_name" autocomplete="off"
					class="required" value="<?php echo set_value('first_name');?>" />
				</p>
				<p class="element">
					<label>Second Name</label> <input type="text" name="middle_name" autocomplete="off"
					class="" value="<?php echo set_value('middle_name');?>" />
				</p>
				<p class="element">
					<label>Surname</label> <input type="text" name="last_name" autocomplete="off"
					class="required" value="<?php echo set_value('last_name');?>" />
				</p>
				<?php /* ?>
				<p class="element">
					<label> Country </label> 
					<select name="country" id="country" class="required">
						<?php foreach(getOperatingCountries() as $k=>$v){ ?>
							<option value="<?php echo $k ?>"><?php echo $v ?></opption>
						<?php } ?>
					</select>
				</p>
				<p class="element">
						<label> District </label> 
						<select name="district" id="district" class="required">
						</select>
				</p>
				<p class="element">
						<label> VDC/MCP/MP </label> 
						<select name="local_gov" id="local_gov" class="required">
							<option value="" >--- Select --- </option>
							<?php foreach(\models\User::$VDC_MCP_MP as $k=>$v){ ?>
								<option value="<?php echo $k ?>">
									<?php echo $v ?>
								</opption>
								<?php } ?>
							</select>
				</p>
				<p class="element">
					<label> Address </label> 
					<textarea rows="5" name="address" id="address" class="required"><?php echo set_value('address');?></textarea>
				</p>
				<?php */ ?>
				<p class="element">
					<label>Telephone/Mobile</label> <input type="text" name="phone" autocomplete="off"
					class="required" value="<?php echo set_value('phone');?>" />
				</p>
				<p class="element">
					<label>Fax</label> <input type="text" name="fax" autocomplete="off"
					class="required" value="<?php echo set_value('fax');?>" />
				</p>

				<p class="element">
					<label>Email</label> <input type="email" name="email" autocomplete="off"
					class="required" value="<?php echo set_value('email');?>" />
				</p>
				<p class="element">
					<label>Login User ID</label> <input type="text" name="username" autocomplete="off"
					class="required" value="<?php echo set_value('username');?>" />
				</p>
				<p class="element">
					<label>Password</label> <input type="password" name="password" autocomplete="off"
					class="required" value="" />
				</p>
				<p class="element">
					<label>Confirm Password</label> <input type="password" name="confirmpassword" autocomplete="off"
					class="required" value="" />
				</p>
			</fieldset>
			<div class="clear"></div>
			<p class="element">
				<label>&nbsp;</label> <input type="submit" value="Create" />
				<a href="<?php echo Site_url('user/SubUser')?>" class="button">Cancel</a>
			</p>
		</form>
	</div>

	<script type="text/javascript">
		$(function() {
			var country = $("#country").val();

			if(country) doActionCountryChanged(country);

			$("country").change(function(){

				var country = $(this).val();

				doActionCountryChanged(country);

			});

			function doActionCountryChanged(country){
				var country = country;
				var selected_city = "<?php echo ($this->input->post('district'))?:'' ?>";
				$.ajax({
					type : 'GET',
					url : Transborder.config.base_url+'country/ajax/getCitiesByCountry/'+country+"/"+selected_city,
					success : function(data){ $('#district').html(data); }
				});
			}
		});
	</script>
