<?php
use models\User;
use models\User\Group;
?>
<form class="validate" action="" method="post" name="addUser">
	<div class="grid_12">
		<h2>View User's Details </h2>
		<div class="grid_6">
			<fieldset id="general">
				<legend>General</legend>
				<div class="section">
					<p class="element">
						<label> Account Name</label> 
						<?php echo $user->getAccName()?>
					</p>
					<p class="element">
						<label> Country </label> 
						<?php echo $user->getCountry()->getName()?>
					</p>
					<p class="element">
						<label> District </label> 
						<?php echo $user->getCity()->getName()?>
					</p>
					<p class="element">
						<label> VDC/MCP/MP </label> 
						<?php echo $user->getLocalGov()?>
					</p>							
					<p class="element">
						<label> Address </label> 
						<?php echo $user->getAddress();?>
					</p>
					<p class="element">
						<label> Account Type</label>
						<?php echo $user->getAccType()->getDisplayName();?>
					</p>
				</p>
				<p class="element">
					<label> Group </label> 
					<?php echo $user->getGroup()->getName()?>
				</p>
			</fieldset>
		</div>


		<div class="grid_6">
			<fieldset id="security_administrator">
				<legend>Security Administrator</legend>
				<div class="section">
					<p class="element">
						<label> Username</label>
						<?php echo $user->getUsername();?>
					</p>
					<p class="element">
						<label>Primary Name</label>
						<?php echo set_value('first_name',$user->getFirstname());?>
					</p>
					<p class="element" <?php if($user->getMiddlename()=='') echo 'style="display: none;"';?>>
						<label>Second Name</label>
						<?php echo $user->getMiddlename();?>
					</p>
					<p class="element">
						<label>Surname</label>
						<?php echo $user->getLastname();?>
					</p>
					<p class="element">
						<label>Telephone/Mobile</label> 
						<?php echo $user->getPhone();?>
					</p>
					<p class="element">
						<label> Fax </label>
						<?php echo $user->getFax();?>
					</p>						
					<p class="element">
						<label>Email</label>
						<?php echo $user->getEmail();?>
					</p>
					<p class="element">
						<label>Status</label>
						<?php echo User::$status_types[$user->getStatus()]?>
					</p>
				</div><!-- section ends -->
			</fieldset>	
		</div><!-- grid_8 ends -->


		<div class="clear"></div>
		<p class="element">
			<label>&nbsp;</label>
			<?php
			if($user->getStatus()==User::STATUS_PENDING){
				if(user_access('approve user'))
					echo anchor('user/approve/'.$user->getUsername(), 'Activate', 'class="button"');
				if(user_access('block user'))
					echo anchor('user/block/'.$user->getUsername(), 'Block', 'class="button"');
			}
			?>	
			<a href="<?php echo Site_url('user')?>" class="button">Return To User's List</a>
		</p>

	</div><!-- grid_12 ends -->
</form>