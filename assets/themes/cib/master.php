<!DOCTYPE html>
<html>
<?php 
	echo get_header();
	echo tb_getBrandBar();
	echo tb_getMainNav();
?>
<div class="maincontainer">
			<!-- BREAD CRUMB START -->
			<div class="grid_12">
            <?php if( \Current_User::user() or \Current_User::nonClient()){ ?>      
				<div class="breadcrumb">
		        	<?php echo $this->breadcrumb->output();?>
		        </div>
		      <?php } ?>  
		    </div>
		    <div class="clear"></div>
			<!-- BREAD CRUMB END -->
			
			<!-- MODULE SECTION START -->
			<div class="main">
				
				<?php $this->load->theme('common/cib/notifications') ?>

				<?php 
					if(isset($maincontent)){
						$this->load->theme($maincontent);
					}
				?>
<?php 
echo get_footer();
?>    
</html>    
<?php if(ENVIRONMENT == 'development') { ?>
		<?php $this->output->enable_profiler(TRUE); ?>

<?php } ?>

	<style type="text/css">
		#codeigniter_profiler{
			margin-top:100px; 
		}
	</style>
