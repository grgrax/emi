<style>
	.ui-timepicker-div .ui-widget-header { margin-bottom: 8px; }
	.ui-timepicker-div dl { text-align: left; }
	.ui-timepicker-div dl dt { height: 25px; margin-bottom: -25px; }
	.ui-timepicker-div dl dd { margin: 0 10px 10px 65px; }
	.ui-timepicker-div td { font-size: 90%; }
	.ui-tpicker-grid-label { background: none; border: none; margin: 0; padding: 0; }

	label.right {float: none; display: inline;}

</style>

<?php loadJS(array('datetimepicker',))?>

<div class="grid_12">
	<h2>CIBN Settings</h2>
	<?php if (user_access_or(
		array(
			'general setting',
			'transaction setting',
			'user setting',
			'fees and notice settings',
			)
			)):?>
			<div class="section tabs">

				<ul>
					<?php if(user_access('fees and notice settings')){ ?> <li><a href="#fees-config">Fees and notice settings</a></li><?php } ?>
					<?php if (user_access('general setting')) { ?><li><a href="#general-config">General</a></li><?php } ?>
					<?php if (Current_User::isSuperUser()) { ?> <li><a href="#maintenance-config">Site Maintenance</a></li> <?php } ?>
						<?php if (Current_User::isSuperUser()) { ?> <li><a href="#email-template-config">Email Alerts</a></li> <?php } ?>
						</ul>
						<?php if(user_access('fees and notice settings')) { ?>
						<div class="grid_10" id="fees-config">
							<form action="" class="" method="post" id="fees-config-form">
								<fieldset>
									<legend>Notice Settings</legend>
									<div class="section">

										<p class="element">
											<label for="notice_continuation_duration">Duration For Continuation of an Notice </label>
											<input type="text" name="notice_continuation_duration" id="notice_continuation_duration" value="<?php echo Options::get('notice_continuation_duration','');?>" class="required" />
											Months
										</p>
									<?php /* no need  ?>
									<p class="element">
										<label for="notice_expiry_duration">Extend Expiry Date By</label>
										<input type="text" name="notice_expiry_year" id="notice_expiry_year" value="<?php echo Options::get('notice_expiry_year','');?>" class="required" />
										Years
										<input type="text" name="notice_expiry_month" id="notice_expiry_month" value="<?php echo Options::get('notice_expiry_month','');?>" class="required" />
										Months
									</p>
									<?php */ ?>
									<p class="element">
										<label for="Cutoff Date">Cutoff date </label>
										<input type="text" name="cutoff_date" id="cutoff_date" class="hasDatepicker datepicker" value="<?php echo Options::get('cutoff_date','');?>" class="required" />
									</p>

									<p class="element">
										<label for="Lapse Date">Lapse Duration </label>
										<input type="text" name="lapse_duration" id="lapse_date" class="hasDatepicker datepicker" value="<?php echo Options::get('lapse_duration','');?>" class="required" /> years
									</p>
								</div>

							</fieldset>

							<fieldset>
								<legend>Notice Charges</legend>
								<div class="section">
									<p class="element">
										<label for="min_amount_for_certified_search">Minimum amount for certified search (Nrs)</label>
										<input type="text" name="min_amount_for_certified_search" id="min_amount_for_certified_search" value="<?php echo Options::get('min_amount_for_certified_search','');?>" class="" />
					<!-- 	<input type="checkbox" name="apply_certified_search_charge" <?php echo \Options::get("apply_certified_search_charge") == 1 ? 'checked="checked"':'' ?>>
						is Applicable
					-->		</p>
					<p class="element">
						<label for="min_amount_for_registration">Minimum amount for registration (Nrs)</label>
						<input type="text" name="min_amount_for_registration" id="min_amount_for_registration" value="<?php echo Options::get('min_amount_for_registration','');?>" class="" />
						<!-- <input type="checkbox" name="apply_registration_charge" <?php echo \Options::get("apply_registration_charge") ==1 ? 'checked="checked"':'' ?>>
						is Applicable 
					--></p>

					<p class="element">
						<label for="amend_charge">Amend Charge</label>
						<input type="text" name="amend_charge" id="amend_charge" value="<?php echo Options::get('amend_charge');?>" class="required" />
			<!-- 			<input type="checkbox" name="apply_amend_charge" 
						<?php echo Options::get('apply_amend_charge')==1?'checked':'';?> class="charge" />
						Is Applicable 
					-->		</p>
					<p class="element">
						<label for="continuation_charge">Continuation Charge</label>
						<input type="text" name="continuation_charge" id="continuation_charge" value="<?php echo Options::get('continuation_charge');?>" class="required" />
			<!-- 			<input type="checkbox" name="apply_continuation_charge" 
						<?php echo Options::get('apply_continuation_charge')==1?'checked':'';?> class="charge" />
						Is Applicable 
					-->		</p>
					<p class="element">
						<label for="correction_charge">Correction Charge</label>
						<input type="text" name="correction_charge" id="correction_charge" value="<?php echo Options::get('correction_charge');?>" class="required" />
			<!-- 			<input type="checkbox" name="apply_correction_charge" 
						<?php echo Options::get('apply_correction_charge')==1?'checked':'';?> class="charge" />
						Is Applicable 
					-->		</p>
					<p class="element">
						<label for="termination_charge">Termination Charge</label>
						<input type="text" name="termination_charge" id="termination_charge" value="<?php echo Options::get('termination_charge');?>" class="required" />
			<!-- 			<input type="checkbox" name="apply_termination_charge"
						<?php echo Options::get('apply_termination_charge')==1?'checked':'';?> class="charge" />
						Is Applicable 
					-->		</p>

					
				</div>
			</fieldset>
			<fieldset>
				<legend>Configure Pdf for Debtors</legend>
				<div class="section">
					<p class="element grid_12">
					<label for="config_operating_countries" >PDF of Citizenship Card</label>
						<input type="radio" name="citizenship_pdf" value="0"  <?php echo (Options::get('citizenship_pdf','0')=='0') ? 'checked="checked"':''?>/> Hidden
						<input type="radio" name="citizenship_pdf" value="1"  <?php echo (Options::get('citizenship_pdf','1')=='1') ? 'checked="checked"':''?>/>
						Visible
						<input type="radio" name="citizenship_pdf" value="2"  <?php echo (Options::get('citizenship_pdf','2')=='2') ? 'checked="checked"':''?>/>
						Mandatory
					</p>
					<p class="element grid_12">
						<label for="passport_pdf" >PDF of Passport</label>
						<input type="radio" name="passport_pdf" value="0"  <?php echo (Options::get('passport_pdf','0')=='0') ? 'checked="checked"':''?>/> Hidden
						<input type="radio" name="passport_pdf" value="1"  <?php echo (Options::get('passport_pdf','1')=='1') ? 'checked="checked"':''?>/>
						Visible
						<input type="radio" name="passport_pdf" value="2"  <?php echo (Options::get('passport_pdf','2')=='2') ? 'checked="checked"':''?>/>
						Mandatory
					</p>
					<p class="element grid_12">
						<label for="registered_nepali_corporate_pdf" >PDF of Registered Nepali Corporate</label>
						<input type="radio" name="registered_nepali_corporate_pdf" value="0"  <?php echo (Options::get('registered_nepali_corporate_pdf','0')=='0') ? 'checked="checked"':''?>/> Hidden
						<input type="radio" name="registered_nepali_corporate_pdf" value="1"  <?php echo (Options::get('registered_nepali_corporate_pdf','1')=='1') ? 'checked="checked"':''?>/>
						Visible
						<input type="radio" name="registered_nepali_corporate_pdf" value="2"  <?php echo (Options::get('registered_nepali_corporate_pdf','2')=='2') ? 'checked="checked"':''?>/>
						Mandatory
					</p>
					<p class="element grid_12">
						<label for="registered_foreigner_corporate_pdf" >PDF of Registered Foreig Corporate</label>
						<input type="radio" name="registered_foreigner_corporate_pdf" value="0"  <?php echo (Options::get('registered_foreigner_corporate_pdf','0')=='0') ? 'checked="checked"':''?>/> Hidden
						<input type="radio" name="registered_foreigner_corporate_pdf" value="1"  <?php echo (Options::get('registered_foreigner_corporate_pdf','1')=='1') ? 'checked="checked"':''?>/>
						Visible
						<input type="radio" name="registered_foreigner_corporate_pdf" value="2"  <?php echo (Options::get('registered_foreigner_corporate_pdf','2')=='2') ? 'checked="checked"':''?>/>
						Mandatory
					</p>
					<p class="element grid_12">
						<label for="unregistered_foreigner_corporate_pdf" >PDF of Unregistered Foreig Corporate</label>
						<input type="radio" name="unregistered_foreigner_corporate_pdf" value="0"  <?php echo (Options::get('unregistered_foreigner_corporate_pdf','0')=='0') ? 'checked="checked"':''?>/> Hidden
						<input type="radio" name="unregistered_foreigner_corporate_pdf" value="1"  <?php echo (Options::get('unregistered_foreigner_corporate_pdf','1')=='1') ? 'checked="checked"':''?>/>
						Visible
						<input type="radio" name="unregistered_foreigner_corporate_pdf" value="2"  <?php echo (Options::get('unregistered_foreigner_corporate_pdf','2')=='2') ? 'checked="checked"':''?>/>
						Mandatory
					</p>
				</div>
			</fieldset>
			<p>
				<label>&nbsp;</label>
				<input type="submit" value="Save" id="submit-fees-config" /> 
				<a href="<?php echo site_url('config')?>" class="button">Cancel</a>
			</p>
		</form>
	</div>
	<?php } ?>
	
	<?php if (user_access('general setting')) { ?>
	<div class="grid_10" id="general-config">
		<form class="" action="" method="post" id="general-config-form">
			<p class="element">
				<?php //show_pre(Options::get('config_operating_countries',NULL));?>
				<label for="config_operating_countries" >Operating Countries</label>
				<?php getSelectCountry('config_operating_countries[]',Options::get('config_operating_countries',NULL),"size = 20, class='required multiselect', id='config_operating_countries'");?>
			</p>

      		<?php /* ?>
      		<p class="element">
	        	<label for="config_operating_banks" >Operating Banks</label>
	        	<?php getSelectBank('config_operating_banks[]',Options::get('config_operating_banks',NULL),"size = 20, class='required multiselect', id='config_operating_banks'");?>
      		</p>
      		<?php */ ?>

      		<p class="element">
      			<label for="config_adminemail">Admin Email</label>
      			<input type="text" name="config_adminemail" id="config_adminemail" value="<?php echo Options::get('config_adminemail','');?>" class="required full email" />
      		</p>
      		<p class="element">
      			<label for="config_technicalemail">Technical Email</label>
      			<input type="text" name="config_technicalemail" id="config_technicalemail" value="<?php echo Options::get('config_technicalemail','');?>" class="required full email" />
      		</p>
      		<p class="element">
      			<label for="cib_account">CIB Account</label>
      			<select name="cib_account">
      				<?php foreach (getRegistryAccounts() as $key => $value)  {   ?>
      				<option <?php echo ($value['acc_number'] == \Options::get('cib_account'))?'selected="selected"':''  ?> value="<?php echo  $value['acc_number'] ?>"><?php echo $value['username']."::".$value['acc_number'] ?></option>
      				<?php } ?>
      			</select>
      		</p>
      		<p class="element">
      			<label>Send email </label>
      			<input type="radio" name="send_mail" value="1"  <?php echo (Options::get('send_mail','0')=='1') ? 'checked="checked"':''?>/> YES &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      			<input type="radio" name="send_mail" value="0"  <?php echo (Options::get('send_mail','0')=='0') ? 'checked="checked"':''?>/> NO	
      		</p>


      		<p>
      			<label>&nbsp;</label>
      			<input type="submit" value="Save" id="submit-general-config" /> 
      			<a href="<?php echo site_url('config')?>" class="button">Cancel</a>
      		</p>
      	</form>
      </div>
      <?php } ?>


      <?php if (Current_User::isSuperUser()) { ?>
      	<div class="grid_10" id="email-template-config">
      		<form class="" action="" method="post">

      			<p class="element">
      				<label for="sign_up_message_to_client">To Client when they submits the Client Application</label>
      				<textarea name="sign_up_message_to_client" id="sign_up_message_to_client" cols="80" rows="3"><?php echo Options::get('sign_up_message_to_client','');?></textarea>
      			</p>
      			<p class="element">
      				<label for="sign_up_message_to_admin">To Registry Administrator when the Client submits the Client Application</label>
      				<textarea name="sign_up_message_to_admin" id="sign_up_message_to_admin" cols="80" rows="3"><?php echo Options::get('sign_up_message_to_admin','');?></textarea>
      			</p>
      			<p class="element">
      				<label for="sign_up_confirm_message">To Client with Client Account Number (Sign up confirmed)</label>
      				<textarea name="sign_up_confirm_message" id="sign_up_confirm_message" cols="80" rows="3"><?php echo Options::get('sign_up_confirm_message','');?></textarea>
      			</p>
      			<p class="element">
      				<label for="bank_receipt_entered_message">To Registry Administrator about the Payment made by the Client</label>
      				<textarea name="bank_receipt_entered_message" id="bank_receipt_entered_message" cols="80" rows="3"><?php echo Options::get('bank_receipt_entered_message','');?></textarea>
      			</p>
      			<p class="element">
      				<label for="bulk_message">Bulk message</label>
      				<textarea name="bulk_message" id="bulk_message" cols="80" rows="3"><?php echo Options::get('bulk_message','');?></textarea>
      			</p>
      			<p>
      				<label>&nbsp;</label>
      				<input type="submit" value="Save" id="submit-general-config" /> 
      				<a href="<?php echo site_url('config/sendBulk')?>" class="button">Send Bulk message</a>
      				<a href="<?php echo site_url('config')?>" class="button">Cancel</a>
      			</p>

      		</form>
      	</div>

      	<div class="grid_10" id="maintenance-config">
      		<form class="" action="" method="post">

      			<p class="element">
      				<label>Force Site Maintenance </label>
      				<input type="radio" name="site_maintenance" value="1" id="site_maintenance-yes" <?php echo (Options::get('site_maintenance','0')=='1') ? 'checked="checked"':''?>/> YES &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      				<input type="radio" name="site_maintenance" value="0" id="site_maintenance-no" <?php echo (Options::get('site_maintenance','0')=='0') ? 'checked="checked"':''?>/> NO	
      			</p>

      			<p class="element">
      				<label>Auto Resume After</label>
      				<input type="text" name="site_maintenance_resume_after" id="site_maintenance_resume_after" class="<?php echo (Options::get('site_maintenance','0')=='1' and Options::get('site_maintenance_resume','0')=='1') ? 'required':''?>" placeholder="Date Time" value="<?php echo Options::get('site_maintenance_resume_after','');?>" readonly="readonly"/>
      			</p>

      			<p class="element">
      				<label>Enable Auto Resume</label>
      				<input type="radio" name="site_maintenance_resume" value="1" id="site_maintenance_resume-yes" <?php echo (Options::get('site_maintenance_resume','0')=='1') ? 'checked="checked"':''?>/> YES &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      				<input type="radio" name="site_maintenance_resume" value="0" id="site_maintenance_resume-no" <?php echo (Options::get('site_maintenance_resume','0')=='0') ? 'checked="checked"':''?>/> NO			
      			</p>

      			<p>
      				<label>&nbsp;</label>
      				<input type="submit" value="Save" id="submit-maintenance-config" /> 
      				<a href="<?php echo site_url('config')?>" class="button">Cancel</a>
      			</p>
      		</form>
      		<script>
      			$(function() {
      				if ($('input[name="site_maintenance_resume"]').filter(':checked').val() == 1 && $('input[name="site_maintenance"]').filter(':checked').val() == 1)  $('#site_maintenance_resume_after').addClass('required').addClass('reqd');
      				$('#site_maintenance_resume-yes').click(function() {
      					$('#site_maintenance_resume_after').addClass('required').addClass('reqd');
      					if ($('#site_maintenance_resume_after').prev('label').find('.required').length != 1) $('#site_maintenance_resume_after').prev('label').append('<em class="required">*</em>');
      				});
      				$('#site_maintenance_resume-no').click(function() {
      					$('#site_maintenance_resume_after').removeClass('required').removeClass('reqd').prev('label').find('.required').remove();
      				});
      				$('#site_maintenance-no').click(function() {
      					$('#site_maintenance_resume_after').removeClass('required').removeClass('reqd').prev('label').find('.required').remove();
      				});
      				$('#site_maintenance-yes').click(function() {
      					if ($('input[name="site_maintenance_resume"]').filter(':checked').val() == 1) $('#site_maintenance_resume-yes').trigger('click');
      				});
      			})


</script>
</div>
<?php } ?>



</div>

<?php endif;?>
</div>

<script>
	$(function(){

		var hash = window.location.hash.substr(1,window.location.hash.length),
		active = ((indx = $('.tabs > div').index($('#' + hash + '-config'))) > 0) ? indx : 0;

		$('form').each(function(i,e) {
			$(this).attr('id', $(this).closest('div').attr('id') + '-form');
		});

		$('.required').addClass('reqd');

		$('.section.tabs').tabs({
			selected: active,
			select: function(e,ui){
				window.location.hash = $(ui.tab).attr('href').split('-')[0];
			}
		});	

		window.onhashchange = function() {

			hash = window.location.hash.substr(1,window.location.hash.length);
			active = ((indx = $('.tabs > div').index($('#' + hash + '-config'))) > 0) ? indx : 0;

			$('.section.tabs').tabs({
				selected: active,
				select: function(e,ui) {
					window.location.hash = $(ui.tab).attr('href').split('-')[0];
				}
			});
		}

		$('#txn_halt_from, #txn_halt_to, #site_maintenance_resume_after').datetimepicker({dateFormat: 'yy-mm-dd',});
		$('#submit-transaction-config').click(function(){
			var from	= $('#txn_halt_from').val(),
			to		= $('#txn_halt_to').val();

			if ( from != '' && to != '' && (from.replace(/[^0-9\.]+/g, '') >= to.replace(/[^0-9\.]+/g, '')) )	{

				alert('\r\nEntire Transaction Halt TimeStamp range invalid.\r\n"From Date Time" must be earlier(before) than "To Date Time".');		
				$('#txn_halt_from').val('');	$('#txn_halt_to').val('');
				return false;
			}

		});

		$('input[type="submit"]').bind('click', function() {

			$(this).closest('form').validate({
				errorElement: 'span',
				invalidHandler: function(form, validator) {
					if (validator.numberOfInvalids()) { $('div.response.success').hide(); }
				}
			});
			$('form').not('#' + $(this).closest('form').attr('id')).each(function(i,e) {
				$(this).find('input.required, textarea.required, select.required').removeClass('required');
			});

		});




	});


</script>
